<?php

namespace App\Http\Middleware;

use App\Store;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
           if(Auth::user()->role==User::ADMIN)
            return redirect('admin/dashboard');
            elseif(Auth::user()->role==User::STORE)
            return redirect()->route('stores.show',Store::where('user_id',Auth::id())->first());
        }

        return $next($request);
    }
}
