<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Profile\CheckinResource;
use App\Http\Resources\Tips\TipResource;
use Illuminate\Http\Resources\Json\JsonResource;
// use App\Http\Resources\Client\Profile\CollectionResource;

class ClientResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {


        return [
            'email'         => $this->user->email,
            'first_name'    => $this->first_name,
            'last_name'     => $this->last_name,
            'age'           => $this->age,
            'bio'           => $this->bio,
            'gender'        => $this->gender,
            'phone'         => $this->phone,
            'avatar'        => $this->avatar,
            'fb_url'        => $this->fb_url,
            'tw_url'        => $this->tw_url,
            'wh_url'        => $this->wh_url,

            'country_id'    => $this->country_id,
            'latitude'      => $this->latitude,
            'longitude'     => $this->longitude,
            'address'       => $this->address,
            'checkins_count'       => empty(CheckinResource::collection($this->whenLoaded('checkins')))?0:CheckinResource::collection($this->whenLoaded('checkins'))->count(),
            'tips_count'   =>  empty(TipResource::collection($this->whenLoaded('tips')))?0:TipResource::collection($this->whenLoaded('tips'))->count(),

            // 'tags'          => $this->tags->pluck('name'),
        ];
    }
}
