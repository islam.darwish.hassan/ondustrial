<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{


    public function __construct($resource, $token = '')
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->token = $token;
    }


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'email'                 => $this->email,
            'token'                 => $this->when(!empty($this->token), $this->token)
        ];
    }
}
