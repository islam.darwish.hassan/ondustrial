<?php

namespace App\Http\Resources\Stores;

use App\ListCollection;
use App\ListPlace;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Stores\ParamResource;
use App\Http\Resources\Profile\CollectionResource;
use App\Http\Resources\Profile\ListPlaceResource;
use App\Http\Resources\Profile\ListResource;
use App\Http\Resources\Stores\SubCategoryResource;
use App\Review;
use App\Tip;
use Carbon\Carbon;

class StoreResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $bookmark = ListPlace::where(['store_id' => $this->id, 'client_id' => $request->user()->client->id])->first();

        if($bookmark)
        {
            $bkmrk_res = [
                'is_bookmarked' => true,
                'id'=>$bookmark->id,
                'collection' => new ListResource(ListCollection::find($bookmark->list_collection_id))
            ];
        }
        else
        {
            $bkmrk_res = [
                'is_bookmarked' => false,
                'collection' => []
            ];
        }
        return [
            'id'            => $this->id,
            'sub_category'  => new SubCategoryResource($this->sub_category),
            'name'          => $this->name,
            'bio'           => $this->bio,
            'rate'          => $this->rate,
            'status'        => ($this->status == 1) ? 'verified' : 'not-verified',
            'phone'         => $this->phone,
            'avatar'        => $this->avatar,
            'header_image'  => $this->header_image,
            'latitude'      => $this->latitude,
            'longitude'     => $this->longitude,
            'address'       => $this->address,
            'pricing_class' => $this->pricing_class,
            'open_at'       => $this->open_at,
            'close_at'      => $this->close_at,
            'open_at_formated'      => Carbon::parse($this->open_at)->format('g:i A'),
            'close_at_formated'     => Carbon::parse($this->close_at)->format('g:i A'),
            'is_opened'     => (Carbon::now() > $this->open_at && Carbon::now() < $this->close_at),
            'website'       => $this->website,
            'fb_url'        => $this->fb_url,
            'wh_url'        => $this->wh_url,
            'tw_url'        => $this->tw_url,
            'created_at_string'     => $this->created_at->diffForHumans(),
            'features'      => StoreFeatureResource::collection($this->whenLoaded('features')),
            'rates_precentage'        => [
            'impressive_rates_count'           => empty($this->impressive_rates_count )||  Tip::where('store_id',$this->id)->count()==0 ? 0 : 
            round( ($this->impressive_rates_count/ Tip::where('store_id',$this->id)->count())*100),
            'ok_rates_count'                   => empty($this->ok_rates_count) ||  Tip::where('store_id',$this->id)->count()==0 ? 0 : 
            round( ($this->ok_rates_count/ Tip::where('store_id',$this->id)->count())*100),
            'bad_rates_count'                  => empty($this->bad_rates_count ) || Tip::where('store_id',$this->id)->count()==0? 0 :
            round( ($this->bad_rates_count/ Tip::where('store_id',$this->id)->count())*100),

            'total_precentage'                 => ($this->rate)*10,

            ],

            'is_bookmarked' => $bkmrk_res,
            'tips_count' => Tip::where('store_id',$this->id)->count()
        ];
    }
}
