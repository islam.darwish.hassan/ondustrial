<?php

namespace App\Http\Resources\Stores;

use Illuminate\Http\Resources\Json\JsonResource;

class StoreUserImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'image'             => $this->content,
            'caption'           => $this->caption,
            'client'             => [
                'first_name'     => $this->client->first_name,
                'last_name'      => $this->client->last_name,
                'avatar'         =>  $this->client->avatar,
                'tips_count'     => $this->client->tips->count(),
                'checkins_count' => $this->client->checkins->count()

            ],
        ];
    }
}
