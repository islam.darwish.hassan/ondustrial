<?php

namespace App\Http\Resources\Tips;

use App\Checkin;
use App\Http\Resources\Stores\StoreResource;
use App\Http\Resources\Stores\SubCategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TipResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'post_type'             => 'tip' ,
            'id'                    => $this->id,
            'type'                  => $this->type,
            'content'               => $this->content,
            'caption'               => $this->caption,
            'rate'                  => $this->rate,
            'is_recommended'        => $this->is_recommended,
            'created_at'            => $this->created_at->format('Y-m-d H:i:s'),
            'created_at_string'     => $this->created_at->diffForHumans(),
            'client'                => [
                'id'             => $this->client->id,
                'first_name'     => $this->client->first_name,
                'last_name'      => $this->client->last_name,
                'avatar'         =>  $this->client->avatar,
                'tips_count'     => $this->client->tips->count(),
                'tips_count_store'     => $this->where(['client_id'=> $this->client->id ,'store_id'=>$this->store->id])->count(),
                'checkins_count'       => $this->client->checkins->count(),
                'checkins_count_store' => Checkin::where(['client_id'=> $this->client->id ,'store_id'=>$this->store->id])->count()

            ],
            'store'              => [
                'id' => $this->store->id,
                'name'=>$this->store->name,
                'rate'=>$this->store->rate,
                'pricing_class'=>$this->store->pricing_class,
                'sub_category'  => new SubCategoryResource($this->store->sub_category),
                'avatar'=>$this->store->avatar,
                'header_image'=>$this->store->header_image

            ],
            'emotions_count'        => [
                'up_emotions_count'           => empty($this->up_emotions_count) ? 0 : $this->up_emotions_count,
                'down_emotions_count'         => empty($this->down_emotions_count) ? 0 : $this->down_emotions_count,
                'emotions_count'               => empty($this->emotions_count) ? 0 : $this->emotions_count
            ],


        ];
    }
}
