<?php

namespace App\Http\Resources\Profile;

use Illuminate\Http\Resources\Json\JsonResource;

class SavedStoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'client'                => [
                'first_name'     => $this->client->first_name,
                'last_name'      => $this->client->last_name,
                'avatar'         =>  $this->client->avatar,
                'tips_count'     => $this->client->tips->count(),
                'checkins_count' => $this->client->checkins->count()

            ],
            'store'              => [
                'id' => $this->store->id,
                'name'=>$this->store->name,
                'rate'=>$this->store->rate,
                'pricing_class'=>$this->store->pricing_class,
                'subcategory_id'=>$this->store->sub_category->id,
                'subcategory'=>$this->store->sub_category->name,
                'avatar'=>$this->store->avatar,
                'header_image'=>$this->store->header_image

            ],

        ];
    }
}
