<?php

namespace App\Http\Resources\Profile;

use App\Http\Resources\Stores\StoreResource;
use App\Store;
use Illuminate\Http\Resources\Json\JsonResource;

class ListPlaceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $store= Store::where('id',$this->store_id)->firstOrFail();
        return [
            'id'                    => $this->id,
            'store'         => [
                'id' => $store->id,
                'name'=>$store->name,
                'rate'=>$store->rate,
                'pricing_class'=>$store->pricing_class,
                'subcategory_id'=>$store->sub_category->id,
                'subcategory'=>$store->sub_category->name,
                'avatar'=>$store->avatar,
                'header_image'=>$store->header_image

                                         ],
            
        ];
    }
}
