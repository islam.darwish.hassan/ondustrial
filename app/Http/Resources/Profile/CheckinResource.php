<?php

namespace App\Http\Resources\Profile;

use App\Http\Resources\Stores\StoreResource;
use App\Http\Resources\Stores\SubCategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CheckinResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'post_type'             => 'checkin' ,
            'id'            => $this->id,
            'store'         => [
                'id' => $this->store->id,
                'name'=>$this->store->name,
                'rate'=>$this->store->rate,
                'pricing_class'=>$this->store->pricing_class,
                'sub_category'  => new SubCategoryResource($this->store->sub_category),
                'avatar'=>$this->store->avatar,
                'header_image'=>$this->store->header_image

                                         ],
            'type'          => $this->type,
            'content'       => $this->content,
            'caption'       => $this->caption,
            'latitude'      => $this->latitude,
            'longitude'     => $this->longitude,
            'created_at'    => $this->created_at,
            'created_at_string'     => $this->created_at->diffForHumans(),

        ];
    }
}
