<?php

namespace App\Http\Misc\Helpers;

class Config
{
	const PAGINATION_LIMIT = 10;
	const MIN_GPS_RADIUS = 0.0;
	const MAX_GPS_RADIUS = 0.3;
}
