<?php

namespace App\Http\Requests\WebService\Tips;

use App\Http\Misc\Helpers\Base64Handler;
use App\Rules\ValidEncodedFile;
use Illuminate\Foundation\Http\FormRequest;

class AddNewTipRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'content'              =>  new ValidEncodedFile(Base64Handler::IMAGE_EXTS),
            'caption'               => 'required_if:type,1|min:2|max:150',
            'type'                  => 'required|between:1,2',
            'rate'                  => 'required|between:0,10',

        ];
    }
}
