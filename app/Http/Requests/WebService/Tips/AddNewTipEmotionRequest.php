<?php

namespace App\Http\Requests\WebService\Tips;

use App\SubCategory;
use App\Rules\ValidEncodedFile;
use App\Http\Misc\Helpers\Base64Handler;
use Illuminate\Foundation\Http\FormRequest;

class AddNewTipEmotionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:up,down'
        ];
    }
}
