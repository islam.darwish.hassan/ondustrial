<?php

namespace App\Http\Requests\WebService\Auth;

use App\Country;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $countries = Country::all()->implode('id', ',');

        return [
            'email'         =>  'required|email|unique:users|max:30',
            'password'      =>  'required|min:6|confirmed|max:190',
            'first_name'     =>  'required|min:2',
            'last_name'     =>  'required|min:2',
            'phone'         =>  'required|min:9|max:12',
            'country_id'    =>  'integer|in:'.$countries,
            'gender'         => 'in:m,f',


        ];
    }
}
