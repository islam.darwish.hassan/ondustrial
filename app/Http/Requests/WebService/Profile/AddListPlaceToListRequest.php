<?php

namespace App\Http\Requests\WebService\Profile;

use App\Store;
use Illuminate\Foundation\Http\FormRequest;

class AddListPlaceToListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $stores = Store::all()->implode('id', ',');

        return [
            'list_collection_id' =>  'in:'.$this->user()->client->list_collection->implode('id', ','),
            'store_id'      =>  'in:'.$stores
        ];
    }
}
