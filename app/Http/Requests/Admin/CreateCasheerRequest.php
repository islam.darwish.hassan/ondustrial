<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CreateCasheerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'         => 'required|unique:users|max:190|email',
            'password'      => 'required|max:190|min:8|alpha_num',
            'name'          => 'required|min:3|max:50',
            'phone'         => 'required |max:12|min:7',
            'address'       => 'required|max:250|min:8',
            'image'        => 'required|image',
            'bio'           => 'required|max:190|min:3|',

        //     'subscribed'           => 'required_if:role,==,3|between:1,3',

        ];
    }
}
