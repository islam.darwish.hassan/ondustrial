<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCasheerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'          => 'required|min:3|max:50',
            'phone'         => 'required |max:12|min:7',
            'address'       => 'required|max:250|min:8',
            'image'        => 'image',
            'bio'           => 'required|max:190|min:3',

        ];
    }
}
