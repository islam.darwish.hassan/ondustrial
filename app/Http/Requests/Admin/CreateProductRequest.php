<?php

namespace App\Http\Requests\Admin;

use App\SubCategory;
use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $sub_categories = SubCategory::all()->implode('id', ',');

        return [
            'sub_category'         => 'in:' . $sub_categories,
            'name'                    => 'required|min:30|max:500',
            'description'             => 'required|min:30|max:500',
            'image'                   => 'required|image|dimensions:ratio=1/1|dimensions:min_width=511,min_height=511|dimensions:max_width=2049,max_height=2049',

            'name'                    => 'required|min:5|max:150',
            'price'                   => 'required|integer',
            'in_stock'                => 'required|integer',

            'brand'                    =>'nullable|min:1|max:120',
            'modal_number'            => 'nullable|min:1|max:120',
            'external_product_id'     => 'nullable|min:1|max:120',
            'ean_13'                  => 'nullable|min:1|max:120',

            'package_height'          => 'nullable|min:1|max:120',
            'package_weight'          => 'nullable|min:1|max:120',
            'package_width'           => 'nullable|min:1|max:120',
            'package_thickness'       => 'nullable|min:1|max:120',

            //
        ];
    }
}
