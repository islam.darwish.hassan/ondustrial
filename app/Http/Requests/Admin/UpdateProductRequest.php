<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'     => 'min:30|max:500',
            'image'           => 'image|dimensions:ratio=1/1|dimensions:min_width=511,min_height=511|dimensions:max_width=2049,max_height=2049',
            'name'          => 'min:5|max:150',
            'price'          => 'integer',
            'in_stock'          => 'integer',

            //
        ];
    }
}
