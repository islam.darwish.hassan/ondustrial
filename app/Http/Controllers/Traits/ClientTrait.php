<?php

namespace App\Http\Controllers\Traits;

use App\Point;
use Illuminate\Support\Facades\Hash;
use App\Http\Misc\Helpers\Base64Handler;

trait ClientTrait
{


    public function completeClientData($request,$user)
    {
    	$this->user_id      = $user->id;
        $this->first_name   = $request->first_name;
        $this->last_name    = $request->last_name;
        $this->phone        = $request->phone;
        $this->country_id   = $request->country_id;
        if($request->has('address'))
        $this->address = $request->address;
        if($request->has('gender'))
            $this->gender = $request->gender;
        if($request->has('fb_url'))
            $this->fb_url = $request->fb_url;
        if($request->has('tw_url'))
            $this->tw_url = $request->tw_url;
        if($request->has('wh_url'))
            $this->wh_url = $request->wh_url;
        if($request->has('age'))
            $this->age = $request->age;
        if($request->has('bio'))
            $this->bio = $request->bio;
        if($request->has('avatar'))
        $this->avatar = Base64Handler::storeFile($request->avatar, 'users_images');
        $this->save();




    }


    public function updateClientData($request)
    {
        if($request->has('first_name'))
            $this->first_name = $request->first_name;

        if($request->has('last_name'))
            $this->last_name = $request->last_name;

        if($request->has('age'))
            $this->age = $request->age;

        if($request->has('bio'))
            $this->bio = $request->bio;

        if($request->has('address'))
            $this->address = $request->address;
        if($request->has('phone'))
            $this->phone = $request->phone;

        if($request->has('gender'))
            $this->gender = $request->gender;

        if($request->has('fb_url'))
            $this->fb_url = $request->fb_url;
        if($request->has('tw_url'))
            $this->tw_url = $request->tw_url;

        if($request->has('wh_url'))
            $this->wh_url = $request->wh_url;


        if($request->has('avatar'))
            $this->avatar = Base64Handler::storeFile($request->avatar, 'users_images');

        $this->save();

        //$this->tags()->sync($request->tags);


    }



}
