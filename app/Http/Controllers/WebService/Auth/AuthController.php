<?php

namespace App\Http\Controllers\WebService\Auth;

use App\User;
use App\Client;
use Illuminate\Http\Request;
use App\Http\Misc\Helpers\Errors;
use App\Http\Misc\Helpers\Success;
use App\Http\Controllers\Controller;
use App\Http\Requests\WebService\Auth\CompleteProfileRequest;
use App\Http\Requests\WebService\Auth\ForgetPasswordRequest;
use App\Http\Requests\WebService\Auth\LoginRequest;
use App\Http\Requests\WebService\Auth\RegisterRequest;
use App\Http\Resources\User\ClientResource;
use App\Http\Resources\User\UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{

    /**
     * register a new  user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(RegisterRequest $request)
    {
        $user = new User;
        $user->saveUserData($request);
        $token = $user->createToken('Takeit')->accessToken;
        $client = new Client;
        $client->completeClientData($request,$user);

        return $this->success_response(['user' => new UserResource($user, $token), 'client' => new ClientResource($user->client->load('checkins','tips'))]);
    }




    /**
     * complete profile.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function complete(CompleteProfileRequest $request)
    {
        if($request->user()->client)
            return $this->error_response(Errors::COMPLETED_PROFILE_BEFORE);

        $client = new Client;
        $client->completeClientData($request);
        return $this->success_response(['client' => new ClientResource($client)]);
    }




    /**
     * login user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        $user = User::where(['email' => strtolower($request->email), 'role' => User::CLIENT])->first();

        if(!$user)
            return $this->error_response(Errors::NOT_FOUND_USER);

        if(!Hash::check($request->password, $user->password))
            return $this->error_response(Errors::WRONG_PASSWORD);

        $client = $user->client;

        if(!$client)
            return $this->error_response(Errors::COMPLETED_PROFILE_MUST);

        $client->increment('login_count');

       // DB::table('oauth_access_tokens')->where('user_id', $user->id)->delete();

        $token = $user->createToken('Takeit')->accessToken;

        return $this->success_response(['user' => new UserResource($user, $token), 'client' => new ClientResource($user->client->load('checkins','tips'))]);
    }




    /**
     * forget password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function forget(ForgetPasswordRequest $request)
    {
        //
    }




    /**
     * logout user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        return $this->dummy_response(Success::LOGGED_OUT);
    }


}
