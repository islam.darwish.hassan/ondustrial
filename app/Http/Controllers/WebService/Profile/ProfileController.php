<?php

namespace App\Http\Controllers\WebService\Profile;

use App\Client;
use App\Review;
use App\Checkin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\User\ClientResource;
use App\Http\Resources\Reviews\ReviewResource;
use App\Http\Resources\Profile\CheckinResource;
use App\Http\Requests\Webservice\Profile\LocateUserRequest;
use App\Http\Resources\Tips\TipResource;
use App\Tip;

class ProfileController extends Controller
{

    /**
     * Display the user's profile.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return $this->success_response(['profile' => new ClientResource($request->client->load('checkins','tips'))]);
    }




    /**
     * Display the user's profile.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function showClientData(Request $request, Client $client)
    {
        return $this->success_response(['profile' => new ClientResource($client->load('checkins','tips'))]);
    }





     /**
     * Display the user's checkins.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function showClientCheckins(Request $request, Client $client)
    {
        $checkins = Checkin::where('client_id', $client->id)->with('store')->paginate(10);
        return $this->success_response(['checkins' => CheckinResource::collection($checkins)]);
    }




    /**
     * Display the user's reviews.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function tips(Request $request)
    {
        $tips = Tip::where(['client_id' => $request->user()->client->id])
                        ->withCount('up_emotions', 'down_emotions','emotions')
                        ->orderBy('created_at', 'desc')
                        ->paginate(10);
        return $this->success_response(['tips' => TipResource::collection($tips)]);
    }

    public function activity(Request $request)
    {
        $tips = Tip::where(['client_id' => $request->user()->client->id])
                        ->withCount('up_emotions', 'down_emotions','emotions')->get();
         $tips= TipResource::collection($tips);
         $checkins = Checkin::where('client_id', $request->client->id)->with('store')->get();
         $checkins= CheckinResource::collection($checkins);
         $activity = $tips->merge($checkins)->sortByDesc('created_at')->values();
         $activity=$activity->forPage($_GET['page'], 10);
         return $this->success_response(['activity' =>$activity]);

    }



    /**
     * Display the user's reviews.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function showClientTips(Request $request, Client $client)
    {
        $tips = Tip::where(['client_id' => $client->id])
                        ->withCount('up_emotions', 'down_emotions','emotions')
                        ->orderBy('created_at', 'desc')
                        ->paginate(10);
        return $this->success_response(['tips' => TipResource::collection($tips)]);
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->user()->client->updateClientData($request);
        return $this->success_response(['profile' => new ClientResource($request->user()->client->load('checkins','tips'))]);
    }



    /**
     * Locate user's location.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function locate(LocateUserRequest $request)
    {
        $client = $request->user()->client;
        $client->latitude = $request->lat;
        $client->longitude = $request->long;
        $client->save();
        return $this->success_response(['lat' => $request->lat, 'long' => $request->long]);
    }








}
