<?php

namespace App\Http\Controllers\WebService\Profile;

use App\Checkin;
use App\ListPlace;
use App\ListCollection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Profile\ListPlacesResource;
use App\Http\Requests\WebService\Profile\AddListPlaceToListRequest;
use App\Http\Resources\Profile\ListPlaceResource;
use App\Http\Resources\Profile\ListResource;
use App\Http\Resources\Stores\StoreResource;
use App\SavedStore;

class ListPlacesController extends Controller
{
    /**
     * Display a listing of the specific list ListPlaces.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $list_places = ListPlace::where(['list_collection_id' =>$id])
                        ->orderBy('created_at', 'desc')
                        ->paginate(10);
        return $this->success_response(['list_places' =>  ListPlaceResource::collection($list_places)]);

    }
    /**
     * Display a listing of the specific list ListPlaces.
     *
     * @return \Illuminate\Http\Response
     */

    public function showLiked(Request $request)
    {
         $stores = SavedStore::where(['client_id' =>$request->user()->client->id])
                        ->orderBy('created_at', 'desc')
                        ->paginate(10);
        return $this->success_response(['liked_places' =>  StoreResource::collection($stores)]);

    }
        /**
     * Display a listing of the specific list ListPlaces.
     *
     * @return \Illuminate\Http\Response
     */



    /**
     * Store a newly created bookmark.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddListPlaceToListRequest $request)
    {
        $bookmark = ListPlace::firstOrCreate(['list_collection_id' => $request->list_collection_id, 'store_id' => $request->store_id, 'client_id' => $request->user()->client->id]);
        return $this->dummy_response('added successfully!');
    }




    /**
     * Destroy a bookmark.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ListPlace $place)
    {
        $place->delete();
        return $this->dummy_response('deleted successfully!');
    }


}
