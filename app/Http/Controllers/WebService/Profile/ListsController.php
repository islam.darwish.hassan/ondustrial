<?php

namespace App\Http\Controllers\WebService\Profile;

use App\Point;
use App\ListPlace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Base64Handler;
use App\Http\Resources\Profile\ListResource;
use App\ListCollection;

class ListsController extends Controller
{
    /**
     * Display a listing of the collections.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->success_response(['lists' => ListResource::collection($request->user()->client->list_collection()
        ->paginate(10))]);
    }



    /**
     * Store a newly created collection.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $listCollection = new ListCollection();
        $listCollection->name = $request->name;
        $listCollection->client_id = $request->user()->client->id;
       // $listCollection->image = Base64Handler::storeFile($request->image, 'collections_images');
       $listCollection->image = 'no_image';
        $listCollection->save();


        return $this->success_response(['list_collection' => new ListResource($listCollection)]);
    }



    /**
     * Update a collection.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ListCollection $list_collection)
    {
        $list_collection->name = $request->name;
        $list_collection->save();
        return $this->success_response(['list_collection' => new ListResource($list_collection)]);
    }





    /**
     * Destroy a collection.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ListCollection $listCollection)
    {
        $listCollection->delete();
        return $this->dummy_response('deleted successfully!');
    }


}
