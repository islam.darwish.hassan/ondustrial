<?php

namespace App\Http\Controllers\WebService\Stores;

use App\Checkin;
use App\User;
use App\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Misc\Helpers\Base64Handler;
use App\Http\Resources\Stores\StoreResource;
use App\Http\Resources\Stores\StoreImageResource;
use App\Http\Resources\Stores\StoreUserImageResource;
use App\StoreImage;
use App\SubCategory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class StoresController extends Controller
{
    /**
     * Display a listing of the stores.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $stores = Store::where('id','!=','0');

        if($request->has('sub_category_id'))
            if($request->sub_category_id!=""){
            $stores = $stores->where('sub_category_id', $request->sub_category_id);
            }
        if($request->has('verified'))
            if($request->verified!=""){
            $stores = $stores->where('status', '=', $request->verified);
            }
        if($request->has('pricing_class'))
            if($request->pricing_class!=""){
            $stores = $stores->where('pricing_class', $request->pricing_class);
            }
        if($request->has('is_opened'))
            if($request->is_opened!=""){
            $stores = $stores->whereTime('close_at', '>', Carbon::now())->whereTime('open_at', '<', Carbon::now());
            }
        if($request->has('name'))
             if($request->name!=""){
            $stores = $stores->where('name', 'LIKE', "%{$request->name}%");
             }
        if($request->has('address'))
            if($request->address!=""){
            $stores = $stores->where('address', 'LIKE', "%{$request->address}%");
            }

        if($request->has('filterBy'))
        {
            if($request->filterBy!=""){


            if($request->filterBy == 'rate')
            {

                $stores =  $stores->orderBy('rate', 'desc');

            }else if($request->filterBy == 'tips')
            {
                $stores = $stores->withCount('tips')->orderBy('tips_count', 'desc');
            }
             }
        }

        $stores=$stores->withCount('impressive_rates', 'ok_rates','bad_rates');

        $stores = $stores->paginate(10);

        return $this->success_response(['stores' => StoreResource::collection($stores)]);
    }



    /**
     * Display a listing of the offers.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Display a store page.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        $store=Store::where('id',$store->id)->withCount('ok_rates','impressive_rates','bad_rates')->firstorFail();
        return $this->success_response(['store' => new StoreResource($store->load("features"))]);
    }
    public function stores_similar(Store $store)
    {
        $sub_category=SubCategory::where('id',$store->sub_category_id)->firstorFail();
        $stores=Store::where('sub_category_id',$sub_category->id)->withCount('ok_rates','impressive_rates','bad_rates')->limit(3)->get();
        return $this->success_response(['stores' => StoreResource::collection($stores)]);
    }

    /**
     * Display a show images.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexImages(Request $request , Store $store)
    {
        $store_image = StoreImage::where(['store_id' => $store->id])
                        ->orderBy('created_at', 'desc')
                        ->paginate(6);
        return $this->success_response(['images' =>   StoreImageResource:: collection($store_image)]);
    }


    /**
     * Display a show images.
     *
     * @return \Illuminate\Http\Response
     */
    public function usersImages(Request $request , Store $store)
    {
         $store_image = Checkin::where(['store_id' => $store->id])
                        ->where('type',2)
                        ->orderBy('created_at', 'desc')
                        ->paginate(6);
        return $this->success_response(['images' =>   StoreUserImageResource:: collection($store_image)]);
    }


    /**
     * Store a newly created store.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;
        $user->email = Str::random(10) . '@dummy.com';
        $user->password = Hash::make(Str::random(10));
        $user->role = User::STORE;
        $user->save();

        $store = new Store;
        $store->user_id = $user->id;
        $store->sub_category_id = $request->sub_category_id;
        $store->name = $request->name;
        $store->rate = 0;
        $store->status = Store::STATUS_NOT_VERIFIED;
        $store->country_id=196;

        if($request->has('bio'))
            $store->bio = $request->bio;

        if($request->has('phone'))
            $store->phone = $request->phone;

        if($request->has('address'))
            $store->address = $request->address;

        if($request->has('latitude'))
            $store->latitude = $request->latitude;

        if($request->has('longitude'))
            $store->longitude = $request->longitude;

        if($request->has('avatar'))
            $store->avatar = Base64Handler::storeFile($request->avatar, 'stores_images');
        else
            $store->avatar = 'default.jpg';

        $store->save();



        return $this->success_response(['store' => new StoreResource($store)]);

    }

}
