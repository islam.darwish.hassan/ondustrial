<?php

namespace App\Http\Controllers\WebService\Stores;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Stores\CategoryResource;
use App\Http\Resources\Stores\SubCategoryResource;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::orderBy('name');

        if($request->has('name'))
            $categories = $categories->where('name', 'LIKE', "%{$request->name}%");

        $categories = $categories->paginate(10);

        return $this->success_response(['categories' => CategoryResource::collection($categories)]);
    }



    /**
     * Display the specified category sub categories.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Category $category)
    {
        $sub_categories = SubCategory::where('category_id', $category->id)->orderBy('name');

        if($request->has('name'))
            $sub_categories = $sub_categories->where('name', 'LIKE', "%{$request->name}%");

        $sub_categories = $sub_categories->paginate(10);

        return $this->success_response(['sub_categories' => SubCategoryResource::collection($sub_categories)]);
    }



}
