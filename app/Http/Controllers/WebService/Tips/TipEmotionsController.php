<?php

namespace App\Http\Controllers\WebService\Tips;

use App\Client;
use App\Tip;
use App\TipEmotion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tips\AddNewTipEmotionRequest as TipsAddNewTipEmotionRequest;
use App\Notifications\EmotionOnReview;
use App\Http\Resources\Tips\EmotionClientResource;
use App\Http\Requests\WebService\Tips\AddNewTipEmotionRequest;

class TipEmotionsController extends Controller
{
    /**
     * Display a listing of the tip emotions.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Tip $tip, $emotion_type)
    {
        $clients_ids = [];

        if($emotion_type == 'up')
            $clients_ids = $tip->up_emotions->pluck('client_id');
        else if($emotion_type == 'down')
            $clients_ids = $tip->down_emotions->pluck('client_id');

        $clients = Client::whereIn('id', $clients_ids)->paginate(10);

        return $this->success_response(['users' => EmotionClientResource::collection($clients)]);
    }

    /**
     * Store a newly created tip emotion.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddNewTipEmotionRequest $request, Tip $tip)
    {
        if($tip->client_id == $request->user()->client->id)
            return $this->error_response('you cant make emotion on your tip');

        if($request->type == 'up')
        {
            $type = TipEmotion::TYPE_UP;
        }
        if($request->type == 'down')
            $type = TipEmotion::TYPE_DOWN;
        TipEmotion::updateOrCreate(
            ['client_id' => $request->user()->client->id, 'tip_id' => $tip->id],
            ['type' => $type]
        );

        $user = $tip->client->user;
        //$user->notify(new EmotionOnReview("{$request->user()->client->first_name} make an {$request->type} emotion on your tip", $tip->id));

        return $this->success_response(
            [ 'emotions_count'        => [
                'up_emotions_count'           => empty($tip->up_emotions->count()) ? 0 : $tip->up_emotions->count(),
                'down_emotions_count'         => empty($tip->down_emotions->count()) ? 0 : $tip->down_emotions->count(),
            ],
           'emotion'=>TipEmotion::where('client_id',$request->user()->client->id)->where('tip_id',$tip->id)->first()  ]);
        }


    /**
     * Destroy emotion.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Tip $tip)
    {
        TipEmotion::where(['client_id' => $request->user()->client->id, 'tip_id' => $tip->id])->delete();
        return $this->dummy_response('tip emotions deleted successfully!');
    }


}
