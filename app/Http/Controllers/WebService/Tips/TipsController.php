<?php

namespace App\Http\Controllers\WebService\Tips;

use App\Store;
use App\Client;
use App\Tip;
use App\TipRate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Base64Handler;
use App\Http\Requests\WebService\Tips\AddNewTipRequest;
use App\Http\Resources\Tips\TipResource;
use App\SavedStore;
use PDO;

class TipsController extends Controller
{
    /**
     * Display a listing of the tips.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Store $store)
    {
        $tips = Tip::where('store_id', $store->id)
        ->withCount('up_emotions', 'down_emotions','emotions');

        if($request->has('filterBy'))
        {
            if($request->filterBy!=""){

         
            if($request->filterBy == 'tips')
            {
            $tips = $tips ->orderBy('up_emotions_count', 'desc');
            }
        } 
    }  else{
        $tips = $tips ->orderBy('created_at', 'desc');

}      
    $tips = $tips->paginate(10);

        return $this->success_response(['tips' => TipResource::collection($tips->load('store'))]);
    }

    public function indexRecommended(Request $request, Store $store)
    {
         $tips = Tip::where('store_id', $store->id)
                        ->withCount('up_emotions', 'down_emotions','emotions')
                        ->orderBy('up_emotions_count', 'desc')
                        ->orderBy('down_emotions_count', 'asc')
                        ->orderBy('emotions_count', 'desc')
                        ->limit(3)
                        ->get();
        return $this->success_response(['tips' => TipResource::collection($tips->load('store'))]);
    }

    /**
     * Destroy Tip.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Tip $tip)
    {
        if($request->user()->client->id != $tip->client_id)
            return $this->error_response('you cant remove a tip that does not belong to you!');
        $tip->delete();

        $store=$tip->store;
        $store->rate = round($store->tips()->avg('rate'),2);
        $store->save();
    

        return $this->dummy_response('tip deleted successfully!');
    }



    /**
     * Store a newly created tip.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddNewTipRequest $request, Store $store)
    {

        $tip                     = new Tip;
        $tip->store_id           = $store->id;
        $tip->client_id          = $request->user()->client->id;
        $tip->type               = $request->type;
        if($request->type==2){
         $tip->content = Base64Handler::storeFile($request->content, 'stores_images');
        }else{
            $tip->content            = 'this is text';
        }
        if($request->has('caption')){
            $tip->caption            = $request->caption;

        }else{
            $tip->caption            = "No Caption Provided";

        }
        $tip->is_recommended     =1;
        $tip->rate               = $request->rate;
        $tip->save();

        $store->rate = round($store->tips()->avg('rate'),2);
        $store->save();

        if($request->rate>7){
            $saved = new SavedStore();
            $saved->client_id   = $request->user()->client->id;
            $saved->store_id    = $store->id;
            $saved->tip_id      = $store->id;
            $saved->save();       
        }
        return $this->success_response(['tip' => new TipResource($tip)]);
    }





}
