<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Filters;
use App\Http\Requests\Admin\CreatePresetRequest;
use App\Http\Requests\Admin\UpdatePresetRequest;
use App\Preset;
use App\PresetsParameter;
use Illuminate\Support\Facades\Auth;

class PresetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,Preset $preset )
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $presets=  $preset;
        $presets = Filters::searchBy($presets, ['name' => $request->query('name')]);
        $presets=$presets->withCount('params')->sortable(['created_at'=>'desc'])->paginate(8);
        $data=$presets;
        $name="presets";
        return view('presets.index', compact('data','name','presets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $name="presets";
        return view('presets.create', compact('name'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePresetRequest $request)
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $preset = new Preset();
        $preset->name     = $request->input('name');
        $preset->desc     = $request->input('desc');

        $preset->save();

        return redirect()->route('presets.index')->with('message', 'Preset created successfully');
    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Preset $preset  )
    {
        //
        $this->authorize('view-super', Auth::user()); 

         $params=  PresetsParameter::where('preset_id',$preset->id);
        $params = Filters::searchBy($params, ['name' => $request->query('name')]);
        $params=$params->paginate(8);
        $data=$params;
        $name="params";
        return view('presets.show', compact('preset','data','name','params'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Preset $preset)
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $name = 'presets';

        return view('presets.edit', compact('preset', 'name'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePresetRequest $request, Preset $preset )
    {
        //
        $this->authorize('view-super', Auth::user()); 

        if (isset($request->name))
            $preset->name = $request->name;

        if (isset($request->desc))
            $preset->desc = $request->desc;

        $preset->save();

        return redirect()->route('presets.index')->with('message', 'Preset info updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Preset $preset)
    {
        //
        $this->authorize('view-super', Auth::user()); 

        $preset->delete();
        return redirect()->route('presets.index')->with('message', 'Preset deleted successfully');

    }
}
