<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Client;
use App\Order;
use App\Product;
use App\Store;
use App\ProductReview;
use App\SubCategory;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Lava;
use Khill\Lavacharts\Lavacharts;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view-super', Auth::user());
        $admins_count  = User::where('role', 1)->count();
        $clients_count = Client::count();

        $stores_count  = Store::count();
        $stores_ver_count=Store::where('status',Store::STATUS_VERIFIED)->count();
        $stores_not_ver_count=Store::where('status',Store::STATUS_NOT_VERIFIED)->count();
        $stores_sus_count=Store::where('status',Store::STATUS_SUSPENDED)->count();

        $reviews_count = ProductReview::count();
        $categories_count    = Category::count();
        $sub_categories_count    = SubCategory::count();

        $products_count=Product::count();
        $product_online_count=Product::where('status',Product::STATUS_ONLINE)->count();
        $product_offline_count=Product::where('status',Product::STATUS_OFFLINE)->count();
        $product_rejected_count=Product::where('status',Product::STATUS_REJECTED)->count();
        $product_inreview_count=Product::where('status',Product::STATUS_INREVIEW)->count();

        $orders_count=Order::count();
        $orders_requests_count=Order::where('status',Order::STATUS_REQUESTED)->count();
        $orders_inprocess_count=Order::where('status',Order::STATUS_PROCESSING)->count();
        $orders_inshipping_count=Order::where('status',Order::STATUS_SHIPPING)->count();
        $orders_shipped_count=Order::where('status',Order::STATUS_SHIPPED)->count();

        $areachart = $this->render_chart_user_per();
        $areachart2 = $this->render_chart_stores_per();
        $areachart3 = $this->render_chart_client_per();
        $areachart4=$this->render_chart_orders_per();
        $name = 'dashboard';
        return view('dashboard', compact('admins_count', 'clients_count',
        'stores_count','stores_ver_count','stores_not_ver_count','stores_sus_count','reviews_count', 'categories_count','sub_categories_count','products_count',
        'product_online_count','product_offline_count','product_rejected_count','product_inreview_count','orders_count','orders_requests_count','orders_inprocess_count',
        'orders_inshipping_count','orders_shipped_count',
        'name','areachart','areachart2','areachart3','areachart4'));
    }
    private function render_chart_stores_per()
    {
        $population = \Lava::DataTable();
        $population->addStringColumn('User Pre month')
        ->addNumberColumn('Stores');
        for($i=6 ;$i>=0 ; $i--){
            $population ->addRow([Carbon::now()->subMonths($i)->format('F'),
            Store::whereMonth('created_at','=',Carbon::now()->subMonths($i))->get()->count() ]);
        }
           return $areachart2= \Lava::AreaChart('Stores', $population, [
            'title' => 'Stores Per Month',
            'areaOpacity'        => 0.1,
            'pointSize'          => 5,
            'colors'=> ['#00ACED'],
            'legend' => [
                'position' => 'top'
            ]
            ]);
        }
    private function render_chart_orders_per()
    {

        $population = \Lava::DataTable();
        $population->addStringColumn('Order Pre month')
        ->addNumberColumn('All');
        for($i=6 ;$i>=0 ; $i--){
            $population ->addRow([Carbon::now()->subMonths($i)->format('F'),
            Order::whereMonth('created_at','=',Carbon::now()->subMonths($i))->get()->count() ]);
        }
        return $areachart4= \Lava::AreaChart('Orders', $population, [
        'title' => 'Orders Per Month',
        'areaOpacity'        => 0.1,
        'pointSize'          => 5,
        'colors'=> ['#00ACED'],
        'legend' => [
            'position' => 'top'
        ]
        ]);
        }
        private function render_chart_user_per()
    {

        $population = \Lava::DataTable();
        $population->addStringColumn('User Pre month')
        ->addNumberColumn('All');
        for($i=6 ;$i>=0 ; $i--){
            $population ->addRow([Carbon::now()->subMonths($i)->format('F'),
            User::whereMonth('created_at','=',Carbon::now()->subMonths($i))->get()->count() ]);
        }
        return $areachart= \Lava::AreaChart('Population', $population, [
        'title' => 'All Per Month',
        'areaOpacity'        => 0.1,
        'pointSize'          => 5,
        'colors'=> ['#00ACED'],
        'legend' => [
            'position' => 'top'
        ]
        ]);
        }
    private function render_chart_client_per()
    {
        $population = \Lava::DataTable();
        $population->addStringColumn('Clients Pre month')
        ->addNumberColumn('Clients');
        for($i=6 ;$i>=0 ; $i--){
            $population ->addRow([Carbon::now()->subMonths($i)->format('F'),
            Client::whereMonth('created_at','=',Carbon::now()->subMonths($i))->get()->count() ]);
        }
        return $areachart3= \Lava::AreaChart('Clients', $population, [
        'title' => 'Clients Pre month',
        'areaOpacity'        => 0.1,
        'pointSize'          => 5,
        'colors'=> ['#00ACED'],

        'legend' => [
            'position' => 'top'
        ]
        ]);
        }

}
