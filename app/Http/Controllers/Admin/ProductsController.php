<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Misc\Helpers\Filters;
use App\Http\Requests\Admin\AddProductImageRequest;
use App\Http\Requests\Admin\CreateProductRequest;
use App\Http\Requests\Admin\UpdateProductRequest;
use App\Product;
use App\ProductImage;
use App\ProductSpec;
use App\Store;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Store $store )
    {
        //
        $products  = Product::where('store_id', $store->id);
        $products = Filters::searchBy($products, ['name' => $request->query('name')]);
        if ($request->query('status') && $request->query('status') != 'none') {
            $products = $products->where('status', $request->query('status'));
        }
        if ($request->query('sub_category') && $request->query('sub_category') != 'none') {
            $products = $products->where('sub_category_id', $request->query('sub_category'));
        }

        $products = $products->sortable()->paginate(8);
        $name = 'products';
        $data = $products;
        $subCategories = SubCategory::whereHas('products')->get();
        return view('stores.products.index', compact('store', 'products', 'name', 'data','subCategories'));


    }
    public function all_index(Request $request)
    {
        //
        $products  = new Product();
        $products = Filters::searchBy($products, ['name' => $request->query('name')]);
        if ($request->query('store_name') && $request->query('store_name') != 'none') {
            $stores=new Store();
            $stores=Filters::searchBy($stores, ['name' => $request->query('store_name')])->pluck('id');
            $products = $products->whereIn('store_id', $stores);
        }

        if ($request->query('status') && $request->query('status') != 'none') {
            $products = $products->where('status', $request->query('status'));
        }
        if ($request->query('sub_category') && $request->query('sub_category') != 'none') {
            $products = $products->where('sub_category_id', $request->query('sub_category'));
        }

        $products = $products->sortable()->paginate(10);
        $name = 'products';
        $data = $products;
        $subCategories = SubCategory::whereHas('products')->get();
        return view('products.index', compact( 'products', 'name', 'data','subCategories'));


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        //
        $storeID = $store->id;
        $categories=Category::whereHas('subcategories')->get();
        $subs = SubCategory::all();
        $name    = 'products';

        return view('stores.products.create', compact('store', 'storeID', 'name','categories','subs'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request ,Store $store)
    {
        //
        $this->authorize('view', $store);
        $product = new product();
        $product->store_id = $store->id;
        $product->name     = $request->name;
        $product->image    = FileHandler::store_img($request->image, 'products_images');;
        $product->description    = $request->input('description');
        $product->price     = $request->price;
        $product->sub_category_id     = $request->sub_category;
        $product->stock     = $request->in_stock;
        $product->status    =Product::STATUS_DRAFT;
        $product->save();
        if (isset($request->brand)){
            $spec=new ProductSpec();
            $spec->product_id   =$product->id;
            $spec->title        ='Brand';
            $spec->value        =$request->brand;
            $spec->save();
        }
        if (isset($request->modal_number)){
            $spec=new ProductSpec();
            $spec->product_id   =$product->id;
            $spec->title        ='Modal Number';
            $spec->value        =$request->modal_number;
            $spec->save();
        }
        if (isset($request->external_product_id)){
            $spec=new ProductSpec();
            $spec->product_id   =$product->id;
            $spec->title        ='External Product ID';
            $spec->value        =$request->external_product_id;
            $spec->save();
        }
        if (isset($request->ean_13)){
            $spec=new ProductSpec();
            $spec->product_id   =$product->id;
            $spec->title        ='EAN-13';
            $spec->value        =$request->ean_13;
            $spec->save();
        }
        if (isset($request->package_height)){
            $spec=new ProductSpec();
            $spec->product_id   =$product->id;
            $spec->title        ='Package Height';
            $spec->value        =$request->package_height;
            $spec->save();
        }
        if (isset($request->package_weight)){
            $spec=new ProductSpec();
            $spec->product_id   =$product->id;
            $spec->title        ='Package Weight';
            $spec->value        =$request->package_weight;
            $spec->save();
        }
        if (isset($request->package_width)){
            $spec=new ProductSpec();
            $spec->product_id   =$product->id;
            $spec->title        ='Package Width';
            $spec->value        =$request->package_width;
            $spec->save();
        }
        if (isset($request->package_thickness)){
            $spec=new ProductSpec();
            $spec->product_id   =$product->id;
            $spec->title        ='Package Thickness';
            $spec->value        =$request->package_thickness;
            $spec->save();
        }

        return redirect()->route('products.index', $store->id)->with('message', 'Product created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store ,Product $product)
    {
        //
        $this->authorize('view', $store);

        $product_images=ProductImage::where('product_id',$product->id)->limit(5)->get();
        return view('stores.products.show', compact('store', 'product','product_images'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request , Store $store , Product $product)
    {
        //
        $this->authorize('view', $store);

        $name    = 'products';
        $categories=Category::whereHas('subcategories')->get();
        $subs = SubCategory::all();

        return view('stores.products.edit', compact('store', 'product','name','categories','subs'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Store $store , Product $product)
    {
        //
        $this->authorize('view', $store);

        if($product->status!=Product::STATUS_INREVIEW ||$product->status!=Product::STATUS_ONLINE){

        if (isset($request->name))
            $product->name = $request->name;
        if (isset($request->description))
        $product->description = $request->input('description');
        if (isset($request->price))
        $product->price = $request->price;

        if (isset($request->stock))
        $product->stock = $request->in_stock;
        if ($request->hasFile('image')) {
            //upload it
            Storage::disk('products_images')->delete($product->image);
            $avatar =FileHandler::store_img($request->image, 'products_images');
            //delete old one
            if (isset($request->image))
            $product->image = $avatar;
        }
        $product->status=Product::STATUS_DRAFT;
        $product->save();
    }else{
        return redirect()->route("products.show",[$store->id,$product->id]) ->with('error', 'You can edit product while in review or online');
    }

        return redirect()->route("products.show",[$store->id,$product->id]) ->with('message', 'Product Updated successfully');
    }
    public function to_review(Request $request, Store $store , Product $product)
    {

        $this->authorize('view', $store);
        $product->status=Product::STATUS_INREVIEW;
        $product->save();

        return redirect()->route("products.show",[$store->id,$product->id]) ->with('message', 'Product Submited Wait For Admin Approval');

    }
    public function to_offline(Request $request, Store $store , Product $product)
    {

        $this->authorize('view', $store);
        $product->status=Product::STATUS_OFFLINE;
        $product->save();

        return redirect()->route("products.show",[$store->id,$product->id]) ->with('message', 'Product becames Offline ');

    }
    public function to_approve(Request $request, Store $store , Product $product)
    {

        $this->authorize('view-super', Auth::user());
        $product->status=Product::STATUS_ONLINE;
        $product->save();

        return redirect()->route("products.show",[$store->id,$product->id]) ->with('message', 'Product Approved');

    }    public function to_reject(Request $request, Store $store , Product $product)
    {

        $this->authorize('view-super', Auth::user());
        $product->status=Product::STATUS_REJECTED;
        $product->save();

        return redirect()->route("products.show",[$store->id,$product->id]) ->with('message', 'Product Rejected  ');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,Store $store, Product $product)
    {
        //
        $this->authorize('view', $store);

        if($product->status==Product::STATUS_DRAFT){
            $product->delete();
            return redirect()->back()->with('message', 'Product deleted successfully');
        }
        else{
            $product->status=Product::STATUS_OFFLINE;
            $product->save();
            return redirect()->back()->with('message', 'Product deactivted successfully');

        }





    }

    public function update_image(Request $request ,Store $store, Product $product)
    {
        //
        if ($request->hasFile('image')) {
            //upload it
            Storage::disk('products_images')->delete($product->image);
            $avatar =FileHandler::store_img($request->image, 'products_images');
            //delete old one
            if (isset($request->image))
            $product->image = $avatar;
            $product->save();
        }
        return redirect()->back()->with('message', 'Product info updated');

    }
    public function add_image(AddProductImageRequest $request ,Store $store, Product $product)
    {
        //
        if ($request->hasFile('added_image')) {
            //upload it
            $avatar =FileHandler::store_img($request->added_image, 'products_images');
            $product_image=new ProductImage();
            $product_image->image = $avatar;
            //for future
            $product_image->small_image = $avatar;
            $product_image->product_id = $product->id;
            $product_image->save();
        }
        return redirect()->back()->with('message', 'Product info updated');

    }

    public function remove_image(Request $request ,Store $store , Product $product ,ProductImage $product_image)
    {
        //
        $this->authorize('view', $store);

        $product_image->delete();

        return redirect()->back()->with('message', 'Product Image deleted ');

    }
}
