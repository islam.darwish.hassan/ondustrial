<?php

namespace App\Http\Controllers\Admin;

use App\Offer;
use App\Store;
use Illuminate\Http\Request;
use App\Http\Misc\Helpers\Filters;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\FileHandler;
use App\Http\Requests\Admin\CreateOfferRequest;
use App\Http\Requests\Admin\CreateTagRequest;
use App\StoreTag;
use App\Tag;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class StoreTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Store $store)
    {
         $tags  = $store->tags;
        $name = 'tags';
        $data=[];
        return view('stores.tags.index', compact('store', 'tags', 'name','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store)
    {
        $storeID = Store::where('id', $store->id)->get();
        $name    = 'tags';

        return view('stores.tags.create', compact('store', 'storeID', 'name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTagRequest $request, Store $store)
    {
        $this->authorize('view', $store); 

        $tag = Tag::where('name',$request->name)->firstOrCreate(['name'=>$request->name]);
        StoreTag::where('tag_id',$tag->id)->firstOrCreate(['store_id'=>$store->id,'tag_id'=>$tag->id]);

        return redirect()->route('tags.index', $store->id)->with('message', 'Tag added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        $this->authorize('view', $store); 

        // $storeID = Store::where('id', $store->id)->get();
        // $name = 'offers';

        // return view('stores.offers.edit', compact('offer', 'storeID', 'name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store, Tag $tag)
    {
        $this->authorize('view', $store); 
         $store_tag= StoreTag::where(['store_id'=>$store->id,'tag_id'=>$tag->id])->first();
        $store_tag->delete();
        return redirect()->back()->with('message', 'tag removed successfully');
    }
}
