<?php

namespace App\Http\Controllers\Admin;

use App\TagsPreset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreatePresetParamRequest;
use App\Http\Requests\Admin\UpdatePresetRequest;
use App\Preset;
use App\PresetsParameter;
use App\TagsPresetsParameter;
use Illuminate\Support\Facades\Auth;

class TagsPresetsParametersController  extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $this->authorize('view-super', Auth::user()); 

        return $presetsParams=PresetsParameter::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
        $this->authorize('view-super', Auth::user()); 
        $preset=TagsPreset::where('id',$id)->first();
        $name="tags_params";
        return view('tags_presets.params.create', compact('name','preset'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePresetParamRequest $request,$id)
    {
        //
        $this->authorize('view-super', Auth::user()); 
        $preset=TagsPreset::where('id',$id)->first();

        $params = new TagsPresetsParameter();
        $params->name     = $request->input('name');
        $params->desc     = $request->input('desc');
        $params->tags_preset_id       = $preset->id;

        $params->save();

        return redirect()->route('tags_presets.show',$preset->id)->with('message', 'Param of '.$preset->name.  ' created successfully');
    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $this->authorize('view-super', Auth::user()); 

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id , $id_preset)
    {
        //
        $this->authorize('view-super', Auth::user()); 
        $preset=TagsPreset::where('id',$id)->first();
        $param=TagsPresetsParameter::where('id',$id_preset)->first();

        $name ='tags_params';

        return view('tags_presets.params.edit', compact('preset', 'name','param'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePresetRequest $request, $id , $id_preset)
    {
        //
        $this->authorize('view-super', Auth::user()); 
        $preset=TagsPreset::where('id',$id)->first();
        $param=TagsPresetsParameter::where('id',$id_preset)->first();

        
        if (isset($request->name))
            $param->name = $request->name;

        if (isset($request->desc))
            $param->desc = $request->desc;

        $param->save();

        return redirect()->route('tags_presets.show',$preset->id)->with('message', 'Param info updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id , $id_preset )
    {
        //
        $this->authorize('view-super', Auth::user()); 
        $preset=TagsPreset::where('id',$id)->first();
        $param=TagsPresetsParameter::where('id',$id_preset)->first();

        $param->delete();
        return redirect()->route('tags_presets.show',$preset->id)->with('message', 'Param of '.$preset->name.  ' deleted successfully');

    }
}
