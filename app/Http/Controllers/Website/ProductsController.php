<?php

namespace App\Http\Controllers\Website;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Filters;
use App\Product;
use App\ProductImage;
use App\Store;
use App\SubCategory;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,Product $product)
    {
        //
        $products = $product;

        $products = Filters::searchBy($products, ['name' => $request->query('name')]);
        $products=$products->where('status',Product::STATUS_ONLINE);
        if($request->has('price_min_range')&&$request->has('price_max_range')){
            $products=$products->whereBetween('price',[$request->price_min_range,$request->price_max_range]);
        }
        if ($request->has('category')) {

            $category=Category::where('id',$request->category)->first();
            $sub_categories=SubCategory::where('category_id',$category->id)->pluck('id');
            $products_categories =Product::whereIn('sub_category_id',$sub_categories)->get();
            $products=$products->whereIn('id',$products_categories)->orderBy('created_at','desc');

        }else{
            $products=$products;

        };
        if($request->has('store')){
             $products=$products->where('store_id',$request->store);
        }
        if($request->has('sub_category')){
            $products=$products->where('sub_category_id',$request->sub_category);
       }

        if ($request->query('filter') && $request->query('filter') != 'none') {
            if($request->query('filter')=="1"){
                $products=$products->orderBy('price','asc');
            }else if($request->query('filter')=="2"){
                $products=$products->orderBy('price','desc');

            }else if($request->query('filter')=="3"){
                $products=$products->orderBy('created_at','desc');

            }
        }
         $products=$products->paginate(8);
         $products_max_in_total=Product::orderBy('price','desc')->value('price');
         $products_min_in_total=Product::orderBy('price','asc')->value('price');
         return view('website.products.index',compact('products','products_max_in_total','products_min_in_total'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request ,Product $product)
    {
        //
        $similars=Product::where('store_id',$product->store->id)->orderBy('created_at','desc')->where('status',Product::STATUS_ONLINE)->inRandomOrder()->get();
        $message ="This Item is not currently avaliable!";
        if ($product->status != Product::STATUS_ONLINE) return view('website.products.not_avaliable',compact('similars','message'));

        $product_images=ProductImage::where('product_id',$product->id)->limit(5)->get();
        return view('website.products.show',compact('product','similars','product_images'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function autocomplete(Request $request)
    {
        $datas = Product::where('status',Product::STATUS_ONLINE)->select("name")
        ->where("name","LIKE","%{$request->input('query')}%")
        ->get();
    $dataModified = array();
     foreach ($datas as $data)
     {
       $dataModified[] = $data->name;
     }

    return response()->json($dataModified);
        return response()->json($data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
