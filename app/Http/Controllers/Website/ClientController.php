<?php

namespace App\Http\Controllers\Website;

use App\Address;
use Illuminate\Support\Facades\Auth;

use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests\Website\AddNewClientRequest;
use App\User;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    //

    public function create()
    {
        //
        return view('website.client.create');
    }

    public function store(AddNewClientRequest $request)
    {
        //
        $user = new User();
        $user->email    = $request->input('email');
        $user->password = bcrypt($request->password);
        $user->role     = 2;
        $user->save();

        $client= new Client();
        $client->user_id      = $user->id;
        $client->name         = $request->name;
        $client->gender       = $request->gender;
        $client->nation_id    = 66;
        $client->phone        = $request->phone;
        $client->save();
        $address = new Address();
        $address->user_id   =$user->id;
        $address->address   =$request->address;
        $address->city_id   =$request->city_id;
        $address->save();

        return redirect()->route('web.login')->with('message', 'Account created successfully');

    }
}
