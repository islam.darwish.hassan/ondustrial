<?php

namespace App\Http\Controllers\Website;

use App\Banner;
use App\Client;
use Illuminate\Http\Request;
use App\Mail\SendQuestionMail;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\Store;
use Illuminate\Support\Facades\Mail;
class WebsiteController extends Controller
{
    public function get_index()
    {

        $products=Product::orderBy('ordered','desc')->where('status',Product::STATUS_ONLINE)->limit(10)->get();
        $arrivals=Product::orderBy('created_at','desc')->where('status',Product::STATUS_ONLINE)->limit(10)->get();

        $clients_count=Client::count();
        $stores_count=Store::count();
        $orders_count=Order::count();
        $products_count=Product::where('status',Product::STATUS_ONLINE)->count();
        $banners=Banner::orderBy('order')->get();
        return view('website.home',compact('products','arrivals','clients_count','stores_count','products_count','orders_count','banners'));
    }

    public function get_product()
    {
        return view('website.product');
    }

    public function get_policy()
    {
        return view('website.policy');
    }

    public function get_terms()
    {
        return view('website.terms');
    }
    public function get_policyAr()
    {
        return view('website.policyAr');
    }
    public function get_termsAr()
    {
        return view('website.termsAr');
    }

}
