<?php

namespace App\Http\Controllers\Website;

use App\Address;
use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests\Website\CheckoutRequest;
use App\Order;
use App\OrderProducts;
use App\Product;
use App\Shipment;
use App\ShipmentProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $client=Client::where('user_id',Auth::user()->id)->first();
        $orders=Order::where('client_id',$client->id)->with('shipments')->where('status','!=',Order::STATUS_DRAFT)->orderBy('created_at','desc')->paginate(1);
        return view('website.orders.index',compact('orders'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function place_order(Request $request,Order $order)
    {
        //

        $order->status          =Order::STATUS_REQUESTED;
        foreach($order->shipments as $shipment)
        {
            $shipment->status= Shipment::STATUS_REQUESTED;
            $shipment->save();
        }
        $order->save();

        \Cart::clear();
        return  redirect()->route('web.myorders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get_checkout(CheckoutRequest $request)
    {
        //
        $client=Client::where('user_id',Auth::user()->id)->first();
        if(\Cart::getTotalQuantity()<0){
            return redirect('index');
        }
        $detailed_address=Address::where('id',$request->address)->first();

        $order =new Order();
        $order->hash_code   =  str_random(12).rand(1,10000);
        $order->client_id   =   $client->id;
        $order->total_price=\Cart::getTotal();
        $order->delivery_option= 1;
        $order->status      =  Order::STATUS_DRAFT;
        $order->address_id  =  $request->address;
        $order->address_log =  'City: '.$detailed_address->city->en_name.' / Address: '.$detailed_address->address ." / Street: ". $detailed_address->street ." / Building No: " . $detailed_address->building . " / Floor: ".$detailed_address->floor ." / Landline : ".$detailed_address->landline." / Landmark : ".$detailed_address->landmark ." / Apartment : " .$detailed_address->apartment ." / Shipping Notes : " .$detailed_address->shipping_note ." .";

        //get address fees
        $address=Address::where('id',$request->address)->first();
        $order->delivery_fees=$address->city->delivery_fees;
        //shelha b3den
        $order->save();

        foreach(\Cart::getContent() as $item){
            $shipment =Shipment::where('order_id',$order->id)->where('store_id',$item->associatedModel->store_id)->firstOrNew(
                ["hash_code"=> str_random(12).rand(1,10000),
                "order_id"=>$order->id,
                "status"=>Shipment::STATUS_DRAFT,
                "store_id"=>$item->associatedModel->store->id]
            );
            $shipment->save();
            $product =new ShipmentProduct();
            $product->product_id = $item->associatedModel->id;
            $product->shipment_id = $shipment->id;
            $product->product_price =$item->getPriceSumWithConditions();
            $product->product_name=$item->associatedModel->name;
            $product->product_log=$item->associatedModel->description;
            $product->qty=$item->quantity ;
            if($product->original_product->stock <$item->quantity){
                \Cart::remove($item->id);
                return redirect()->route('index')->with("modal_error","Cart has item that out of stock ! ");
            }
             $original= Product::where('id',$product->original_product->id)->first();
            $original->stock=$original->stock-$item->quantity;
            $original->save();
            if($original->stock<=0){
                $original->status=Product::STATUS_OFFLINE;
                $original->save();

            }
            $product->save();
            //cal price of shipments
            $shipment->total_price=$shipment->total_price+$item->getPriceSumWithConditions();
            $shipment->save();

        }

        return view('website.orders.checkout',compact('order'));
       }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
