<?php

namespace App\Http\Controllers\Website\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mail;
use Illuminate\Support\Str;

class ForgetPasswordController extends Controller
{
    //
    public function index()
    {
        //
        return view('website.auth.forget');
    }
    public function forget(Request $request)
    {
        //
        $user=User::where('email',$request->email)->first();
        if($user==null){
            return redirect()->back()->with(['error'=>'Email Address Not Exists! ']);

        }else{
            $token = Str::random(60);

            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);
            $user->sendPasswordResetNotification($token);
                return redirect()->back()->with(['message'=>'Reset code sent to your email']);

        }

        return view('website.auth.forget');
    }
    public function showResetForm($token)
    {
        return view('website.auth.reset', compact('token'));
    }


    public function reset(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|min:6|max:190'
       ]);

       $reset_data = DB::table('password_resets')->where('token', $request->token)->first();

       $user = User::where('email', $reset_data->email)->first();
       $user->password = bcrypt($request->password);
       $user->save();

       DB::table('password_resets')->where('token', $request->token)->delete();

       return redirect()->route('web.login')->with(['message'=>'Password Reset Successfully!']);
    }


}
