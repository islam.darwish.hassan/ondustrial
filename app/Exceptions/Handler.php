<?php

namespace App\Exceptions;

use Exception;
use App\Http\Misc\Helpers\Errors;
use App\Http\Misc\Traits\WebServiceResponse;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    use WebServiceResponse;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson())
        {
            if($exception instanceof ModelNotFoundException)
                return $this->error_response(Errors::TESTING);
            else if($exception instanceof AuthorizationException)
                return $this->error_response(Errors::UNAUTHORIZED);
            else if($exception instanceof AuthenticationException)
                return $this->error_response(Errors::UNAUTHENTICATED);
            else if($exception instanceof ValidationException)
                return $this->error_response($this->print_validation_errors($exception->validator->errors()->all()));
            // else
            //     return $this->error_response(Errors::GENERAL);
        }
        return parent::render($request, $exception);
    }



    private function print_validation_errors($errors)
    {
        $errors_txt = "";
        foreach($errors as $message)
            $errors_txt .= $message."\n";
        return $errors_txt;
    }
}
