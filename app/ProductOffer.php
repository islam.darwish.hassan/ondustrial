<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOffer extends Model
{
    //
    protected $fillable=["offer","product_id"];
}
