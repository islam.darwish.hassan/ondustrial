<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishlistProduct extends Model
{
    //
    protected $fillable = ['wishlist_id', 'product_id'];
    public function original_product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

}
