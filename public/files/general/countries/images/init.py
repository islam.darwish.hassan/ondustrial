# -*- coding: utf-8 -*-
import json

f = open("demofile.txt", "w")
countries_en = open('countries_en.json').read()
data_en = json.loads(countries_en)

countries_ar = open('countries_ar.json', encoding='utf-8').read()
data_ar = json.loads(countries_ar)

all_data = []
for i in range(len(data_en)):
	all_data.append({"country_name_en" : data_en[i]['name'], "country_name_ar" : data_ar[i]['name'], "country_flag" : data_en[i]['alpha2']+'.png'})
