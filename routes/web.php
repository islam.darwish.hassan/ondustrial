<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Website
Route::group(['namespace' => 'Website'], function () {

    Route::get('/', 'WebsiteController@get_index')->name('index');
    Route::get('/product', 'WebsiteController@get_product')->name('product');
    Route::get('policy', 'WebsiteController@get_policy')->name('policy');
    Route::get('terms', 'WebsiteController@get_terms')->name('terms');
    //products
    Route::get('products', 'ProductsController@index')->name('products.web.index');
    Route::get('products/{product}', 'ProductsController@show')->name('products.web.show');
    Route::get('autocomplete', 'ProductsController@autocomplete')->name('products.web.autocomplete');

    //category
    Route::get('categories/{category}/products', 'ProductsController@index_category')->name('products.categories');
    Route::get('categories/{category}', 'CatgoriesController@show')->name('categories.web.show');

    //cart
    Route::get('cart','CartContoller@index')->name('cart.index');
    Route::post('cart/{product}/store','CartContoller@store')->name('cart.store');
    Route::delete('cart/empty','CartContoller@empty')->name('cart.empty');
    Route::delete('cart/{product}/remove','CartContoller@remove')->name('cart.remove');
    Route::get('register', 'ClientController@create')->name('web.client.create');
    Route::post('register/store', 'ClientController@store')->name('web.client.store');

    // Auth::routes()
    Route::group(['namespace' => 'Auth'], function() {
        Route::get('login', 'LoginController@showLoginForm')->name('web.login');
        Route::post('login', 'LoginController@login')->name('web.login.store');
        Route::post('logout', 'LoginController@logout')->name('web.logout');
        Route::get('forget','ForgetPasswordController@index')->name('web.forget');
        Route::post('forget','ForgetPasswordController@forget')->name('web.forget_password');
        Route::get('password/reset/{token}', 'ForgetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'ForgetPasswordController@reset')->name('password.update');

    });

    Route::group(['middleware' => 'auth.buyer'], function () {
        //addresses
        Route::get('MyAddresses/index', 'AddressesController@index')->name('web.myaddresses.index');
        Route::get('MyAddresses/create', 'AddressesController@create')->name('web.myaddresses.create');
        Route::get('MyAddresses/{address}/edit', 'AddressesController@edit')->name('web.myaddresses.edit');
        Route::put('MyAddresses/{address}/update', 'AddressesController@update')->name('web.myaddresses.update');
        Route::delete('MyAddresses/{address}/destroy', 'AddressesController@destroy')->name('web.myaddresses.destroy');
        Route::post('MyAddresses/store', 'AddressesController@store')->name('web.myaddresses.store');

        //orders
        Route::get('MyOrders', 'OrdersController@index')->name('web.myorders.index');
        Route::get('MyOrders/{order}/order', 'OrdersController@show')->name('web.myorders.show');




        Route::get('MyWishlists', 'WishlistsController@index')->name('web.mywishlists');
        Route::get('MyWishlists/create', 'WishlistsController@create')->name('web.mywishlists.create');
        Route::post ('MyWishlists/add', 'WishlistsController@add')->name('web.mywishlists.add');
        Route::put ('MyWishlists/update', 'WishlistsController@update')->name('web.mywishlists.update');
        Route::delete('MyWishlists/{wishlist_product}/remove', 'WishlistsController@remove')->name('web.mywishlists.remove');

        Route::delete('MyWishlists/{wishlist}/destroy', 'WishlistsController@destroy')->name('web.mywishlists.destroy');
        Route::post('MyWishlists/store', 'WishlistsController@store')->name('web.mywishlists.store');

        Route::post('checkout', 'OrdersController@get_checkout')->name('web.checkout');
        Route::post('checkout/place_order/{order}', 'OrdersController@place_order')->name('web.orders.place_order');

    });

});


//Admin
Route::group(['prefix' => 'admin'], function () {


    // Auth::routes()
    Route::group(['namespace' => 'Auth'], function() {
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'ConfirmPasswordController@login');
        Route::post('logout', 'LoginController@logout')->name('logout');
        Route::post('reset', 'ResetPasswordController@reset')->name('password.request');
    });

    Route::group(['middleware' => 'auth', 'namespace' => 'Admin'], function () {

        // Dashboard
        Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');
        // All
        Route::resource('users', 'UserController');
        Route::post('users/create/fetch', 'UserController@fetch_category_data')->name('category.fetch');

        // Clients
        Route::resource('clients', 'ClientController');
        //Cities
        Route::resource('cities', 'CityController');
        // Stores
        Route::resource('stores', 'StoreController');
        Route::resource('stores/{store}/tags', 'StoreTagController');
        Route::put('stores/{store}/cover', 'StoreController@update_cover')->name('stores.update_cover');
        Route::resource('stores/{store}/offers', 'StoreOfferController');
        // products
        Route::resource('stores/{store}/products', 'ProductsController');
        Route::resource('stores/{store}/products/{product}/specs', 'SpecsController');
        Route::get('products','ProductsController@all_index')->name('all_products.index');
        Route::put('stores/{store}/products/{product}/to_review','ProductsController@to_review')->name('products.to_review');
        Route::put('stores/{store}/products/{product}/to_approve','ProductsController@to_approve')->name('products.to_approve');
        Route::put('stores/{store}/products/{product}/to_reject','ProductsController@to_reject')->name('products.to_reject');
        Route::put('stores/{store}/products/{product}/to_offline','ProductsController@to_offline')->name('products.to_offline');

        //offers
        Route::resource('stores/{store}/products/{product}/offers', 'ProductOffersController');
        ////
        Route::put('stores/{store}/products/{product}/update_image', 'ProductsController@update_image')->name('products.update_image');
        Route::post('stores/{store}/products/{product}/add_image', 'ProductsController@add_image')->name('products.add_image');
        Route::delete('stores/{store}/products/{product}/product_image/{product_image}', 'ProductsController@remove_image')->name('products.remove_image');
        //Orders
        Route::resource('orders', 'OrderController');
        Route::put('orders/{order}/confirm','OrderController@confirm')->name('orders.confirm');
        Route::put('orders/{order}/shipped','OrderController@shipped')->name('orders.shipped');
        Route::get('orders/{order}/print','PrintController@prnpriview')->name('orders.print');

        Route::put('orders/{order}/in_shipping','OrderController@in_shipping')->name('orders.in_shipping');

        //shipments
        Route::get('shipments','ShipmentsController@index')->name('shipments.index');
        Route::put('shipments/{shipment}','ShipmentsController@confirm')->name('shipments.confirm');
        //reviews
        Route::resource('stores/{store}/reviews', 'ProductsController');
        //Banners
        Route::resource('banners','BannerController');
        // Categories
        Route::resource('categories', 'CategoryController');
        Route::resource('categories/{category}/subs', 'SubCategoryController');
        // Notifications
        Route::resource('notifications', 'NotificationController', ['only' => ['index', 'create', 'store']]);
        //Tags Presets
        Route::resource('tags_presets', 'TagsPresetsController');
        Route::resource('tags_presets/{tags_preset}/tags_params', 'TagsPresetsParametersController');


    });

});
