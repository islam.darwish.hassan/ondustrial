@extends('layouts.app')
@section('title', 'Edit ' . $preset->name . ": " .$param->name)


@section('content')
 <form class="form" action="{{ route($name. '.update', [$preset->id ,$param->id]) }}" method="post" enctype="multipart/form-data">
               @csrf


    @method('put')

    <div class="row">
        <div class="col-2 form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control" placeholder="Name "
                value="{{ $param->name }}">
        </div>
        <div class="col form-group">
            <label>Description</label>
            <textarea type="text" row="5" name="desc" class="form-control" placeholder="Description "
            value="{{ $param->desc }}"> {{ $param->desc }}</textarea>
        </div>

    </div>

    <button type="submit" class="btn btn-block btn-success bg-brandgreen ">Update</button>

</form>
<script>
    $('.custom-file-input').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
</script>
@endsection
