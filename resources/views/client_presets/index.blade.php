@extends('layouts.resource.index')
@section('create-btn')
<a href="{{ route($name . '.create') }}" class=" clr-blue p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')

    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block bg-blue mb-2">Filter</button>
            </div>
            <a href="{{ route($name. '.index') }}" class="small-header-bold"> X</a>

        </div>
    </form>

@endsection

@section('table-header')

    <th scope="col" style="width: 10%" >@sortablelink('id', '#')</th>
    <th scope="col" >@sortablelink('name','Name')</th>
    <th scope="col" >Description</th>
    <th scope="col"  >@sortablelink('params_count','Params')</th>
    <th scope="col"  >@sortablelink('created_at','Created At')</th>

    <th  scope="col" style="width: 10%">Operations</th>
@endsection



@section('table-body')

    @foreach ($presets as $preset)
        <tr>
            <td>{{ $preset->id }}</td>
            <td>{{ $preset->name }}</td>
            <td>{{ Str::limit($preset->desc ,30) }}</td>
            <td>{{ $preset->params_count}}</td>
            <td>{{Carbon\Carbon::parse($preset->created_at)->diffForHumans()}}</td>
            <td class="d-flex justify-content-center">
                <a href="{{ route('clients_params'.'.create', $preset->id) }}"
                    class="btn clr-blue "><i class=" fas fa-plus"></i></a>
                <a href="{{ route('client_presets'.'.show', $preset->id) }}"
                class="btn clr-black  "><i class="fas fa-eye"></i></a>
               <a href="{{ route('client_presets'.'.edit', $preset->id) }}"
                 class="btn clr-black "><i class="fas fa-edit"></i></a>
                   <form method="POST" action="{{ route('client_presets'.'.destroy', $preset->id) }}">
                      @csrf
                       {{ method_field('DELETE') }}
                       <button type="submit"
                        onclick="return confirm('Are you sure you want to delete this preset?')"
                        data-toggle="modal" data-target="#exampleModal"
                   class="btn  clr-blue  bg-transparent"><i class="fas fa-trash-alt "></i></button>
                   </form>
               </td>

        </tr>
    @endforeach
    @section('table-footer')
    <p class="table-footer">Results :  {{$data->total()}}</p>
@endsection

@endsection
