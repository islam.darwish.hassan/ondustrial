@extends('layouts.app')
@section('title', $client->first_name." ".$client->last_name)

@section('content')
<!--Statistics-->
<div class="row">
  <div class="col ">
    <div class="card bg-blue mb-2 flex-fill ">
      <div class="card-header  d-flex justify-content-center">
        <i class="fab fas fa-street-view text-white my-3 display-4"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <div class="text-vlarge text-white">{{$client->orders->count()}}</div>
          <div class="text-uppercase text-white small">Orders</div>
        </div>
      </div>
    </div>
  </div>
  <div class="col" >
    <div class="card bg-blue mb-2 flex-fill ">
      <div class="card-header  d-flex justify-content-center">
        <i class="fas fa-pencil-alt text-white my-3 display-4"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <div class="text-vlarge text-white">{{$client->reviews->count()}}</div>
          <div class="text-uppercase text-white small">Reviews</div>
        </div>
      </div>
    </div>
  </div>
<div class="col" >
    <div class="card bg-blue mb-2  flex-fill">
      <div class="card-header  d-flex justify-content-center">
        <i class="fas fa-clipboard-list text-white my-3 display-4"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <div class="text-vlarge text-white">{{$wishlists->count()}}</div>
          <div class="text-uppercase text-white small">Wishlists</div>
        </div>
      </div>
    </div>
  </div>

  </div>
</div>
</div>

<!--end of Statistics-->
<!--extra Info-->
<div class="row justify-content-center d-flex">
  @if($client->reviews->count()>0)
  <div class="col">
    <div class="card border-white">
      <div class="card-header bg-blue text-white"><b>Reviews</b></div>
      <table class="table  table-bordered  table-hover table-sm text-center  table-center ">
        <thead class="thead-light  ">
          <tr>
            {{-- <th scope="col">User</th> --}}
            <th scope="col">Partener</th>
            <th scope="col">Rate</th>
            <th scope="col">Tip</th>
            <th scope="col">Rate</th>
            <th scope="col">Created At</th>
            </tr>
        </thead>
        <tbody>
          @foreach($reviews as $review)
          <tr>
            <td><b><a href ={{route('stores.show',$review->store->id)}}>{{Str::limit($review->store->name,20)}} </a></b>
            </td>
            <td>{{$review->rate}}</td>
            <td>{{ Str::limit($review->tip ,30) }}</td>
            <td>{{ $review->rate}}</td>
            <td>{{Carbon\Carbon::parse($review->created_at)->diffForHumans()}}
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <a href="{{route('clientsreviews.index',$client->id)}}" class="clr-blue text-bold float-right mb-2" >See more -></a>
  </div>
  @endif
</div>
<div class="row justify-content-center d-flex">
  @if($wishlists->count()>0)
  <div class="col">
    <div class="card border-white">
      <div class="card-header bg-blue text-white"><b>Lists</b></div>
      <table class="table  table-bordered  table-hover table-sm text-center  table-center ">
        <thead class="thead-light  ">
          <tr>
            {{-- <th scope="col">User</th> --}}
            <th scope="col">Name</th>
            <th scope="col">Bookmarked Counts</th>
            <th scope="col">Bookmarked Parteners</th>
            <th scope="col">Created At</th>
            </tr>
        </thead>
        <tbody>
          @foreach($wishlists as $wishlist)
          <tr>
            <td>{{$wishlist->name}} </td>
            <td>{{$wishlist->products->count()}} </td>
            <td>
            @if($wishlist->products->count()<=0)
            Empty
            @else
            @foreach ($wishlist->products as $product)
              @if($loop->last)
              <b><a href ={{route('stores.show',$product->original_product->store->id)}}>{{Str::limit($product->original_product->store->name,10)}} .</a></b>
              @else
              <b><a href ={{route('stores.show',$product->original_product->store->id)}}>{{Str::limit($product->original_product->store->name,10)}} ,</a></b>
              @endif
              @endforeach
            @endif
            </td>
            <td>{{Carbon\Carbon::parse($wishlist->created_at)->diffForHumans()}}
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  {{-- <a href="{{route('collections.index',$client->id)}}" class="clr-blue text-bold float-right mb-2" >See more -></a> --}}
  </div>
  @endif
</div>
<div class="row justify-content-center d-flex">
@if($orders->count()>0)
<div class="col">
  <div class="card border-white">
    <div class="card-header bg-blue text-white"><b>Checkins</b></div>
    <table class="table  table-bordered  table-hover table-sm text-center  table-center ">
      <thead class="thead-light  ">
        <tr>
          {{-- <th scope="col">User</th> --}}
          <th scope="col">Sub</th>
          <th scope="col">Partener</th>
          <th scope="col">Partener Bio</th>
          <th scope="col">Location</th>
          <th scope="col">Created at </th>
          </tr>
      </thead>
      <tbody>
        @foreach($checkins as $checkin)
        <tr>
          <td><a href ={{route('stores.index','sub_category='.$checkin->store->sub_category->id)}}>
           <b> {{$checkin->store->sub_category->name}} </b></a> </td>
           <td><b><a href ={{route('stores.show',$checkin->store->id)}}>{{Str::limit($checkin->store->name,10)}} </a></b></td>
          <td>{{ Str::limit($checkin->store->bio ,30) }}</td>
          <td>{{$checkin->latitude}} , {{$checkin->longitude}} </td>
          <td>{{Carbon\Carbon::parse($checkin->created_at)->diffForHumans()}}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
<a href="{{route('checkins.index',$client->id)}}" class="clr-blue text-bold float-right mb-2" >See more -></a>
</div>
@endif
</div>

<!-- end of extra Info-->
@endsection
