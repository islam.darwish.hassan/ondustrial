@extends('layouts.resource.index')
@section('title', $client->first_name." ".$client->last_name."'s ".'collections')

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index',[$client->id]) }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                <input type="text" class="form-control mb-2 mr-sm-2" name="name" placeholder="Search in name ..." value="{{request()->query('name')}}">
            </div> 
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block bg-blue mb-2">Filter</button>
            </div>
            <a href="{{ route($name. '.index',[$client->id]) }}" class="small-header-bold"> X</a>

        </div>

    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-header')

<th scope="col">Sub</th>
<th scope="col">Store</th>
<th scope="col">Store Bio</th>
<th scope="col">Location</th>
<th scope="col">Created at </th>

@endsection

@section('table-body')
@foreach($checkins as $checkin)
<tr>
  <td><a href ={{route('stores.index','sub_category='.$checkin->store->sub_category->id)}}>
   <b> {{$checkin->store->sub_category->name}} </b></a> </td>
   <td><b><a href ={{route('stores.show',$checkin->store->id)}}>{{Str::limit($checkin->store->name,10)}} </a></b></td>
  <td>{{ Str::limit($checkin->store->bio ,30) }}</td>
  <td>{{$checkin->latitude}} , {{$checkin->longitude}} </td>
  <td>{{Carbon\Carbon::parse($checkin->created_at)->diffForHumans()}}
  </td>
</tr>
@endforeach
@section('table-footer')
    <p class="table-footer">Results :  {{$data->total()}}</p>
    @endsection

@endsection
