@extends('layouts.resource.edit')


@section('form')

<form method="POST" action="{{ route($name. '.update', $client->id) }}" enctype="multipart/form-data">

               @csrf


    @method('PUT')

    <div class="form2">
        <div class=" mb-3">
        <h3 class="section-header ">Additional information: </h3>
        <p class="section-subheader">
            This is required additional information for creating new user
            <code>Client</code>
        </p>
        </div>
        </div>
            <div class=" flex-fill ">
                <div class="row">
                    <div class="col form-group">
                        <label class="form-label">Name</label>
                        <input type="text" name="first_name" class="form-control" placeholder="First Name"
                            value={{isset($client) ? $client->first_name:old('first_name')}}>
                    </div>

                    <div class="col form-group">
                        <label class="form-label">Avatar</label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="validatedCustomFile">Replace avatar...</label>
                            <input type="file" name="avatar" id="custom-file-input-image" class="custom-file-input">
                        </div>
                    </div>
                    <div class="col-1 form-group ">
                        <label class="form-label">Current </label>
                    <img class=" pp-square " src="{{ $client->avatar }}" alt="avatar" >
                    </div>
                </div>
                <div class="row">
                    <div class=" col form-group">
                        <label class="form-label">BIO</label>
                        <textarea type="text" row="5" name="bio" class="form-control" placeholder="BIO"
                       value= {{isset($client) ? $client->bio:old('bio')}}>{{isset($client) ? $client->bio:old('bio')}}</textarea>
                    </div>
                    <div class="form-group col ">
                        <label class="form-label">Gender : </label>
                        <div>
                            <select class="form-control m-input " name="gender" ">
                                                    <option value="">Select a gender </option>
                                                    <option value="m" @if(request()->query('gender') == 'm') selected @endif>Male</option>
                                <option value="f" @if(request()->query('gender') == 'f') selected @endif>Female</option>
                            </select>

                        </div>
                    </div>

                    {{-- <div class="col-4 form-group">
                        <input type="tel" name="phone" class="form-control my-2" placeholder="Phone" id="phone"
                        value={{isset($client) ? $client->phone:old('phone')}}>
                            <input type="tel" name="fb_url" class="form-control" placeholder="Facebook" id="fb_url"
                            value={{isset($client) ? $client->fb_url:old('fb_url')}}>
                </div> --}}

                </div>
        </div>
        <div class="row">
            <div class="col form-group">
                <label class="form-label">Status</label>
                <select name="status" class="custom-select">
                    <option value="0" {{isset($client) ? $client->status == 'none' ? 'selected' :"":""}} >Choose...</option>
                    <option value="1" {{isset($client) ? $client->status == '1' ? 'selected' :"":""}}>Verified</option>
                    <option value="2" {{isset($client) ? $client->status == '2' ? 'selected' :"":""}}>Not verified</option>
                    <option value="3" {{isset($client) ? $client->status == '3' ? 'selected' :"":""}}>Suspended</option>
                </select>
            </div>
            <div class="form-group col ">
                <label class="form-label">Age : </label>
                <input type="number" name="age" class="form-control m-input" min="5" max="130"
                id="age" aria-describedby="nameHelp" placeholder="Enter user age"  value="{{isset($client) ? $client->age:old('age')}}">
            </div>

        </div>
    <button type="submit" class="btn1 btn float-right bg-blue mb-5 ">Submit</button>
    </div>

</form>

<script type="text/javascript">
    $(document).ready(function(){
      $(".form_datetime").datetimepicker({
        pickDate: false,
        minuteStep: 15,
        pickerPosition: 'bottom-right',
        format: 'HH:ii:00',
        autoclose: true,
        showMeridian: true,
        startView: 1,
        maxView: 1,
      });
      });
    </script>
   <script>
    $('.custom-file-input').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
</script>

<script>
    $('.sub').prop('disabled', true);
    $('.cat').change(function () {
        $('.sub').prop('disabled', false);
        if ($(this).val() != '') {
            var select = $(this).attr("id");
            var value = $(this).val();
            var dependent = $(this).data('dependent');
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('category.fetch') }}",
                method: "POST",
                data: {
                    select: select,
                    value: value,
                    dependent: dependent,
                    _token: _token
                },
                success: function (result) {
                    $('#' + dependent).html(result);
                }
            })
        }
    });

    $('#category').change(function () {
        $('#sub_category').val('');
    });
</script>

@endsection
