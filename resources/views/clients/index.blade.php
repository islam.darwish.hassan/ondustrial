@extends('layouts.resource.index')
@section('title', 'Clients')
@section('create-btn')
<a href="{{ route('users' . '.create',["type"=>2]) }}" class=" clr-blue p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <select class="form-control form-inline" name="gender">
                    <option value="none" @if(request()->query('gender') == 'none') selected @endif>Genders</option>
                    <option value="m"  @if(request()->query('gender') == 'males') selected @endif>Males</option>
                    <option value="f"  @if(request()->query('gender') == 'females') selected @endif>females</option>

                </select>
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block bg-blue mb-2">Filter</button>
            </div>
            <a href="{{ route($name. '.index') }}" class="small-header-bold"> X</a>

        </div>

    </form>
    <!-- Search and Filters END-->
@endsection




@section('table-header')

    <!------------------------------------------------------------- Columns START---------------------------------------------------------------------------->


    <th scope="col" scope="col" style="width: 10%" >@sortablelink('id', '#')</th>
    <th scope="col">@sortablelink('first_name', 'Name')</th>
    <th scope="col">Gender</th>
    <th scope="col">@sortablelink('nation_id', 'Nation')</th>
    <th scope="col" scope="col" style="width: 10%">Operations</th>


    <!------------------------------------------------------------- Columns END---------------------------------------------------------------------------->


@endsection



@section('table-body')
    @foreach ($clients as $client)
        <tr>

            <!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->


            <td>{{ $client->id }}</td>
            <td>{{ $client->name }} </td>
            <td>{{ isset($client->gender)?$client->gender:"Not Available"}}</td>
            <td>{{ $client->nation->en_name }}</td>


            <!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->

            <td class="d-flex justify-content-center"><a href="{{ route($name.'.show', $client->id) }}"
                class="btn clr-black  "><i class="fas fa-eye"></i></a>
               <a href="{{ route($name.'.edit', $client->id) }}"
                 class="btn clr-black "><i class="fas fa-edit"></i></a>
                   <form method="POST" action="{{ route('users.'.'destroy', $client->user_id) }}">
                      @csrf
                       {{ method_field('DELETE') }}
                       <button type="submit"
                        onclick="return confirm('Are you sure you want to delete this user?')"
                        data-toggle="modal" data-target="#exampleModal"
                   class="btn  clr-blue  bg-transparent"><i class="fas fa-trash-alt "></i></button>
                   </form>
               </td>
           </tr>
    @endforeach
@endsection
@section('table-footer')
<p class="table-footer">Results :  {{$data->total()}}</p>
@endsection
@section('extra_scripts')
@endsection
