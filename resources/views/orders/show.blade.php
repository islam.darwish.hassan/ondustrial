@extends('layouts.app')
@section('title', 'Order Managment')

@section('content')
<!--Info-->
<script src="{{asset('js/print.js')}}"></script>

@can('view-super',Auth::user())
  <div class="card mb-3  shadow-sm "  >
    <div class="card-header">
        Order Details : #{{$order->hash_code}}
    </div>
        <a href="{{ route('orders.print',$order) }}" class="btnprn btn" target="_blank">Print Preview</a>
        <script type="text/javascript">
        $(document).ready(function(){
        $('.btnprn').printPage();
        });
        </script>
    <div class="card-body text-muted">
        <div>
        <b>Code :</b> #{{$order->hash_code}}
        </div>
        <div>

        <b>Client Name :</b>{{$order->client->name}} |
        <b>Shipment Address: </b> {{$order->address_log}}
        </div>
        <div>
            <b>Shipment Address Note: </b> {{null !==($order->address->shipping_note)?$order->address->shipping_note:'empty'}}
         </div>
        <div>
        <b>Delivery Option: </b> {{null !==($order->delivery_option==1)?'(COD) Cash On Delivery ':'empty'}}
        </div>
        <div>
            <b>Delivery Fees: </b> {{$order->delivery_fees}}
        </div>

        <div>
        <b>Order Note: </b> {{null !==($order->note)?$order->note:'empty'}}
        </div>
        <div>
         <b>Shipments Number: </b> {{$order->shipments->count()}}
        </div>
        <div>
            <b>Shipments Codes: </b>
                @foreach($order->shipments as $shipment)
                @if($loop->last)
                {{$shipment->hash_code}}.
                @else
                {{$shipment->hash_code}},
                @endif
                @endforeach
         </div>
         <div>
            <b>Total Price: </b> {{$order->total_price}} EGP
        </div>
        <div>
            <b>Total & Delivery Fees: </b> {{$order->delivery_fees+$order->total_price}} EGP
        </div>

           <b>Status :</b>
           @if ($order->status == 1)
           <span class="badge bg-purple text-white">Request</span>
           @elseif ($order->status == 2)
           <span class="badge badge-primary text-black">Processing</span>
           @elseif ($order->status == 3)
           <span class="badge badge-warning text-black">In Shipping</span>
           @elseif ($order->status == 4)
           <span class="badge badge-success text-white">Shipped</span>
           @elseif ($order->status == 5)
           <span class="badge bg-grey text-black">Draft</span>
           @else
           <span class="badge bg-grey text-black">Not determined</span>
           @endif
    </div>
    @if($order->status==1)
    <form method="POST" action="{{ route('orders.'.'confirm',[$order->id]) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <button type="submit"
        onclick="return confirm('Are you sure you want to confirm this order there is no back for this process ,please be sure to confirm all shipment first with stores?')"
        data-toggle="modal" data-target="#Modal"
    class="btn bg-purple text-white  float-right mb-2 mr-2 text-uppercase">Confirm To Process</button>
    </form>
@elseif($order->status==2)
<form method="POST" action="{{ route('orders.'.'in_shipping',[$order->id]) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <button type="submit"
    onclick="return confirm('Are you sure you want to change status to -in shipping- this order there is no back for this process ?')"
    data-toggle="modal" data-target="#Modal"
class="btn btn-warning   float-right mb-2 mr-2 text-uppercase text-bold">From Proccessing To Out For Shipping</button>
</form>
@elseif($order->status==3)
<form method="POST" action="{{ route('orders.'.'shipped',[$order->id]) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <button type="submit"
    onclick="return confirm('Are you sure you want to change status to -shipped- this order there is no back for this process ?')"
    data-toggle="modal" data-target="#Modal"
class="btn btn-success   float-right mb-2 mr-2 text-uppercase text-bold ">From In Shipping To Shipped</button>
</form>

@elseif($order->status==4)
<a  class="btn  disabled  btn-block text-uppercase">Shipped to client</a>

@endif

</div>
@endcan
@foreach($order->shipments as $shipment)
@can('view',$shipment->store)
<div class="card mb-3  shadow-sm "  >
    <div class="card-header">
        Shipment {{$loop->index +1}} of {{$order->shipments->count()}}  : {{isset($shipment->store)?$shipment->store->name:'(Deleted) '.$shipment->store_log}}
        @if(isset($shipment->store))
        <a class="float-right" href={{route("stores.show",$shipment->store->id)}}> Visit Store </a>
        @endif
    </div>
    <div class="card-body">
        <div class="text-muted">
        <b>Code :</b> #{{$shipment->hash_code}} |
        </div>
    <div class="text-muted">
        <b>Store Name :</b>{{isset($shipment->store)?$shipment->store->name:'(Deleted) '.$shipment->store_log}} |
        <b>Shipment Address: </b> {{$order->address_log}}
    </div>
    <div class="text-muted">
        <b>Items Number: </b> {{$shipment->ordered_products->count()}}
    </div>
    <div class="text-muted">
        <b>Total Price: </b> {{$shipment->total_price}} EGP
    </div>
    <div class="text-muted">
        <b>Status :</b>
        @if ($shipment->status == 1)
        <span class="badge bg-purple text-white">Request</span>
        @elseif ($shipment->status == 2)
        <span class="badge badge-primary text-black">Confirmed</span>
        @elseif ($shipment->status == 3)
        <span class="badge badge-warning text-black">In Shipping</span>
        @elseif ($shipment->status == 4)
        <span class="badge badge-success text-white">Shipped</span>
        @elseif ($shipment->status == 5)
        <span class="badge bg-grey text-black">Draft</span>
        @else
            <span class="badge bg-grey text-black">Not determined</span>
        @endif
    </div>

    @if($shipment->ordered_products->count()>0)
    <div class="text-muted pb-2">
    <b>Products :</b>
    </div>
        <table class="table border-bottom ">
            <thead class="">
              <tr class="text-center">
                <th>&nbsp;</th>
                <th>Product</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total</th>

              </tr>
            </thead>
            <tbody >
                @foreach ($shipment->ordered_products as $item)

              <tr class="text-center">

                <td class="image-prod">  <a href="{{route('products.web.show',$item->original_product->id)}}" class="img-prod "><img style="width:100px" src="{{$item->original_product->image}}" alt="{{$item->product_name}}"></a></td>

                <td class="product-name">
                    <h3>{{$item->product_name}}</h3>
                    <p>{{$item->original_product->description}}</p>
                </td>

                <td class="price">{{$item->product_price/$item->qty}} EGP </td>

                <td class="quantity">
                    {{$item->qty}}
              </td>

                <td class="total">{{$item->product_price}} EGP</td>
              </tr><!-- END TR-->
              @endforeach
            </tbody>
          </table>
</div>
@if($shipment->status==1)
<form method="POST" action="{{ route('shipments.'.'confirm',[$shipment->id]) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <button type="submit"
     onclick="return confirm('Are you sure you want to confirm this shipment there is no back for this process?')"
     data-toggle="modal" data-target="#Modal"
class="btn bg-purple text-white btn-block text-uppercase">Confirm this Shipment</button>
</form>
@elseif($shipment->status==2)
<a  class="btn  disabled  btn-block text-uppercase">Confirmed</a>
@endif
@else
<p> No Products Available </p>
@endif
</div>
@endcan
@endforeach
<!--end of info -->
</div>
<!-- end of extra Info-->
@endsection
