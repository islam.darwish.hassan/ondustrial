
@extends('layouts.website')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('images/bg_6.jpg');">
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9 ftco-animate text-center">
        <h1 class="mb-0 bread">Cart [{{Cart::getTotalQuantity()}}]</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="{{route('index')}}">Home</a></span> </p>
        </div>
      </div>
      <div class="col-md-12">
        @if (session('message'))
            <div class="alert alert-success mt-2 " role="alert">
                <b>{{ session('message') }}</b>
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger mt-2" role="alert" >
                <b>{{ session('error') }}</b>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert  mt-2 px-2">
                {{-- <p><b>Please fix these errors.</b></p> --}}
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    </div>
  </div>

  <section class="ftco-section ftco-cart">
        <div class="row">
        <div class="col-md-12 ">
            <div class="">
                @if(Cart::getTotalQuantity()>0)
                <table class="table">
                    <thead class="thead-primary">
                      <tr class="text-center">
                        <th>&nbsp;</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th style="width:5%">Quantity</th>
                        <th>Total</th>
                        <th>&nbsp;</th>

                      </tr>
                    </thead>
                    <tbody>
                        @foreach (Cart::getContent() as $item)


                      <tr class="text-center">

                        <td class="image-prod">  <a href="{{route('products.web.show',$item->associatedModel->id)}}" class="img-prod "><img style="width:100px" src="{{$item->associatedModel->image}}" alt="{{$item->associatedModel->name}}"></a></td>

                        <td class="product-name">
                            <h4 class="text-center">{{$item->associatedModel->name}}</h4>
                            <p class="text-center">{{$item->associatedModel->description}}</p>
                        </td>

                        <td class="price">
                            @if(isset($item->associatedModel->offer))
                            {{$item->associatedModel->price -(($item->associatedModel->price*$item->associatedModel->offer->offer)/100)}} EGP
                            @else
                            {{$item->associatedModel->price}} EGP </td>
                            @endif
                        <td class="quantity">
                             <p class="text-center">{{$item->quantity}}</p>
                      </td>

                        <td class="total">{{$item->getPriceSumWithConditions()}} EGP</td>
                        <td class="product-remove">
                            <form method="POST" action="{{ route('cart.remove', $item->id) }}">
                               @csrf
                                {{ method_field('DELETE') }}
                                <button type="submit"
                                 onclick="return confirm('Are you sure you want to remove this item?')"
                                 data-toggle="modal" data-target="#exampleModal"
                            class="btn  px-2  "><i class="ion-ios-close"></i></button>
                            </form>

                        </td>

                      </tr><!-- END TR-->
                      @endforeach
                    </tbody>
                  </table>
                  @else
                  <div class=" d-flex flex-fill justify-content-center">
                  <a href={{route('index')}} class="btn bg-transparent " >
                    <span class="add-to-cart-btn">Continue Shopping <span class="icon-shopping_cart"></span></span>
                </a>
                  </div>
                  @endif
              </div>
        </div>
    </div>
    <form method="POST" action="{{route('web.checkout')}}">
                   @csrf

        @if(Cart::getTotalQuantity()>0)
    <div class="container-fluid">
        <div class="form-group ">
            <label>Choose Delivery Address <span style="color:red" >*</span></label>
            <select class="form-control m-input  " name="address">
                <option selected value="">Select Destination Address</option>
                @foreach($addresses as $address)
                <option value="{{ $address->id }}" {{ (old('address') == $address->id) ? 'selected' : '' }}>
                    {{ $address->address }}</option>
                @endforeach
            </select>
        </div>
        @endif
    @if(Cart::getTotalQuantity()>0)
    <div class="row justify-content-end">
        <div class="col col-lg-4 col-md-6 mt-5 cart-wrap ">
            <div class="cart-total mb-3  text-right">
                <h3 class="border-bottom pb-2 ">Cart Totals</h3>
                <p class="text-right ">
                    <b>Subtotal: </b>
                    <span>{{Cart::getSubTotal()}} EGP</span>
                </p>
                {{-- <p class="text-right">
                    <b>Delivery: </b>
                    <span> 0.00 EGP</span>
                </p> --}}
                <hr>
                <p class="text-right">
                    <b>Total: </b>
                    <span>{{Cart::getTotal()
                    }} EGP</span>
                </p>
            </div>

            <div class="text-right">
                <h3 class=""><button type="submit" class=" btn btn-primary py-3 px-5 mt-2 " >
                  <b>Processed to Checkout</b>
                </button></h3>
        </div>
    </div>
    </div>
    @endif
    </div>
    </form>
</section>
@endsection
