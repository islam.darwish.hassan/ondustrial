
@extends('layouts.website')

@section('content')
<div class="hero-wrap hero-bread" style="background-color: #ededed">
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-0 bread">My Profile</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="{{route('index')}}">Orders</a></span> </p>
        </div>
      </div>
      @if (session('message'))
      <div class="alert alert-success mt-2 " role="alert">
          <b>{{ session('message') }}</b>
      </div>
    @endif
    @if (session('error'))
      <div class="alert alert-danger mt-2" role="alert" >
          <b>{{ session('error') }}</b>
      </div>
    @endif

    </div>
  </div>
      <section class="ftco-section ">
      <div class="container">
          <div class="d-flex flex-row-reverse ">
                <a href={{route('index')}} class="btn bg-transparent " >
                    <span class="add-to-cart-btn">Continue Shopping <span class="icon-shopping_cart"></span></span>
                </a>
          </div>
            @foreach ($orders as $order)
                <div class="row my-2">
                <div class="card w-100">
                <div class="card-header d-flex justify-content-between">
                    <div class=" align-items-start ">
                        <div>
                        <a class=" mr-2"> Status:
                            @if ($order->status == 1)
                            <span class=" ">Request</span>
                            @elseif ($order->status == 2)
                            <span class=" ">Processing</span>
                            @elseif ($order->status == 3)
                            <span class=" ">In Shipping</span>
                            @elseif ($order->status == 4)
                            <span class=" ">Shipped</span>

                            @else
                            <span class="badge bg-grey text-black">Not determined</span>
                            @endif

                        </a>
                        </div>
                        <div>
                        <p>
                            Delivery Address : {{$order->address->address}}
                        </b>

                    </div>
                    </div>

                    <div class="row w-50">

                        <div class="col">
                        <p>
                            Order placed on : {{Carbon\Carbon::parse($order->created_at)->toDateString()}}
                        </b>
                        <p>
                            Order ID : #{{$order->hash_code}}
                        </b>
                        </div>
                        <div class="col">
                            <p>
                                Recipient : {{$order->client->name}}
                            </p>
                            <p>
                                Payment method: COD
                            </p>
                            <p>
                                Total: {{$order->total_price}} EGP
                            </p>
                        </div>
                    </div>
                </div>

                @foreach($order->shipments as $shipment)
                <div class="card-body">
                    <div class="cart-list">
                        <table class="table">
                            <thead class="thead-primary">
                              <tr class="text-left">
                                <th>                 Shipment {{$loop->index +1}} of {{$order->shipments->count()}}
                                </th>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>&nbsp;</th>

                              </tr>
                            </thead>
                            <tbody>
                                @foreach($shipment->ordered_products as $product)


                              <tr class="text-left">

                                <td class="image-prod">  <a href="{{route('products.web.show',$product->original_product->id)}}" class="img-prod "><img style="width:100px" src="{{$product->original_product->image}}" alt="{{$product->original_product->name}}"></a></td>

                                <td class="product-name">
                                    <h3>{{Str::limit($product->original_product->name,20)}}</h3>
                                    <p>{{Str::limit($product->original_product->description,70)}}</p>
                                </td>

                                <td class="price">{{$product->product_price/$product->qty}} EGP </td>

                                <td class="quantity">
                                    {{$product->qty}}
                                </td>

                                <td class="total"> {{$product->product_price}} EGP</td>
                                <td class="product-remove">
                                </td>

                              </tr><!-- END TR-->
                              @endforeach
                            </tbody>
                          </table>
                      </div>
                     </div>
                @endforeach
            </div>

          </div>
          @endforeach

      </div>
      <div class="container ">
        {{ $orders->appends(\Request::except('page'))->render() }}
            @if($orders->total()<=0)
            <div class="row justify-content-center">
                <h5 class="empty-table-text"> There are no orders  😴  </h5>

            </div>
            @endif
        </div>

  </section>
@endsection
