
@extends('layouts.website')

@section('content')
<div class="hero-wrap hero-bread" style="background-color: #ededed">
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-0 bread">My Profile</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="{{route('index')}}">Wishlists</a></span> </p>
        </div>
      </div>
      @if (session('message'))
      <div class="alert alert-success mt-2 " role="alert">
          <b>{{ session('message') }}</b>
      </div>
    @endif
    @if (session('error'))
      <div class="alert alert-danger mt-2" role="alert" >
          <b>{{ session('error') }}</b>
      </div>
    @endif

    </div>
  </div>
      <section class="ftco-section ">
      <div class="container">
          <div class="d-flex flex-row-reverse ">
                <a href={{route('web.mywishlists.create')}} class="btn bg-transparent " >
                    <span class="add-to-cart-btn">Create New Wishlist <i class="ion-ios-add ml-1"></i></span>
                </a>
          </div>
            @foreach ($wishlists as $wishlist)
                <div class="row my-2">
                <div class="card w-100">
                <div class="card-header d-flex justify-content-between">
                    <b>
                    {{$wishlist->name}}
                    </b>
                    <div class="d-flex align-items-center">
                    <form method="POST" action="{{route('web.mywishlists.destroy',$wishlist->id)}}">
                       @csrf
                        {{ method_field('DELETE') }}
                        <button type="submit"
                         onclick="return confirm('Are you sure you want to remove this item?')"
                         data-toggle="modal" data-target="#exampleModal"
                    class="btn"><i class="ion-ios-close "></i></button>
                    </form>
                    </div>

                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="">
                                @if($wishlist->products->count()>0)
                                <table class="table">
                                    <thead class="thead-primary">
                                      <tr class="text-center">
                                        <th>&nbsp;</th>
                                        <th>Product</th>
                                        <th>
                                        </th>

                                      </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($wishlist->products as $item)

                                      <tr class="text-center">

                                        <td class="image-prod">  <a href="{{route('products.web.show',$item->original_product->id)}}" class="img-prod "><img style="width:100px"
                                            src="{{$item->original_product->image}}" alt="{{$item->original_product->name}}"></a></td>

                                        <td class="product-name">
                                            <h4 class="text-left">{{$item->original_product->name}}</h4>
                                            <p class="text-left">{{$item->original_product->description}}</p>
                                        </td>



                                        <td class="product-remove">
                                            <form method="POST" action="{{ route('web.mywishlists.remove', $item->id) }}">
                                               @csrf
                                                {{ method_field('DELETE') }}
                                                <button type="submit"
                                                 onclick="return confirm('Are you sure you want to remove this item?')"
                                                 data-toggle="modal" data-target="#exampleModal"
                                            class="btn  px-2  "><i class="ion-ios-close"></i></button>
                                            </form>

                                        </td>

                                      </tr><!-- END TR-->
                                      @endforeach
                                    </tbody>
                                    @else
                                    There is no products in this wishlist
                                    @endif

                                  </table>
                              </div>
                        </div>
                    </div>
                </div>
                </div>
          </div>
          @endforeach

      </div>
        <div class="container"> {{ $wishlists->appends(request()->query())->links() }}
        </div>
  </section>
@endsection
