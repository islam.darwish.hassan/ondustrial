<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>

    <!-- main css -->
    <link rel="stylesheet" href="{{asset('website/css/main.css')}}">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{asset('website/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- bootstrap js -->
    <script src="{{asset('website/js/bootstrap.bundle.min.js')}}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <!-- jQuery -->
    <script src="{{asset('website/js/bootstrap.bundle.js')}}"></script>
    <script src="{{asset('website/js/jquery-3.5.1.min.js')}}"></script>
    <script src="{{asset('websitenode_modules/jquery/dist/jquery.min.js')}}"></script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
    <!-- fontawesome -->
    <script src="{{asset('website/js/all.js')}}"></script>
    <!-- fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">

</head>

<body class="bg-light">
    <!--------------------------------------------------------------- navbar ---------------------------------------------------------------------->
    @yield('ads')

    <!-- dark small div/navbar start-->
    <div class="container-fluid secondary-navbar-hight bg-dark d-none d-xl-block">
        <div class="row">
            <!-- left side -->
            <div class="col-lg-5 col-2 d-flex justify-content-center">
                <ul class="secondary-navbar-ul">
                    <li class="secondary-navbar-li">
                        <a href=""><i class="fas fa-phone-alt mr-2"></i>(+002)01124851646</a>
                    </li>
                    <li class="secondary-navbar-li">
                        <a href=""><i class="far fa-envelope mr-2"></i>info@industorial.com</a>
                    </li>
                </ul>
            </div>
            <!-- right side -->
            <div class="col-lg-7 col-10 d-flex justify-content-center">
                <ul class="secondary-navbar-ul ">
                    <li class="secondary-navbar-li">
                        <a href=""><i class="fas fa-store-alt mr-2"></i>Become a seller</a>
                    </li>
                    <li class="secondary-navbar-li">
                        <a href=""><i class="far fa-thumbs-up mr-2"></i>Best sellers</a>
                    </li>
                    <li class="secondary-navbar-li">
                        <a href=""><i class="far fa-credit-card mr-2"></i>Payment</a>
                    </li>
                    <li class="secondary-navbar-li">
                        <a href=""><i class="fas fa-truck mr-2"></i>Delivery</a>
                    </li>
                    <li class="secondary-navbar-li">
                        <a href=""><i class="far fa-user mr-2"></i>Lorem ipsum</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- dark small div/navbar end -->

    <!-- main navbar start-->
    <div class="container-fluid shadow-sm border-0">
        <!-- first navbar -->
        <div class="row align-items-center pt-3">
            <!-- logo section -->
            <div class="col-lg-4 d-flex justify-content-center pb-1">
                <button type="button" class="btn btn-light btn-lg button-logo"></button>
            </div>
            <!-- search bar -->
            <div class="col-lg-4 d-flex justify-content-center pb-1">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <div class="input-group-append">
                        <button class="btn btn-success" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
            <!-- info section -->
            <div class="col-lg-1 d-flex justify-content-center info-style pb-1">
                7 days a week from 8:00 am to 5:00 pm
            </div>
            <!-- account button/cart button section -->
            <div class="col-lg-3 d-flex justify-content-center pb-1">
                <!-- group for both buttons -->
                <div class="btn-group" role="group" aria-label="Basic example">
                    <!-- account button -->
                    <button class="btn btn-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Ahmed
                    </button>
                    <!-- account options list -->
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#">My orders</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">My addresses</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Wishlist</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Account settings</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Logout</a>
                    </div>
                    <!-- cart button -->
                    <button type="button" class="btn btn-light"><i class="fas fa-shopping-cart mr-1"></i>Cart</button>
                </div>
            </div>
        </div>
        <!-- navbar divider -->
        <div class="dropdown-divider"></div>

        <!-- second navbar -->
        <div class="row align-items-center">
            <div class="col d-flex justify-content-center">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <div class="dropdown">
                            <button class="dropbtn">All Categories</button>
                            <div class="dropdown-content">
                                <a href="#">Electrical Parts</a>
                                <a href="#">Mechanical Parts</a>
                                <a href="#">Electro-mechnical Parts</a>
                                <a href="#">Industrial Conrtol</a>
                                <a href="#">Accessories</a>
                            </div>
                        </div>
                        <div class="dropdown">
                            <button class="dropbtn">Electrical Parts</button>
                            <div class="dropdown-content">
                                <a href="#">Link 1</a>
                                <a href="#">Link 2</a>
                                <a href="#">Link 3</a>
                            </div>
                        </div>
                        <div class="dropdown">
                            <button class="dropbtn">Mechanical Parts</button>
                            <div class="dropdown-content">
                                <a href="#">Link 1</a>
                                <a href="#">Link 2</a>
                                <a href="#">Link 3</a>
                            </div>
                        </div>
                        <div class="dropdown">
                            <button class="dropbtn">Electro-mechnical Parts</button>
                            <div class="dropdown-content">
                                <a href="#">Link 1</a>
                                <a href="#">Link 2</a>
                                <a href="#">Link 3</a>
                            </div>
                        </div>
                        <div class="dropdown">
                            <button class="dropbtn">Industrial Conrtol</button>
                            <div class="dropdown-content">
                                <a href="#">Link 1</a>
                                <a href="#">Link 2</a>
                                <a href="#">Link 3</a>
                            </div>
                        </div>
                        <div class="dropdown">
                            <button class="dropbtn">Accessories</button>
                            <div class="dropdown-content">
                                <a href="#">Link 1</a>
                                <a href="#">Link 2</a>
                                <a href="#">Link 3</a>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!-- main navbar end-->

    @yield('content')

    <!------------------------------------------------------------ footer --------------------------------------------------------------------->
    <!-- footer start -->
    <div class="container-fluid bg-dark text-light footer pt-3">
        <!-- 1st footer -->
        <div class="row">
            <!-- 1st col -->
            <div class="col-sm-6 col-md-2">
                <ul class="footer-ul">
                    <li class="footer-li">
                        <h5>Contact us</h5>
                    </li>
                    <li class="footer-li">
                        <a href=""><i class="fas fa-phone-alt mr-2"></i>(+002)01124851646</a>
                    </li>
                    <li class="footer-li">
                        <a href=""><i class="far fa-envelope mr-2"></i>info@industorial.com</a>
                    </li>
                    <li class="footer-li">
                        <a href=""><i class="fas fa-map-marker-alt mr-2"></i>The Greek Campus,11 Usuf El Guindy street,
                            Bab El Louk, Tahrir, Cairo, Egypt</a>
                    </li>
                </ul>
            </div>
            <!-- 2nd col -->
            <div class="col-sm-6 col-md-2">
                <ul class="footer-ul ">
                    <li class="footer-li">
                        <h5>Policy & info</h5>
                    </li>
                    <li class="footer-li">
                        <a href="">Terms & conditions</a>
                    </li>
                    <li class="footer-li">
                        <a href=""></i>Policy for sellers</a>
                    </li>
                    <li class="footer-li">
                        <a href=""></i>Policy for buyers</a>
                    </li>
                    <li class="footer-li">
                        <a href="">Shipping & refund</a>
                    </li>
                    <li class="footer-li">
                        <a href="">Wholesale Policy</a>
                    </li>
                </ul>
            </div>
            <!-- 3rd col -->
            <div class="col-sm-6 col-md-2">
                <ul class="footer-ul ">
                    <li class="footer-li">
                        <h5>Quick links</h5>
                    </li>
                    <li class="footer-li">
                        <a href="">Seller login</a>
                    </li>
                    <li class="footer-li">
                        <a href="">Seller sign up</a>
                    </li>
                    <li class="footer-li">
                        <a href=""></i>Seller handbook</a>
                    </li>
                    <li class="footer-li">
                        <a href="">Seller control</a>
                    </li>
                    <li class="footer-li">
                        <a href="">Panel seller FAQs</a>
                    </li>
                </ul>
            </div>
            <!-- 4th col -->
            <div class="col-sm-6 col-md-2">

                <ul class="footer-ul ">
                    <li class="footer-li">
                        <h5>My account</h5>
                    </li>
                    <li class="footer-li">
                        <a href="">Sign in</a>
                    </li>
                    <li class="footer-li">
                        <a href="">Creat account</a>
                    </li>
                    <li class="footer-li">
                        <a href="">Orders</a>
                    </li>
                    <li class="footer-li">
                        <a href="">Wishlist</a>
                    </li>
                </ul>
            </div>
            <!-- 5th col -->
            <div class="col-sm-6 col-md-4">
                <ul class="footer-ul ">
                    <li class="footer-li">
                        <h5>Subscribe now</h5>
                    </li>
                    <li class="footer-li">
                        <p class="small">Subscribe to receive recent promotions & useful essays!</p>
                    </li>
                    <li class="footer-li">
                        <div class="input-group py-3 w-75">
                            <input type="text" class="form-control" placeholder="Your E-mail Address">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="button">
                                    <i class="fas fa-paper-plane"></i>
                                </button>
                            </div>
                        </div>
                    </li>
                    <li class="footer-li">
                        <div>
                            <a href="#"><i class="fab fa-facebook fa-2x mx-1"></i></a>
                            <a href="#"><i class="fab fa-twitter-square fa-2x mx-1"></i></a>
                            <a href="#"><i class="fab fa-linkedin fa-2x mx-1"></i></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <hr>
        <!-- 2nd footer -->
        <div class="row secondary-footer">
            <div class="col d-flex justify-content-center">
                <p>Copyright © 2020 Silicon-Arena.com</p>
            </div>
            <div class="col d-flex justify-content-center">
                <i class="fab fa-cc-visa fa-2x mx-2"></i>
                <i class="fab fa-cc-paypal fa-2x mx-2"></i>
                <i class="fab fa-cc-mastercard fa-2x mx-2"></i>
            </div>
        </div>
    </div>
    <!-- footer end -->

</body>

</html>
