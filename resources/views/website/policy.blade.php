<!DOCTYPE html>
<html lang="en">

<head>
    <title>Privacy Policy</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- MainStyle -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- CustomStyle -->
    <link rel="stylesheet" href="{{ asset('css/main3.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free-5.11.2-web/css/all.min.css') }}">
    <!-- BootstrapScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</head>
<body data-spy="scroll">
    <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-blue ">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
            aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand  " href="{{ route('index') }}"><img src="{{ asset('assets/imgs/logo.png') }}" width="auto" height="30px"></a>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0 ">
                <li class="nav-item ">
                    <a class="nav-link small text-white" href="{{ route('terms') }}"><b>Terms & Conditions</b>  <span
                            class="sr-only"></span></a>
                </li>

                <li class="nav-item ">
                    <a class="nav-link small active text-white" href="{{ route('policy') }}"><b>Privacy Policy</b> <span class="sr-only">(current)</span></a>
                </li>


            </ul>
        </div>

    </nav>
    <div class="policy bg-grad-light">
        <h1 class="text-center" style=" font-family:'brandfont';text-decoration: underline; color:#3A5DF7; ">Privacy Policy</h1>
        <div class=WordSection1>


            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Welcome to Ondustrial's Privacy Policy.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Ondustrial respects your privacy and is committed to protecting
            your personal data. This Privacy Policy will inform you as</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>to how we look after your personal data when you visit our
            application/website (regardless of where you visit it from)</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>and tell you about your privacy rights and how the law protects
            you.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>1. Important information and who we are</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>1.1 Purpose of this Privacy Policy</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>This Privacy Policy aims to give you information on how Ondustrial
            collects and processes your personal data through</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>your use of the website or the mobile application, including any
            data you may provide through this application/website</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>when you when you [sign up to our newsletter, register and/or
            purchase a product or service, as well as installing and</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>signing up to any Ondustrial's powered application or our mobile
            applications].</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>This application/website is not intended for children and we do
            not knowingly collect data relating to children.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>It is important that you read this Privacy Policy together along
            with our <a  href="{{ route('terms') }}"><b>Terms & Conditions</b>
                </a>
  when we are</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>collecting or processing personal data about you so that you are
            fully aware of how and why we are using your data.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>This Privacy Policy supplements other notices and privacy
            policies and is not intended to override them.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>1.2 Controller</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Ondustrial is the controller and responsible for your personal data
            (referred to as [&amp;Ondustrial&amp;], &amp;();, &amp;() or &amp;() in this</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Privacy Policy). Ondustrial is the controller and responsible for
            this application and website.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We have appointed a Data Protection Officer (DPO) who is
            responsible for overseeing questions in relation to this</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Privacy Policy. If you have any questions about this Privacy
            Policy, including any requests to exercise, please contact</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>the DPO using the details set out below.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>1.3 Contact details</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>If you have any questions about this Privacy Policy or our
            privacy practices, please contact our DPO in the following</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>ways:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Email address: Ondustrial2020@gmail.com</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>1.4 Changes to the Privacy Policy and your duty to inform us of
            changes</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We keep our Privacy Policy under regular review.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>It is important that the personal data we hold about you is
            accurate and current. Please keep us informed if your</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>personal data changes during your relationship with us.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>1.5 Third-party links</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>2</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>This Platform (Website, Mobile Application, and Client
            Portal/Dashboard) may include links to third-party websites,</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>plug-ins and applications. Clicking on those links or enabling
            those connections may allow third parties to collect or</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>share data about you. We do not control these third-party
            applications/websites and are not responsible for them</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>privacy statements. When you leave our application/website, we
            encourage you to read the Privacy Policy of every</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>application/website you visit.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>2. Personal Data that we collect</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Your personal data, or personal information, means any
            information about an individual from which that person can</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>be identified. It does not include data where the identity has
            been removed (anonymous data).</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We may collect, use, store and transfer different kinds of
            personal data about you which is including but not limited to</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>the following:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>a. Identity data includes [first name, maiden name, last name,
            username or similar identifier, marital status, title,</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>date of birth and gender].</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>b. Contact data includes [billing address, delivery address,
            email address and telephone numbers].</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>c. Financial data includes [bank account and payment card
            details].</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>d. Transaction data includes [details about payments to and from
            you and other details of services you have</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>ordered from us].</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>e. Technical data includes [internet protocol (IP) address, your
            login data, browser type and version, time zone</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>setting and location, browser plug-in types and versions,
            operating system and platform, and other technology</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>on the devices you use to access this website].</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>f. Profile data includes [your username and password, purchases
            or orders made by you, your interests,</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>preferences, and feedback and survey responses].</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>g. Usage data includes [information about how you use our
            website, products and services].</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We also collect, use and share aggregated data such as
            statistical or demographic data for any purpose. Aggregated</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>data could be derived from your personal data but is not considered
            personal data in law as this data will not directly</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>or indirectly reveal your identity. For example, we may
            aggregate your usage data to calculate the percentage of users</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>accessing a specific website feature. However, if we combine or
            connect aggregated data with your personal data so</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>that it can directly or indirectly identify you, we treat the
            combined data as personal data which will be used in</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>accordance with this Privacy Policy.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We do not collect any Special Categories of Personal Data about
            you (this includes details about your race or</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>ethnicity, religious or philosophical beliefs, sex life, sexual
            orientation, political opinions, trade union membership,</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>information about your health, and genetic and biometric data).
            Nor do we collect any information about criminal</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>convictions and offences.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>3. If you fail to provide personal data</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Where we need to collect personal data by law, or under the
            terms of a contract we have with you, and you fail to</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>provide that data when requested, we may not be able to perform
            the contract we have or are trying to enter into with</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>you (for example, to provide you with services). In this case,
            we may have to cancel a service you have with us but we</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>will notify you if this is the case at the time.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>4. How is your personal data collected?</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We use different methods to collect data from and about you
            including through:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>4.1 Direct interactions. You may give us your Identity, contact
            and financial data by filling in forms or by</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>corresponding with us by post, pehone, and email or otherwise.
            This includes personal data you provide when you</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>undertake the following:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>3</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>a. purchases our products or services;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>b. creates an account on our application or website;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>c. subscribes to our service or publications;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>d. request marketing to be sent to you;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>e. enters a competition, promotion or survey; or</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>f. gives us feedback or contact us.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>4.2 Automated technologies or interactions. As you interact with
            our application/website, we will automatically collect</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Technical Data about your equipment, browsing actions and
            patterns. We collect this personal data by using cookies,</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>server logs and other similar technologies. We may also receive
            Technical Data about you if you visit other mobile</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>applications/websites employing our cookies. Please see our
            cookie policy for further details.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>5. How we use your personal data</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We will only use your personal data when the law allows us to.
            Most commonly, we will use your personal data in the</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>following circumstances:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>a. Where we need to perform the contract, we are about to enter
            into or have entered into with you.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>b. Where it is necessary for our legitimate interests (or those
            of a third party) and your interests and fundamental</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>rights do not override those interests.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>c. Where we need to comply with a legal obligation.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>d. Where we need to set up your account and administrate it.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>e. Where we need to deliver marketing and events communication.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>f. Where we need to carry out surveys.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>g. Where we need to personalize content, user experience or
            Partener information.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>h. Where we need to meet audit requirements internally.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>i. Where you have given consent.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Generally, we do not rely on consent as a legal basis for
            processing your personal data although we will get your</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>consent before sending third party direct marketing
            communications to you via email or text message. You have the</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>right to withdraw consent to marketing at any time by contacting
            us.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>6. Purposes for which we will use your personal data</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>A) Performance of a contract with you</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We process your personal data because it is necessary for the
            performance of a contract to which you are a party or in</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>order to take steps at your request prior to entering into a
            contract or agreement.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>In this respect, we use your personal data for the following:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>i. To prepare a proposal for you regarding the services we
            offer;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>ii. To provide you with the services as set as the scope of our
            services, or as otherwise agreed with you</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>from time to time;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>iii. To deal with any complaints or feedback you may have;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>iv. For any other purpose for which you provide us with your
            personal data.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>In this respect, we may share your personal data with or
            transfer it to the following:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>i. Subject to your consent, independent third parties whom we
            engage to assist in delivering the services</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>to you, including third parties;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>4</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>ii. Our professional advisers where it is necessary for us to
            obtain their advice or assistance, including</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>lawyers, accountants, IT or public relations advisers;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>iii. Debt collection agencies where it is necessary to recover
            money you owe us;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>iv. Other third parties such as intermediaries who we introduce
            to you. We will wherever possible tell</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>you who they are before we introduce you;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>v. Our data storage providers.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>The legal basis for the processing of the aforementioned data
            categories is Art. 6 (1) (a) of the European General Data</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Protection Regulation (GDPR). Due to the said purposes, in
            particular to guarantee security and a smooth connection</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>setup, we have a legitimate interest to process this data.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>B) Legitimate interests</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We also process your personal data because it is necessary for
            our legitimate interests, or sometimes where it is</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>necessary for the legitimate interests of another person.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>In this respect, we use your personal data for:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>i. For the administration and management of our Partener,
            including recovering money you owe to us,</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>and archiving or statistical analysis;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>ii. Seeking advice on our rights and obligations, such as where
            we require our own legal advice. In this</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>respect we will share your personal data with our advisers or
            agents where it is necessary for us to</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>obtain their advice or assistance and with third parties and
            their advisers where those third parties are</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>acquiring, or considering acquiring, all or part of our
            Partener.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>C) Legal obligations</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We also process your personal data for our compliance with a
            legal obligation which we are under. In this respect, we</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>will use your personal data for the following:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>i. To meet our compliance and regulatory obligations, such as
            compliance with anti-money laundering</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>laws;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>ii. As required by tax authorities or any competent court or
            legal authority. In this respect, we will share</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>your personal data with the following:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>iii. Our advisers where it is necessary for us to obtain their
            advice or assistance;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>iv. Our auditors where it is necessary as part of their auditing
            functions;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>v. With third parties who assist us in conducting background
            checks;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>vi. With relevant regulators or law enforcement agencies where
            we are required to do so.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>D) Marketing</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We will send you marketing about services we provide which may
            be of interest to you, as well as other information</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>in the form of alerts, newsletters, discounts or functions which
            we believe might be of interest to you or in order to</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>update you with information (such as legal or commercial news)
            which we believe may be relevant to you. We will</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>communicate this to you in a number of ways including by post,
            telephone, email or other digital channels.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>5</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>E) Promotional offers from us</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We may use your Identity, Contact, Technical, Usage and Profile
            Data to form a view on what we think you may want</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>or need, or what may be of interest to you. This is how we
            decide which products, services and offers may be relevant</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>for you (Marketing).</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>You will receive marketing communications from us if you have
            requested information from us or purchased services</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>from us [or if you provided us with your details when you
            entered a competition or registered for a promotion] and, in</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>each case, you have not opted out of receiving that marketing.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>F) Third-party marketing</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>i. We will get your express opt-in consent before we share your
            personal data with any company outside</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Ondustrial for marketing purposes.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>ii. You can ask us or third parties to stop sending you
            marketing messages at any time by logging into</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>the application/website and checking or unchecking relevant
            boxes to adjust your marketing</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>preferences or by following the opt-out links on any marketing
            message sent to you or by contacting</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>us at any time.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>G) Cookies</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>You can set your browser to refuse all or some browser cookies,
            or to alert you when application/websites set or</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>access cookies. If you disable or refuse cookies, please note
            that some parts of this website may become inaccessible</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>or not function properly.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>H) Change of purpose</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We will only use your personal data for the purposes for which
            we collected it, unless we reasonably consider that we</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>need to use it for another reason and that reason is compatible
            with the original purpose. If you wish to get an</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>explanation as to how the processing for the new purpose is
            compatible with the original purpose, please contact us. If</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>we need to use your personal data for an unrelated purpose, we
            will notify you and we will explain the legal basis</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>which allows us to do so. Please note that we may process your
            personal data without your knowledge or consent, in</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>compliance with the above rules, where this is required or
            permitted by law.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>I) Opting out</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Where you opt out of receiving these marketing messages, this
            will not apply to personal data provided to us as a</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>result of a product/service purchase, warranty registration, product/service
            experience or other transactions.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>7. Disclosures of your personal data</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We may share your personal data with the parties set out in
            article (6) purposes for which we will use your personal</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>data above.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Third parties to whom we may choose to sell, transfer or merge
            parts of our Partener or our assets. Alternatively, we</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>may seek to acquire other Parteneres or merge with them. If a
            change happens to our Partener, then the new owners</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>may use your personal data in the same way as set out in this Privacy
            Policy.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We require all third parties to respect the security of your
            personal data and to treat it in accordance with the law. We</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>do not allow our third-party service providers to use your
            personal data for their own purposes and only permit them</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>to process your personal data for specified purposes and in
            accordance with our instructions.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>8. Data security</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>6</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We have put in place appropriate security measures to prevent
            your personal data from being accidentally lost, used or</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>accessed in an unauthorized way, altered or disclosed. In
            addition, we limit access to your personal data to those</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>employees, agents, contractors, third party service providers
            and other parties who have a Partener need to know.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>They will only process your personal data on our instructions
            and they are subject to a duty of confidentiality.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We have put in place procedures to deal with any suspected
            personal data breach and will notify you and any</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>applicable regulator of a breach where we are legally required
            to do so.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>9. Data retention</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>How long will you use my personal data for?</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We will only retain your personal data for as long as reasonably
            necessary to fulfil the purposes we collected it for,</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>including for the purposes of satisfying any legal, regulatory,
            tax, accounting or reporting requirements. We may</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>retain your personal data for a longer period in the event of a
            complaint or if we reasonably believe there is a prospect</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>of litigation in respect to our relationship with you.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>To determine the appropriate retention period for personal data,
            we consider the amount, nature and sensitivity of the</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>personal data, the potential risk of harm from unauthorized use
            or disclosure of your personal data, the purposes for</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>which we process your personal data and whether we can achieve those
            purposes through other means, and the</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>applicable legal, regulatory, tax, accounting or other
            requirements.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>When it is no longer necessary to retain your personal data, we
            will delete it.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>[Details of retention periods for different aspects of your personal
            data are [available in our retention policy which you</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>can request from us by contacting us.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>10. What we may need from you</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We may need to request specific information from you to help us
            confirm your identity and ensure your right to access</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>your personal data (or to exercise any of your other rights).
            This is a security measure to ensure that personal data is</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>not disclosed to any person who has no right to receive it. We
            may also contact you to ask you for further information</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>in relation to your request to speed up our response.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>11. Time limit to respond</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We try to respond to all legitimate requests within one month.
            Occasionally it could take us longer than a month if</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>your request is particularly complex or you have made a number
            of requests. In this case, we will notify you and keep</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>you updated.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>12. What we do if the information is incorrect?</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>We do perform checks on the information that we receive to
            detect any defects or mistakes. However, we are reliant</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>upon suppliers, namely Companies House, providing accurate
            information to us. You have the right to request that we:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Rectify any personal data relating to you that is inaccurate;
            and</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Complete any incomplete data, including by way of a
            supplementing, corrective statement. This is known as the right</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>to rectification. If you do exercise your right to
            rectification, we will take steps to check the information and correct it</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>where necessary.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>13. How do we deal with the 'right to be forgotten'?</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>7</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>You have the right to request the erasure of personal data that
            we hold about you in certain circumstances, for example</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>if it were not acquired for, or has ceased to be necessary for,
            a lawful purpose. This is known as the right to be</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>forgotten. Where you request that we erase your data, we will
            usually only do so where the data has ceased to be</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>publicly available, whether at Companies House or otherwise, or
            where we no longer use it</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>14. Glossary</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>LAWFUL BASIS</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Legitimate Interest means the interest of our Partener in
            conducting and managing our Partener to enable us to give</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>you the best service and the best and most secure experience. We
            make sure we consider and balance any potential</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>impact on you (both positive and negative) and your rights
            before we process your personal data for our legitimate</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>interests. We do not use your personal data for activities where
            our interests are overridden by the impact on you</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>(unless we have your consent or are otherwise required or
            permitted to by law). You can obtain further information</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>about how we assess our legitimate interests against any
            potential impact on you in respect of specific activities by</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>contacting us.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Performance of Contract means processing your data where it is
            necessary for the performance of a contract to which</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>you are a party or to take steps at your request before entering
            into such a contract.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Comply with a legal obligation means processing your personal
            data where it is necessary for compliance with a legal</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>obligation that we are subject to.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>THIRD PARTIES</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Internal Third Parties</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Other companies in Ondustrial acting as joint controllers or
            processors who are based in Egypt and provide IT and system</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>administration services and undertake leadership reporting.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>External Third Parties</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Service providers acting as processors-based Egypt who provide
            IT and system administration services.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Professional advisers acting as processors or joint controllers
            including lawyers, bankers, auditors and insurers who</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>provide consultancy, banking, legal, insurance and accounting
            services.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Governmental bodies and other authorities acting as processors
            or joint controllers based in the Arab Republic of</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Egypt who require reporting of processing activities in certain
            circumstances.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>YOUR LEGAL RIGHTS</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>You have the right to:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Request access to your personal data (commonly known as a ()
            subject access()). This enables you to</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>receive a copy of the personal data we hold about you and to
            check that we are lawfully processing it.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Request correction of the personal data that we hold about you.
            This enables you to have any incomplete or inaccurate</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>data we hold about you corrected, though we may need to verify
            the accuracy of the new data you provide to us.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Request erasure of your personal data. This enables you to ask
            us to delete or remove personal data where there is no</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>good reason for us continuing to process it. You also have the
            right to ask us to delete or remove your personal data</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>where you have successfully exercised your right to object to
            processing (see below), where we may have processed</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>your information unlawfully or where we are required to erase
            your personal data to comply with local law. Note,</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>8</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>&nbsp;</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>however, that we may not always be able to comply with your
            request of erasure for specific legal reasons which will</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>be notified to you, if applicable, at the time of your request.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Object to processing of your personal data where we are relying
            on a legitimate interest (or those of a third party) and</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>there is something about your particular situation which makes
            you want to object to processing on this ground as you</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>feel it impacts on your fundamental rights and freedoms. You
            also have the right to object where we are processing</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>your personal data for direct marketing purposes. In some cases,
            we may demonstrate that we have compelling</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>legitimate grounds to process your information which override
            your rights and freedoms.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Request restriction of processing of your personal data. This
            enables you to ask us to suspend the processing of your</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>personal data in the following scenarios:</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>' If you want us to establish the data&amp;()accuracy.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>' Where our use of the data is unlawful but you do not want us
            to erase it.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>' Where you need us to hold the data even if we no longer
            require it as you need it to establish, exercise or</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>defend legal claims.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>' You have objected to our use of your data but we need to
            verify whether we have overriding legitimate</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>grounds to use it.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Request the transfer of your personal data to you or to a third
            party. We will provide to you, or a third party you have</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>chosen, your personal data in a structured, commonly used,
            machine-readable format. Note that this right only applies</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>to automated information which you initially provided consent
            for us to use or where we used the information to</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>perform a contract with you.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>Withdraw consent at any time where we are relying on consent to
            process your personal data. However, this will not</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>affect the lawfulness of any processing carried out before you
            withdraw your consent. If you withdraw your consent,</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>we may not be able to provide certain products or services to
            you. We will advise you if this is the case at the time you</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>withdraw your consent.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>You will not have to pay a fee to access your personal data (or
            to exercise any of the other rights). However, we may</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>charge a reasonable fee if your request is clearly unfounded,
            repetitive or excessive. Alternatively, we could refuse to</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>comply with your request in these circumstances.</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>How to contact us? If you have any questions about how we use
            your personal data, or you wish to exercise any of the</span></p>

            <p class=MsoNormal align=center style='margin-right:9.9pt;text-align:center'><span
            style='font-size:13.5pt;line-height:107%;font-family:"brandfont",sans-serif;
            color:#3A5DF7'>rights set out above, please contact our controller listed under
            1.3 of this Privacy Policy.</span></p>

            </div>
                            <!-- footer -->
    <section class="footer">
        <div class="container">
            <footer class="">
                <div class="d-flex justify-content-center">
                    <ul class="row">
                        <a href="#" style="padding-right: 10px;"><i class="fab fa-facebook fa-2x"></i></a>
                        <a href="#" style="padding-right: 10px;"><i class="fab fa-twitter fa-2x"></i></a>
                        <a href="#" style="padding-right: 45px;"><i class="fab fa-instagram fa-2x"></i></a>
                    </ul>
                </div><!-- Ende Sozial media -->
                <div class="d-flex justify-content-center">
                    <p> All rights reseved </p>
                </div><!-- Ende Copyright -->
            </footer>
        </div>
    </section>
</body>

<script>
    //ScrollReveal().reveal('.headline');
    //for nav bar
    $('.navbar-nav .nav-link').click(function () {
        $('.navbar-nav .nav-link').removeClass('active');
        $(this).addClass('active');
    });
</script>

</html>
