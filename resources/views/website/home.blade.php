@extends('website.layouts.main')

@section('ads')
     <!-- top div image/home page start-->
     <div class="container-fluid top-div-bg-image d-none d-lg-block"></div>
     <!-- top div image/home page end-->
@endsection

@section('content')
<!------------------------------------------------------------ carousel  -------------------------------------------------------------------->
    <!-- caroussel on xlarge screen start-->
    <div class="container-fluid d-none d-xl-block">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <!-- display on large screen -->
            <div class="carousel-inner carousel-inner-height-lg">
                <div class="carousel-item active">
                    <img src="{{asset('website/img/person-holding-tablet-computer-3740378.jpg')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('website/img/person-holding-white-printer-paper-3740390.jpg')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('website/img/photo-of-person-pointing-on-laptop-3183171.jpg')}}" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- caroussel on xlarge screen end-->

    <!-- caroussel on large screen start-->
    <div class="container-fluid d-none d-lg-block d-xl-none">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <!-- display on large screen -->
            <div class="carousel-inner carousel-inner-height-lg">
                <div class="carousel-item active">
                    <img src="{{asset('./img/person-holding-tablet-computer-3740378.jpg')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('./img/person-holding-white-printer-paper-3740390.jpg')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('./img/photo-of-person-pointing-on-laptop-3183171.jpg')}}" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- caroussel on large screen end-->

    <!-- caroussel on meduim screen start-->
    <div class="container-fluid d-none d-md-block d-lg-none">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <!-- display on meduim screen -->
            <div class="carousel-inner carousel-inner-height-md">
                <div class="carousel-item active">
                    <img src="{{asset('./img/person-holding-tablet-computer-3740378.jpg')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('./img/person-holding-white-printer-paper-3740390.jpg')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('./img/photo-of-person-pointing-on-laptop-3183171.jpg')}}" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- caroussel on meduim screen end-->

    <!-- caroussel on small screen start-->
    <div class="container-fluid d-none d-sm-block d-md-none">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <!-- display on small screen -->
            <div class="carousel-inner carousel-inner-height-sm">
                <div class="carousel-item active">
                    <img src="{{asset('./img/person-holding-tablet-computer-3740378.jpg')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('./img/person-holding-white-printer-paper-3740390.jpg')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('./img/photo-of-person-pointing-on-laptop-3183171.jpg')}}" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- caroussel on small screen end-->

    <!-- caroussel on xsmall screen start-->
    <div class="container-fluid d-block d-sm-none">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <!-- display on xsmall screen -->
            <div class="carousel-inner carousel-inner-height-sm">
                <div class="carousel-item active">
                    <img src="{{asset('./img/person-holding-tablet-computer-3740378.jpg')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('./img/person-holding-white-printer-paper-3740390.jpg')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('./img/photo-of-person-pointing-on-laptop-3183171.jpg')}}" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- caroussel on xsmall screen end-->
    <!--------------------------------------------------------- features ----------------------------------------------------------------------->
    <!-- middle section start -->
    <div class="container-fluid pt-3">
        <!-- section box -->
        <div class="container pt-3 md-section-style">
            <div class="row">
                <div class="col-sm-6 col-md-3 text-center">
                    <i class="fas fa-rocket fa-4x text-success mb-3"></i>
                    <h5>Free Shipping</h5>
                    <p class="text-small">Free Shipping All Products</p>
                </div>
                <div class="col-sm-6 col-md-3 text-center">
                    <i class="fas fa-umbrella fa-4x text-success mb-3"></i>
                    <h5>100% Money Guarantee</h5>
                    <p class="text-small">30 Days Money Back</p>
                </div>
                <div class="col-sm-6 col-md-3 text-center">
                    <i class="fas fa-hands-helping fa-4x text-success mb-3"></i>
                    <h5>Help Center</h5>
                    <p class="text-small">24/7 Support System</p>
                </div>
                <div class="col-sm-6 col-md-3 text-center">
                    <i class="fas fa-money-check fa-4x text-success mb-3"></i>
                    <h5>Payment Method</h5>
                    <p class="text-small">Secure Payment</p>
                </div>
            </div>
        </div>
    </div>
    <!-- middle section end -->
    <!----------------------------------------------------------- product section ------------------------------------------------------------------>
    <!-- selected product cards start -->
    <div class="container-fluid">
        <div class="container pt-5">
            <!-- First Row [Prosucts]-->
            <h2 class="font-weight-bold pb-4">Selected Products</h2>
            <!-- button group -->
            <div class="d-none d-lg-block pb-3">
                <button type="button" class="btn btn-outline-secondary cards-menu-button-1">Trending Items</button>
                <button type="button" class="btn btn-outline-secondary cards-menu-button-1">New Arrival</button>
                <button type="button" class="btn btn-outline-secondary cards-menu-button-1">Best Sale</button>
            </div>
            <div class="row pb-5 mb-4">
                <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                    <!-- Card-->
                    <div class="card rounded shadow-sm border-0">
                        <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                                class="img-fluid d-block mx-auto mb-3">
                            <h5> <a href="{{route('product')}}" class="text-dark">CL 10000 delta UPS</a></h5>
                            <h5 class="text-muted">350.00 EGP</h5>
                            <ul class="list-inline small">
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                    <!-- Card-->
                    <div class="card rounded shadow-sm border-0">
                        <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                                class="img-fluid d-block mx-auto mb-3">
                            <h5> <a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                            <h5 class="text-muted">350.00 EGP</h5>
                            <ul class="list-inline small">
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                    <!-- Card-->
                    <div class="card rounded shadow-sm border-0">
                        <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                                class="img-fluid d-block mx-auto mb-3">
                            <h5> <a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                            <h5 class="text-muted">350.00 EGP</h5>
                            <ul class="list-inline small">
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                    <!-- Card-->
                    <div class="card rounded shadow-sm border-0">
                        <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                                class="img-fluid d-block mx-auto mb-3">
                            <h5> <a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                            <h5 class="text-muted">350.00 EGP</h5>
                            <ul class="list-inline small">
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-outline-secondary btn-md btn-block cards-menu-button-2">
                    <i class="fas fa-redo-alt mr-2"></i>Show more</button>
            </div>
        </div>
    </div>
    <!-- selected product cards end -->

    <!-- featured product cards start -->
    <div class="container-fluid">
        <div class="container">
            <!-- First Row [Prosucts]-->
            <h2 class="font-weight-bold pb-4">Featured Products</h2>
            <!-- button group -->
            <div class="d-none d-lg-block pb-3">
                <button type="button" class="btn btn-outline-secondary cards-menu-button-1 mb-1">Trending Items</button>
                <button type="button" class="btn btn-outline-secondary cards-menu-button-1 mb-1">New Arrival</button>
                <button type="button" class="btn btn-outline-secondary cards-menu-button-1 mb-1">Best Sale</button>
                <button type="button" class="btn btn-outline-secondary cards-menu-button-1 mb-1">Best Sale</button>
                <button type="button" class="btn btn-outline-secondary cards-menu-button-1 mb-1">Best Sale</button>
                <button type="button" class="btn btn-outline-secondary cards-menu-button-1 mb-1">Best Sale</button>
                <button type="button" class="btn btn-outline-secondary cards-menu-button-1 mb-1">Best Sale</button>
            </div>
            <div class="row pb-5 mb-4">
                <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                    <!-- Card-->
                    <div class="card rounded shadow-sm border-0">
                        <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                                class="img-fluid d-block mx-auto mb-3">
                            <h5><a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                            <h5 class="text-muted">350.00 EGP</h5>
                            <ul class="list-inline small">
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                    <!-- Card-->
                    <div class="card rounded shadow-sm border-0">
                        <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                                class="img-fluid d-block mx-auto mb-3">
                            <h5><a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                            <h5 class="text-muted">350.00 EGP</h5>
                            <ul class="list-inline small">
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                    <!-- Card-->
                    <div class="card rounded shadow-sm border-0">
                        <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                                class="img-fluid d-block mx-auto mb-3">
                            <h5> <a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                            <h5 class="text-muted">350.00 EGP</h5>
                            <ul class="list-inline small">
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                    <!-- Card-->
                    <div class="card rounded shadow-sm border-0">
                        <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                                class="img-fluid d-block mx-auto mb-3">
                            <h5> <a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                            <h5 class="text-muted">350.00 EGP</h5>
                            <ul class="list-inline small">
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-outline-secondary btn-md btn-block cards-menu-button-2">
                    <i class="fas fa-redo-alt mr-2"></i>Show more</button>
            </div>
        </div>
    </div>
    <!-- featured product cards end -->

@endsection
