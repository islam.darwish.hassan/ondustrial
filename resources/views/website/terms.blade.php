<!DOCTYPE html>
<html lang="en">

<head>
    <title>Terms & Conditions</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- MainStyle -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- CustomStyle -->
    <link rel="stylesheet" href="{{ asset('css/main3.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free-5.11.2-web/css/all.min.css') }}">
    <!-- BootstrapScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</head>


<body>
    <nav class="navbar navbar-expand-lg fixed-top navbar-light bg-blue">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
            aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand  " href="{{ route('index') }}"><img src="{{ asset('assets/imgs/logo.png') }}" width="auto" height="30px"></a>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0 ">
                <li class="nav-item ">
                    <a class="nav-link small active text-white " href="{{ route('terms') }}"><b>Terms & Conditions</b> <span
                            class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link small text-white" href="{{ route('policy') }}"><b>Privacy Policy </b><span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="policy bg-grad-light">
        <h1 class="text-center" style=" font-family:'brandfont';text-decoration: underline; color:#3A5DF7; ">Terms & conditions</h1>
        <div class=WordSection1>

          <p class=MsoNormal dir=RTL style='text-align:justify;direction:rtl;unicode-bidi:
          embed'><b><span dir=LTR style='font-size:26.0pt;line-height:107%'>&nbsp;</span></b></p>

          <p class=text-right dir=RTL style='margin-top:12.5pt;margin-right:0in;
          margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;
          direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-size:13.5pt;
          font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1578;&#1578;&#1590;&#1605;&#1606;
          &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
          &#1580;&#1605;&#1610;&#1593; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
          &#1608;&#1575;&#1604;&#1571;&#1581;&#1603;&#1575;&#1605; &#1575;&#1604;&#1578;&#1610;
          &#1578;&#1591;&#1576;&#1602; &#1593;&#1604;&#1609; &#1586;&#1608;&#1575;&#1585;
          &#1578;&#1591;&#1576;&#1610;&#1602; &#1578;&#1610;&#1603; &#1575;&#1578;&#1587;
          . &#1610;&#1593;&#1583; &#1583;&#1582;&#1608;&#1604;
          &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;
          &#1608;&#1578;&#1589;&#1601;&#1581;&#1607;
          &#1608;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;&#1607;
          &#1576;&#1605;&#1579;&#1575;&#1576;&#1577; &#1602;&#1576;&#1608;&#1604;
          &#1571;&#1581;&#1603;&#1575;&#1605; &#1608;&#1588;&#1585;&#1608;&#1591;
          &#1575;&#1604;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
          &#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577; &#1601;&#1610;
          &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
          &#1583;&#1608;&#1606; &#1602;&#1610;&#1583; &#1571;&#1608;
          &#1588;&#1585;&#1591;&#1548; &#1608;&#1578;&#1581;&#1604; &#1605;&#1581;&#1604;
          &#1580;&#1605;&#1610;&#1593; &#1575;&#1604;&#1575;&#1578;&#1601;&#1575;&#1602;&#1610;&#1575;&#1578;
          &#1575;&#1604;&#1587;&#1575;&#1576;&#1602;&#1577; &#1571;&#1608;
          &#1575;&#1604;&#1605;&#1572;&#1602;&#1578;&#1577;&#1548;
          &#1608;&#1575;&#1604;&#1578;&#1605;&#1579;&#1610;&#1604;&#1575;&#1578;
          &#1608;&#1575;&#1604;&#1590;&#1605;&#1575;&#1606;&#1575;&#1578;
          &#1608;&#1575;&#1604;&#1575;&#1578;&#1601;&#1575;&#1602;&#1575;&#1578;
          &#1591;&#1576;&#1602;&#1611;&#1575;
          &#1604;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;
          &#1608;&#1575;&#1604;&#1605;&#1581;&#1578;&#1608;&#1609;
          &#1575;&#1604;&#1584;&#1610; &#1610;&#1602;&#1583;&#1605;&#1607;
          &#1608;&#1605;&#1608;&#1590;&#1608;&#1593;
          &#1575;&#1604;&#1575;&#1578;&#1601;&#1575;&#1602;&#1610;&#1577;</span><span
          dir=LTR></span><span dir=LTR style='font-size:13.5pt;font-family:"brandfont",sans-serif;
          color:#3A5DF7'><span dir=LTR></span>.</span></p>

          <p class=text-right dir=RTL style='margin-top:22.5pt;margin-right:0in;
          margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;
          direction:rtl;unicode-bidi:embed'><strong><span lang=AR-SA style='font-size:
          15.0pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1575;&#1604;&#1578;&#1581;&#1585;&#1610;&#1585;
          &#1608;&#1575;&#1604;&#1581;&#1584;&#1601; &#1608;&#1575;&#1604;&#1578;&#1593;&#1583;&#1610;&#1604;
          </span></strong><span lang=AR-SA style='font-size:13.5pt;font-family:"brandfont",sans-serif;
          color:#3A5DF7'>&#1602;&#1583; &#1610;&#1604;&#1580;&#1571;
          &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602; &#1573;&#1604;&#1609;
          &#1578;&#1581;&#1585;&#1610;&#1585; &#1571;&#1608; &#1581;&#1584;&#1601;
          &#1571;&#1608; &#1578;&#1593;&#1583;&#1610;&#1604; &#1571;&#1610;
          &#1605;&#1606; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
          &#1608;&#1575;&#1604;&#1571;&#1581;&#1603;&#1575;&#1605;
          &#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577; &#1601;&#1610;
          &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1601;&#1610;
          &#1571;&#1610; &#1608;&#1602;&#1578; &#1608;&#1576;&#1605;&#1581;&#1590;
          &#1575;&#1582;&#1578;&#1610;&#1575;&#1585;&#1607;&#1548;
          &#1608;&#1584;&#1604;&#1603; &#1576;&#1606;&#1588;&#1585;
          &#1573;&#1588;&#1593;&#1575;&#1585; &#1571;&#1608; &#1608;&#1579;&#1610;&#1602;&#1577;
          &#1580;&#1583;&#1610;&#1583;&#1577;
          &#1576;&#1575;&#1604;&#1605;&#1584;&#1603;&#1608;&#1585;&#1593;&#1604;&#1609;
          &#1578;&#1591;&#1576;&#1610;&#1602; &#1578;&#1610;&#1603; &#1575;&#1578;&#1587;
          ".</span></p>

          <p class=text-right dir=RTL style='margin-top:22.5pt;margin-right:0in;
          margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;
          direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-size:13.5pt;
          font-family:"brandfont",sans-serif;color:#3A5DF7'>"&#1608;&#1610;&#1593;&#1583;
          &#1586;&#1610;&#1575;&#1585;&#1577; &#1578;&#1591;&#1576;&#1610;&#1602; &#1578;&#1610;&#1603;
          &#1575;&#1578;&#1587; "&#1576;&#1593;&#1583; &#1606;&#1588;&#1585;
          &#1571;&#1610; &#1605;&#1606;
          &#1575;&#1604;&#1573;&#1588;&#1593;&#1575;&#1585;&#1575;&#1578;
          &#1575;&#1604;&#1578;&#1610; &#1578;&#1588;&#1610;&#1585; &#1573;&#1604;&#1609;
          &#1578;&#1594;&#1610;&#1610;&#1585; &#1601;&#1610; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
          &#1571;&#1608; &#1575;&#1604;&#1571;&#1581;&#1603;&#1575;&#1605; &#1571;&#1608;
          &#1606;&#1588;&#1585; &#1608;&#1579;&#1575;&#1574;&#1602;
          &#1580;&#1583;&#1610;&#1583;&#1577;&#1548;
          &#1576;&#1605;&#1579;&#1575;&#1576;&#1577; &#1602;&#1576;&#1608;&#1604;
          &#1575;&#1604;&#1593;&#1605;&#1610;&#1604; &#1604;&#1607;&#1584;&#1607;
          &#1575;&#1604;&#1578;&#1594;&#1610;&#1610;&#1585;&#1575;&#1578;</span></p>

          <p class=text-right dir=RTL style='margin-top:22.5pt;margin-right:0in;
          margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;
          direction:rtl;unicode-bidi:embed'><strong><span lang=AR-SA style='font-size:
          15.0pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1583;&#1602;&#1577;
          &#1608;&#1575;&#1603;&#1578;&#1605;&#1575;&#1604;
          &#1608;&#1578;&#1608;&#1602;&#1610;&#1578;
          &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;</span></strong><span
          dir=LTR></span><strong><span lang=AR-SA dir=LTR style='font-size:15.0pt;
          font-family:"brandfont",sans-serif;color:#3A5DF7'><span dir=LTR></span> </span></strong><span
          lang=AR-SA style='font-size:13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;
          &#1594;&#1610;&#1585; &#1605;&#1587;&#1572;&#1608;&#1604; &#1601;&#1610;
          &#1581;&#1575;&#1604;&#1577; &#1606;&#1588;&#1585; &#1571;&#1610;
          &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578; &#1594;&#1610;&#1585;
          &#1583;&#1602;&#1610;&#1602;&#1577; &#1571;&#1608;
          &#1603;&#1575;&#1605;&#1604;&#1577; &#1571;&#1608;
          &#1581;&#1583;&#1610;&#1579;&#1577;. &#1573;&#1606;
          &#1575;&#1604;&#1594;&#1585;&#1590; &#1605;&#1606;
          &#1575;&#1604;&#1605;&#1608;&#1575;&#1583; &#1575;&#1604;&#1578;&#1610;
          &#1610;&#1606;&#1588;&#1585;&#1607;&#1575;
          &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602; &#1607;&#1608;
          &#1575;&#1604;&#1581;&#1589;&#1608;&#1604; &#1593;&#1604;&#1609;
          &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578; &#1593;&#1575;&#1605;&#1577;
          &#1608;&#1586;&#1610;&#1575;&#1583;&#1577; &#1575;&#1604;&#1608;&#1593;&#1610;
          &#1608;&#1578;&#1576;&#1575;&#1583;&#1604; &#1575;&#1604;&#1605;&#1593;&#1585;&#1601;&#1577;
          &#1601;&#1610; &#1605;&#1580;&#1575;&#1604;&#1575;&#1578;
          &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1577;
          &#1575;&#1604;&#1605;&#1578;&#1593;&#1583;&#1583;&#1577; &#1601;&#1602;&#1591;.
          &#1610;&#1587;&#1578;&#1582;&#1583;&#1605;
          &#1575;&#1604;&#1586;&#1575;&#1574;&#1585; &#1571;&#1610; &#1605;&#1606;
          &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
          &#1575;&#1604;&#1605;&#1608;&#1580;&#1608;&#1583;&#1577; &#1593;&#1604;&#1609;
          &#1607;&#1584;&#1575; &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;
          &#1593;&#1604;&#1609; &#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1578;&#1607;
          &#1575;&#1604;&#1582;&#1575;&#1589;&#1577;. &#1610;&#1602;&#1585;
          &#1608;&#1610;&#1608;&#1575;&#1601;&#1602; &#1575;&#1604;&#1586;&#1575;&#1574;&#1585;
          &#1571;&#1606;&#1607; &#1605;&#1587;&#1572;&#1608;&#1604; &#1593;&#1606;
          &#1585;&#1589;&#1583;
          &#1575;&#1604;&#1578;&#1594;&#1610;&#1610;&#1585;&#1575;&#1578;
          &#1575;&#1604;&#1578;&#1610; &#1602;&#1583; &#1578;&#1591;&#1585;&#1571; &#1593;&#1604;&#1609;
          &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;</span></p>

          <p class=text-right dir=RTL style='margin-top:22.5pt;margin-right:0in;
          margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;
          direction:rtl;unicode-bidi:embed'><strong><span lang=AR-SA style='font-size:
          15.0pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1575;&#1604;&#1608;&#1589;&#1604;&#1575;&#1578;
          &#1604;&#1605;&#1608;&#1575;&#1602;&#1593; &#1608;
          &#1578;&#1591;&#1576;&#1610;&#1602;&#1575;&#1578; &#1571;&#1582;&#1585;&#1609;
          &#1608;&#1575;&#1604;&#1589;&#1610;&#1575;&#1594;&#1577;</span></strong><span
          dir=LTR></span><strong><span lang=AR-SA dir=LTR style='font-size:15.0pt;
          font-family:"brandfont",sans-serif;color:#3A5DF7'><span dir=LTR></span> </span></strong><span
          lang=AR-SA style='font-size:13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;
          &#1594;&#1610;&#1585; &#1605;&#1587;&#1572;&#1608;&#1604; &#1593;&#1606;
          &#1606;&#1608;&#1593;&#1610;&#1577; &#1571;&#1608;
          &#1605;&#1581;&#1578;&#1608;&#1609; &#1571;&#1608; &#1591;&#1576;&#1610;&#1593;&#1577;
          &#1571;&#1608; &#1583;&#1602;&#1577; &#1571;&#1610;
          &#1578;&#1591;&#1576;&#1610;&#1602; &#1570;&#1582;&#1585;
          &#1610;&#1605;&#1603;&#1606; &#1575;&#1604;&#1608;&#1589;&#1608;&#1604;
          &#1573;&#1604;&#1610;&#1607; &#1605;&#1606; &#1582;&#1604;&#1575;&#1604;
          &#1585;&#1575;&#1576;&#1591; &#1593;&#1604;&#1609;
          &#1578;&#1591;&#1576;&#1610;&#1602; &#1578;&#1610;&#1603; &#1575;&#1578;&#1587;
          &#1571;&#1608; &#1575;&#1604;&#1605;&#1608;&#1575;&#1602;&#1593;
          &#1575;&#1604;&#1605;&#1585;&#1578;&#1576;&#1591;&#1577; &#1576;&#1607;
          &#1587;&#1608;&#1575;&#1569; &#1576;&#1591;&#1585;&#1610;&#1602;&#1577;
          &#1605;&#1576;&#1575;&#1588;&#1585;&#1577; &#1571;&#1608; &#1594;&#1610;&#1585;
          &#1605;&#1576;&#1575;&#1588;&#1585;&#1577;. &#1608;&#1581;&#1610;&#1579;
          &#1573;&#1606; &#1607;&#1584;&#1607;
          &#1575;&#1604;&#1605;&#1608;&#1575;&#1602;&#1593; &#1604;&#1575;
          &#1578;&#1582;&#1590;&#1593; &#1604;&#1585;&#1602;&#1575;&#1576;&#1578;&#1606;&#1575;&#1548;
          &#1601;&#1573;&#1606;&#1606;&#1575; &#1594;&#1610;&#1585;
          &#1605;&#1587;&#1572;&#1608;&#1604;&#1608;&#1606; &#1593;&#1606;
          &#1605;&#1581;&#1578;&#1608;&#1610;&#1575;&#1578; &#1571;&#1610; &#1578;&#1591;&#1576;&#1610;&#1602;
          &#1605;&#1585;&#1578;&#1576;&#1591; &#1576;&#1578;&#1591;&#1576;&#1610;&#1602;
          &#1578;&#1610;&#1603; &#1575;&#1578;&#1587; ""&#1571;&#1608; &#1571;&#1610;
          &#1585;&#1575;&#1576;&#1591; &#1601;&#1610; &#1571;&#1610;
          &#1578;&#1591;&#1576;&#1610;&#1602; &#1584;&#1575;&#1578; &#1589;&#1604;&#1577;
          &#1576;&#1607;&#1548; &#1571;&#1608; &#1571;&#1610;
          &#1589;&#1601;&#1581;&#1577; &#1575;&#1587;&#1578;&#1593;&#1585;&#1575;&#1590;
          &#1571;&#1608; &#1571;&#1610; &#1578;&#1594;&#1610;&#1610;&#1585;&#1575;&#1578;
          &#1608;&#1578;&#1581;&#1583;&#1610;&#1579;&#1575;&#1578;
          &#1578;&#1582;&#1590;&#1593; &#1604;&#1607;&#1575; &#1607;&#1584;&#1607;
          &#1575;&#1604;&#1605;&#1608;&#1575;&#1602;&#1593;. &#1610;&#1585;&#1580;&#1609;
          &#1575;&#1604;&#1578;&#1608;&#1580;&#1607;
          &#1604;&#1605;&#1587;&#1572;&#1608;&#1604;
          &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602; &#1575;&#1604;&#1582;&#1575;&#1585;&#1580;&#1610;
          &#1605;&#1576;&#1575;&#1588;&#1585;&#1577; &#1593;&#1606;&#1583;
          &#1581;&#1583;&#1608;&#1579; &#1571;&#1610; &#1605;&#1588;&#1603;&#1604;&#1577;
          &#1576;&#1588;&#1571;&#1606; &#1575;&#1604;&#1585;&#1575;&#1576;&#1591;</span></p>

          <p class=text-right dir=RTL style='margin-top:22.5pt;margin-right:0in;
          margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;
          direction:rtl;unicode-bidi:embed'><strong><span lang=AR-SA style='font-size:
          15.0pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1573;&#1582;&#1604;&#1575;&#1569;
          &#1575;&#1604;&#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1577;</span></strong><span
          dir=LTR></span><strong><span lang=AR-SA dir=LTR style='font-size:15.0pt;
          font-family:"brandfont",sans-serif;color:#3A5DF7'><span dir=LTR></span> </span></strong><span
          lang=AR-SA style='font-size:13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1610;&#1587;&#1578;&#1582;&#1583;&#1605;
          &#1575;&#1604;&#1586;&#1575;&#1574;&#1585;
          &#1578;&#1591;&#1576;&#1610;&#1602;&#1606;&#1575; &#1601;&#1610; &#1571;&#1610;
          &#1608;&#1602;&#1578; &#1593;&#1604;&#1609;
          &#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1578;&#1607;
          &#1575;&#1604;&#1588;&#1582;&#1589;&#1610;&#1577;. &#1608;&#1610;&#1581;&#1602;
          &#1604;&#1606;&#1575; &#1578;&#1602;&#1610;&#1610;&#1583; &#1571;&#1608;
          &#1573;&#1606;&#1607;&#1575;&#1569; &#1575;&#1604;&#1608;&#1589;&#1608;&#1604;
          &#1573;&#1604;&#1609; &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;
          &#1571;&#1608; &#1571;&#1610; &#1580;&#1586;&#1569; &#1605;&#1606;&#1607;
          &#1601;&#1610; &#1571;&#1610; &#1608;&#1602;&#1578;</span><span dir=LTR></span><span
          dir=LTR style='font-size:13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'><span
          dir=LTR></span>.</span></p>

          <p class=text-right dir=RTL style='margin-top:22.5pt;margin-right:0in;
          margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;
          direction:rtl;unicode-bidi:embed'><strong><span lang=AR-SA style='font-size:
          15.0pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;
          &#1608;&#1581;&#1602;&#1608;&#1602;
          &#1575;&#1604;&#1605;&#1604;&#1603;&#1610;&#1577;
          &#1575;&#1604;&#1601;&#1603;&#1585;&#1610;&#1577; </span></strong><span
          lang=AR-SA style='font-size:13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1610;&#1608;&#1575;&#1601;&#1602;
          &#1575;&#1604;&#1586;&#1575;&#1574;&#1585; &#1593;&#1604;&#1609;
          &#1580;&#1605;&#1610;&#1593; &#1575;&#1604;&#1581;&#1602;&#1608;&#1602;
          &#1608;&#1575;&#1604;&#1571;&#1604;&#1602;&#1575;&#1576; &#1608;&#1575;&#1604;&#1605;&#1589;&#1575;&#1604;&#1581;&#1548;
          &#1576;&#1605;&#1575; &#1601;&#1610; &#1584;&#1604;&#1603;
          &#1593;&#1604;&#1609; &#1587;&#1576;&#1610;&#1604;
          &#1575;&#1604;&#1605;&#1579;&#1575;&#1604; &#1604;&#1575;
          &#1575;&#1604;&#1581;&#1589;&#1585; &#1575;&#1604;&#1581;&#1602;&#1608;&#1602;
          &#1575;&#1604;&#1578;&#1610; &#1578;&#1594;&#1591;&#1610;&#1607;&#1575;
          &#1581;&#1602;&#1608;&#1602; &#1575;&#1604;&#1605;&#1604;&#1603;&#1610;&#1577;
          &#1575;&#1604;&#1601;&#1603;&#1585;&#1610;&#1577; &#1593;&#1604;&#1609;
          &#1607;&#1584;&#1575; &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1548;
          &#1605;&#1581;&#1601;&#1608;&#1592;&#1577;
          &#1604;&#1604;&#1605;&#1608;&#1602;&#1593; &#1608; &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;.
          &#1608;&#1571;&#1606;&#1607; &#1604;&#1575; &#1610;&#1580;&#1608;&#1586;
          &#1575;&#1603;&#1578;&#1587;&#1575;&#1576; &#1571;&#1610; &#1581;&#1602;
          &#1571;&#1608; &#1605;&#1604;&#1603;&#1610;&#1577; &#1571;&#1608;
          &#1605;&#1589;&#1604;&#1581;&#1577; &#1605;&#1606;
          &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;
          &#1576;&#1575;&#1587;&#1578;&#1579;&#1606;&#1575;&#1569; &#1605;&#1575;
          &#1607;&#1608; &#1605;&#1606;&#1589;&#1608;&#1589; &#1593;&#1604;&#1610;&#1607;
          &#1589;&#1585;&#1575;&#1581;&#1577; &#1601;&#1610; &#1607;&#1584;&#1607;
          &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.
          &#1610;&#1578;&#1593;&#1607;&#1583; &#1575;&#1604;&#1593;&#1605;&#1610;&#1604;
          &#1576;&#1593;&#1583;&#1605; &#1578;&#1593;&#1583;&#1610;&#1604; &#1571;&#1608;
          &#1605;&#1604;&#1575;&#1574;&#1605;&#1577; &#1571;&#1608;
          &#1578;&#1585;&#1580;&#1605;&#1577;&#1548; &#1571;&#1608;
          &#1573;&#1593;&#1583;&#1575;&#1583; &#1571;&#1593;&#1605;&#1575;&#1604;
          &#1571;&#1608; &#1578;&#1601;&#1603;&#1610;&#1603; &#1571;&#1608;
          &#1573;&#1593;&#1575;&#1583;&#1577; &#1607;&#1606;&#1583;&#1587;&#1577;
          &#1571;&#1610; &#1605;&#1606; &#1575;&#1604;&#1582;&#1583;&#1605;&#1575;&#1578;
          &#1608;&#1575;&#1604;&#1576;&#1585;&#1605;&#1580;&#1610;&#1575;&#1578;
          &#1608;&#1575;&#1604;&#1608;&#1579;&#1575;&#1574;&#1602;
          &#1575;&#1604;&#1578;&#1610; &#1610;&#1602;&#1583;&#1605;&#1607;&#1575;
          &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1548; &#1571;&#1608;
          &#1573;&#1606;&#1588;&#1575;&#1569; &#1571;&#1608;
          &#1605;&#1581;&#1575;&#1608;&#1604;&#1577; &#1573;&#1606;&#1588;&#1575;&#1569;
          &#1582;&#1583;&#1605;&#1577; &#1576;&#1583;&#1610;&#1604;&#1577; &#1571;&#1608;
          &#1605;&#1588;&#1575;&#1576;&#1607;&#1577; &#1571;&#1608; &#1571;&#1610;
          &#1605;&#1606;&#1578;&#1580; &#1593;&#1606; &#1591;&#1585;&#1610;&#1602;
          &#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
          &#1576;&#1585;&#1575;&#1605;&#1580; &#1571;&#1608;
          &#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;
          &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;</span></p>

          <p class=text-right dir=RTL style='margin-top:22.5pt;margin-right:0in;
          margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;
          direction:rtl;unicode-bidi:embed'><strong><span lang=AR-SA style='font-size:
          15.0pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1593;&#1583;&#1605;
          &#1575;&#1604;&#1578;&#1606;&#1575;&#1586;&#1604;</span></strong><span dir=LTR></span><strong><span
          lang=AR-SA dir=LTR style='font-size:15.0pt;font-family:"brandfont",sans-serif;
          color:#3A5DF7'><span dir=LTR></span> </span></strong><span lang=AR-SA
          style='font-size:13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1604;&#1575;
          &#1610;&#1593;&#1583; &#1593;&#1583;&#1605; &#1578;&#1606;&#1601;&#1610;&#1584;
          &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602; &#1604;&#1571;&#1610;
          &#1605;&#1606; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
          &#1608;&#1575;&#1604;&#1571;&#1581;&#1603;&#1575;&#1605;
          &#1608;&#1575;&#1604;&#1575;&#1604;&#1578;&#1586;&#1575;&#1605;&#1575;&#1578;
          &#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577; &#1601;&#1610;
          &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577; &#1576;&#1581;&#1586;&#1605;
          &#1578;&#1582;&#1604;&#1610; &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;
          &#1571;&#1608; &#1578;&#1606;&#1575;&#1586;&#1604;&#1607; &#1593;&#1606;
          &#1571;&#1610; &#1605;&#1606; &#1575;&#1604;&#1581;&#1602;&#1608;&#1602;
          &#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577; &#1601;&#1610;
          &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
          &#1571;&#1608; &#1571;&#1610;&#1577; &#1588;&#1585;&#1608;&#1591;
          &#1602;&#1583; &#1610;&#1604;&#1580;&#1571;
          &#1575;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602; &#1573;&#1604;&#1609;
          &#1575;&#1578;&#1582;&#1575;&#1584;&#1607; &#1601;&#1610; &#1607;&#1584;&#1575;
          &#1575;&#1604;&#1589;&#1583;&#1583;. &#1603;&#1605;&#1575; &#1604;&#1575;
          &#1610;&#1572;&#1604; &#1584;&#1604;&#1603; &#1593;&#1604;&#1609;
          &#1571;&#1606;&#1607; &#1578;&#1606;&#1575;&#1586;&#1604; &#1593;&#1606;
          &#1571;&#1610; &#1582;&#1585;&#1602; &#1587;&#1575;&#1576;&#1602; &#1571;&#1608;
          &#1604;&#1575;&#1581;&#1602; &#1604;&#1604;&#1588;&#1585;&#1608;&#1591;
          &#1608;&#1575;&#1604;&#1571;&#1581;&#1603;&#1575;&#1605;
          &#1608;&#1575;&#1604;&#1575;&#1604;&#1578;&#1586;&#1575;&#1605;&#1575;&#1578;
          &#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577; &#1601;&#1610;
          &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;.
          &#1608;&#1578;&#1592;&#1604; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
          &#1608;&#1575;&#1604;&#1571;&#1581;&#1603;&#1575;&#1605; &#1608;&#1575;&#1604;&#1575;&#1604;&#1578;&#1586;&#1575;&#1605;&#1575;&#1578;
          &#1587;&#1575;&#1585;&#1610;&#1577; &#1608;&#1606;&#1575;&#1601;&#1584;&#1577;.
          &#1603;&#1605;&#1575; &#1604;&#1575; &#1610;&#1593;&#1583;
          &#1578;&#1606;&#1575;&#1586;&#1604; &#1571;&#1610; &#1605;&#1606;
          &#1575;&#1604;&#1571;&#1591;&#1585;&#1575;&#1601; &#1593;&#1606; &#1571;&#1610;
          &#1605;&#1606; &#1575;&#1604;&#1588;&#1585;&#1608;&#1591;
          &#1608;&#1575;&#1604;&#1571;&#1581;&#1603;&#1575;&#1605; &#1608;&#1575;&#1604;&#1575;&#1604;&#1578;&#1586;&#1575;&#1605;&#1575;&#1578;
          &#1575;&#1604;&#1608;&#1575;&#1585;&#1583;&#1577; &#1601;&#1610;
          &#1607;&#1584;&#1607; &#1575;&#1604;&#1608;&#1579;&#1610;&#1602;&#1577;
          &#1578;&#1606;&#1575;&#1586;&#1604;&#1575; &#1593;&#1606; &#1582;&#1585;&#1602;
          &#1587;&#1575;&#1576;&#1602; &#1571;&#1608; &#1604;&#1575;&#1581;&#1602;
          &#1604;&#1607;&#1584;&#1575; &#1575;&#1604;&#1576;&#1606;&#1583; &#1571;&#1608;
          &#1571;&#1610; &#1576;&#1606;&#1583; &#1570;&#1582;&#1585;</span></p>

          <p class=text-right dir=RTL style='margin-top:22.5pt;margin-right:0in;
          margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;
          direction:rtl;unicode-bidi:embed'><strong><span lang=AR-SA style='font-size:
          15.0pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1575;&#1587;&#1578;&#1602;&#1604;&#1575;&#1604;&#1610;&#1577;
          &#1575;&#1604;&#1606;&#1589;&#1608;&#1589;</span></strong><span dir=LTR></span><strong><span
          lang=AR-SA dir=LTR style='font-size:15.0pt;font-family:"brandfont",sans-serif;
          color:#3A5DF7'><span dir=LTR></span> </span></strong><span lang=AR-SA
          style='font-size:13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>&#1573;&#1584;&#1575;
          &#1571;&#1589;&#1576;&#1581; &#1571;&#1610; &#1580;&#1586;&#1569;
          &#1605;&#1606; &#1588;&#1585;&#1608;&#1591;
          &#1575;&#1604;&#1575;&#1587;&#1578;&#1582;&#1583;&#1575;&#1605;
          &#1594;&#1610;&#1585; &#1606;&#1575;&#1601;&#1584; &#1571;&#1608;
          &#1594;&#1610;&#1585; &#1602;&#1575;&#1576;&#1604;
          &#1604;&#1604;&#1578;&#1591;&#1576;&#1610;&#1602;&#1548;
          &#1601;&#1610;&#1601;&#1587;&#1585; &#1584;&#1604;&#1603;
          &#1575;&#1604;&#1580;&#1586;&#1569; &#1576;&#1591;&#1585;&#1610;&#1602;&#1577; &#1578;&#1578;&#1608;&#1575;&#1601;&#1602;
          &#1605;&#1593; &#1575;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606;
          &#1575;&#1604;&#1587;&#1575;&#1585;&#1610; &#1604;&#1610;&#1593;&#1603;&#1587;
          &#1576;&#1602;&#1583;&#1585; &#1575;&#1604;&#1573;&#1605;&#1603;&#1575;&#1606;
          &#1575;&#1604;&#1606;&#1608;&#1575;&#1610;&#1575;
          &#1575;&#1604;&#1571;&#1589;&#1604;&#1610;&#1577;
          &#1604;&#1604;&#1591;&#1585;&#1601;&#1610;&#1606;&#1548; &#1608;&#1578;&#1592;&#1604;
          &#1576;&#1575;&#1602;&#1610; &#1575;&#1604;&#1571;&#1580;&#1586;&#1575;&#1569;
          &#1575;&#1604;&#1571;&#1582;&#1585;&#1609; &#1576;&#1603;&#1575;&#1605;&#1604;
          &#1602;&#1608;&#1578;&#1607;&#1575; &#1575;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606;&#1610;&#1577;
          &#1608; &#1587;&#1575;&#1585;&#1610;&#1577;
          &#1575;&#1604;&#1605;&#1601;&#1593;&#1608;&#1604; &#1608;&#1601;&#1602;&#1575;
          &#1604;&#1604;&#1602;&#1575;&#1606;&#1608;&#1606;
          &#1575;&#1604;&#1587;&#1575;&#1585;&#1610;</span><span dir=LTR></span><span
          dir=LTR style='font-size:13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'><span
          dir=LTR></span>.</span></p>

          <p class=text-right dir=RTL style='margin-top:22.5pt;margin-right:0in;
          margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;
          direction:rtl;unicode-bidi:embed'><span lang=AR-SA style='font-size:13.5pt;
          font-family:"brandfont",sans-serif;color:#3A5DF7'>&nbsp;</span></p>

          <p class=text-right dir=RTL style='text-align:justify;direction:rtl;
          unicode-bidi:embed'><span dir=LTR style='font-size:13.5pt;font-family:"brandfont",sans-serif;
          color:#3A5DF7'>This document includes all terms and conditions that apply to
          visitors to the Ondustrial application. Entering, browsing and using the
          application is tantamount to accepting the terms and conditions of use in this
          document without restriction or condition, and supersedes all previous or
          temporary agreements, representations, warranties and agreements in accordance
          with the application, the content it provides and the subject of the agreement</span><span
          dir=RTL></span><span lang=AR-SA style='font-size:13.5pt;font-family:"brandfont",sans-serif;
          color:#3A5DF7'><span dir=RTL></span>.</span></p>

          <p class=text-right dir=RTL style='text-align:justify;direction:rtl;
          unicode-bidi:embed'><span dir=LTR style='font-size:13.5pt;font-family:"brandfont",sans-serif;
          color:#3A5DF7'>Editing, deletion, and amendment The application may resort to
          editing, deleting or amending any of the terms and conditions mentioned in this
          document at any time and at its sole discretion, by publishing a notice or a
          new document mentioned in the application Ondustrial</span><span dir=RTL></span><span
          lang=AR-SA style='font-size:13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'><span
          dir=RTL></span>.</span></p>

          <p class=text-right dir=RTL style='text-align:justify;direction:rtl;
          unicode-bidi:embed'><span dir=RTL></span><span lang=AR-SA style='font-size:
          13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'><span dir=RTL></span>&nbsp;</span><span
          dir=LTR style='font-size:13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'>A
          visit to the Ondustrial application after the posting of any notices indicating a
          change in the terms or conditions or the publication of new documents is
          considered a customer acceptance of these changes</span><span dir=RTL></span><span
          lang=AR-SA style='font-size:13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'><span
          dir=RTL></span>.</span></p>

          <p class=text-right dir=RTL style='text-align:justify;direction:rtl;
          unicode-bidi:embed'><span dir=LTR style='font-size:13.5pt;font-family:"brandfont",sans-serif;
          color:#3A5DF7'>Accuracy, completeness and timeliness of information The
          application is not responsible in the event that any inaccurate, complete or
          recent information is published. The purpose of the material published by the
          application is to obtain general information, raise awareness, and exchange
          knowledge in various fields of tourism only. The visitor uses any information
          on this app at his own risk. The visitor acknowledges and agrees that he is
          responsible for monitoring changes to the application</span></p>

          <p class=text-right dir=RTL style='text-align:justify;direction:rtl;
          unicode-bidi:embed'><span dir=LTR style='font-size:13.5pt;font-family:"brandfont",sans-serif;
          color:#3A5DF7'>Links to other websites and applications, and the formulation The
          application is not responsible for the quality, content, nature or accuracy of
          any other application that can be accessed through a link on the Ondustrial
          application or its related sites, either directly or indirectly. As these sites
          are not subject to our control, we are not responsible for the contents of any
          application related to the Ondustrial application or any link in any related
          application, or any review page or any changes and updates to which these sites
          are subject. Please contact the external application administrator directly
          when any problem with the link occurs</span></p>

          <p class=text-right dir=RTL style='text-align:justify;direction:rtl;
          unicode-bidi:embed'><span dir=LTR style='font-size:13.5pt;font-family:"brandfont",sans-serif;
          color:#3A5DF7'>Disclaimer: The visitor uses our app at any time on his personal
          responsibility. We may restrict or terminate access to the application or any
          part of it at any time</span><span dir=RTL></span><span lang=AR-SA
          style='font-size:13.5pt;font-family:"brandfont",sans-serif;color:#3A5DF7'><span
          dir=RTL></span>.</span></p>

          <p class=text-right dir=RTL style='text-align:justify;direction:rtl;
          unicode-bidi:embed'><span dir=LTR style='font-size:13.5pt;font-family:"brandfont",sans-serif;
          color:#3A5DF7'>Application and intellectual property rights The visitor agrees
          to all rights, titles and interests, including but not limited to the rights
          covered by intellectual property rights on this application, reserved for the
          site and application. And that it is not permissible to acquire any right,
          ownership or interest from the application except as expressly provided for in
          this document. The customer undertakes not to modify, adapt, translate, prepare
          works, disassemble, or re-engineer any of the services, software, and documents
          provided by the application, or create or attempt to create an alternative or
          similar service or any product by using software or application information</span></p>

          <p class=text-right dir=RTL style='text-align:justify;direction:rtl;
          unicode-bidi:embed'><span dir=LTR style='font-size:13.5pt;font-family:"brandfont",sans-serif;
          color:#3A5DF7'>Failure to relinquish Failure to implement the application shall
          not be considered as any of the terms, conditions and obligations set forth in
          this document with the intention of relinquishing the application or
          relinquishing any of the rights mentioned in this document or any conditions
          that the application may resort to taking in this regard. Nor shall this be
          construed as a waiver of any past or future breach of the terms, conditions and
          obligations contained in this document. The terms, conditions and obligations
          remain in force and enforceable. The assignment of any of the parties to any of
          the terms, conditions, and obligations mentioned in this document is not
          considered a waiver of a previous or a later breach of this clause or any other
          clause</span><span dir=RTL></span><span lang=AR-SA style='font-size:13.5pt;
          font-family:"brandfont",sans-serif;color:#3A5DF7'><span dir=RTL></span>.</span></p>

          <p class=text-right dir=RTL style='margin-top:22.5pt;margin-right:0in;
          margin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;
          direction:rtl;unicode-bidi:embed'><span dir=LTR style='font-size:13.5pt;
          font-family:"brandfont",sans-serif;color:#3A5DF7'>Independence of the texts If
          any part of the terms of use becomes impermissible or not applicable, then that
          part is interpreted in a manner consistent with the applicable law to reflect
          as much as possible the original intentions of the parties, and the rest of the
          other parts remain at full legal force and are in force in accordance with the
          applicable law</span></p>

          <p class=MsoNormal dir=RTL style='text-align:justify;direction:rtl;unicode-bidi:
          embed'><span dir=LTR style='font-size:14.0pt;line-height:107%'>&nbsp;</span></p>

          </div>

    </div>
    <!-- footer -->
    <section class="footer">
        <div class="container">
            <footer class="">
                <div class="d-flex justify-content-center">
                    <ul class="row">
                        <a href="#" style="padding-right: 10px;"><i class="fab fa-facebook fa-2x"></i></a>
                        <a href="#" style="padding-right: 10px;"><i class="fab fa-twitter fa-2x"></i></a>
                        <a href="#" style="padding-right: 45px;"><i class="fab fa-instagram fa-2x"></i></a>
                    </ul>
                </div><!-- Ende Sozial media -->
                <div class="d-flex justify-content-center">
                    <p> All rights reseved </p>
                </div><!-- Ende Copyright -->
            </footer>
        </div>
    </section>
</body>

<script>
    //ScrollReveal().reveal('.headline');
    //for nav bar
    $('.navbar-nav .nav-link small').click(function () {
        $('.navbar-nav .nav-link small').removeClass('active');
        $(this).addClass('active');
    });
</script>

</html>
