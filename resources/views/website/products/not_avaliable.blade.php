@extends('layouts.website')

@section('content')

<section class="ftco-section bg-light">
    <div class="container ftco-animate">
        <div class="row shadow pt-3">
            <div class="col-lg-12 mb-5  text-center py-5 ">
                   <p><b class=""> {{$message}}</b></p>
                   <a href={{route('index')}} class="btn bg-transparent " >
                    <span class="add-to-cart-btn">Continue Shopping <span class="icon-shopping_cart"></span></span>
                </a>

            </div>
        </div>
    </div>
</div>

</section>


@if($similars->count()>0)
<section class="ftco-section " >
    <div class="container-fluid ">
        <div class="row justify-content-center mb-3 pb-3">
      <div class="col-md-12 heading-section text-center ">
          <h1 class="big">Similar Products</h1>
        <h2 class="mb-4">Similar Products</h2>
      </div>
    </div>
        <div class="row">
            <div class="col-md-12">
                <div class="product-slider owl-carousel  ">
        @foreach($similars as $product)
        @include('layouts.includes.items.product_item',
        ['field' => ['name' => $product->name, 'price' => $product->price ,'rate'=>$product->rate ,'id'=>$product->id,'image'=>$product->image]])
  @endforeach
            </div>
        </div>
    </div>
</section>
@endif

<div class="modal " id="modal_message" tabIndex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">My Wishlists</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <div>
                          @auth
                          <b>{{ session('modal_message') }}</b>
                          @endauth

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@if(!empty(Session::get('modal_message')))
<script>
$(function() {
    $('#modal_message').modal('show');
});
</script>
@endif
@endsection
