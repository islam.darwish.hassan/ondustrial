@extends('website.layouts.main')

@section('content')
 <!------------------------------------------------------------ product image & price -->
 <div class="container mt-3">
    <p class="text-muted">Home/Electrical Parts/Motor Inventors/CL 10000 delta UPS</p>
    <div class="row">
        <!-- product thumbnail image -->
        <div class="col-1 text-center">
            <a href=""><img class="img-thumbnail img-thumbnail-size mb-1" src="{{asset('./img/n-seires-500x500.jpg')}}"
                    alt="..."></a>
            <a href=""><img class="img-thumbnail img-thumbnail-size mb-1" src="{{asset('./img/n-seires-500x500.jpg')}}"
                    alt="..."></a>
            <a href=""><img class="img-thumbnail img-thumbnail-size mb-1" src="{{asset('./img/n-seires-500x500.jpg')}}"
                    alt="..."></a>
            <a href=""><img class="img-thumbnail img-thumbnail-size mb-1" src="{{asset('./img/n-seires-500x500.jpg')}}"
                    alt="..."></a>
            <a href=""><img class="img-thumbnail img-thumbnail-size mb-1" src="{{asset('./img/n-seires-500x500.jpg')}}"
                    alt="..."></a>
        </div>
        <!-- product main image -->
        <div class="col-5 text-center">
            <img class="img-thumbnail product-image-size" src="{{asset('./img/n-seires-500x500.jpg')}}" alt="...">
        </div>
        <!-- product information -->
        <div class="col-6 text-left">
            <!-- product review rate -->
            <div>
                <ul class="list-inline small">
                    <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                    <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                    <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                    <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                    <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                    <li class="list-inline-item m-0">
                        <p class=" pl-3">3 Customers Reviews</p>
                    </li>
                </ul>
            </div>
            <!-- product name & availability -->
            <div>
                <h4>CL 10000 delta UPS</h4>
                <p class="small text-success">In Stock</p>
            </div>
            <!-- product price & favourite & cart buttons -->
            <div class="row align-items-start">
                <!-- product price -->
                <div class="col-4">
                    <h5>350.00 EGP</h5>
                    <p class="small text-muted">0% Discount</p>
                </div>
                <!-- favourite & cart buttons-->
                <div class="col-8">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" class="btn btn-light mx-2"><i
                                class="fas fa-heart text-success"></i></button>
                        <button type="button" class="btn btn-light mx-2"><i
                                class="fas fa-shopping-cart text-success mr-1"></i>Cart</button>
                    </div>
                </div>
            </div>
            <!-- additional information -->
            <div class="row my-5">
                <!-- 1st col -->
                <div class="col-3">
                    <ul class="footer-ul">
                        <li class="footer-li">
                            <p class="small">Availability</p>
                        </li>
                        <li class="footer-li">
                            <p class="small">Quantity</p>
                        </li>
                        <li class="footer-li">
                            <p class="small">Delivery</p>
                        </li>
                        <li class="footer-li">
                            <p class="small">payment</p>
                        </li>
                    </ul>
                </div>
                <!-- 2nd col -->
                <div class="col-9">
                    <ul class="footer-ul">
                        <li class="footer-li">
                            <p class="small">15 items(s)</p>
                        </li>
                        <li class="footer-li">
                            <p class="small">15 item(s)</p>
                        </li>
                        <li class="footer-li">
                            <p class="small">Lorem ipsum dolor, incidunt!</p>
                        </li>
                        <li class="footer-li">
                            <i class="fab fa-cc-visa fa-1x mx-2 text-success"></i>
                            <i class="fab fa-cc-paypal fa-1x mx-2 text-success"></i>
                            <i class="fab fa-cc-mastercard fa-1x mx-2 text-success"></i>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- button group -->
<div class="container b-3">
    <button type="button" class="btn btn-outline-secondary cards-menu-button-2 mb-1">Description</button>
    <button type="button" class="btn btn-outline-secondary cards-menu-button-2 mb-1">Additional Information</button>
    <button type="button" class="btn btn-outline-secondary cards-menu-button-2 mb-1">Reviews</button>
    <button type="button" class="btn btn-outline-secondary cards-menu-button-2 mb-1">Download</button>
</div>

<!---------------------------------------------------------- information div start------------------------------------------------------------>
<div class="container info-table-section">
    <!---------------------------------------------------- description section start---------------------------------------------------------->
    <!-- header -->
    <div class="row pl-4 py-3">
        <h4>Description</h4>
    </div>
    <div class="row">
        <div class="col">
            <p class="pl-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam quas autem, unde et
                qui
                illo explicabo voluptates facilis accusamus, sit cum deleniti corporis possimus laudantium
                asperiores ad ab consectetur voluptatem! Lorem ipsum dolor, sit amet consectetur adipisicing
                elit.
                Perspiciatis quos exercitationem recusandae quasi officia alias, minus laborum repellat mollitia
                at
                ut aperiam magnam perferendis, voluptate eligendi! Error praesentium eaque atque?</p>
        </div>
    </div>
    <!----------------------------------------------------- description section end---------------------------------------------------------->
    <hr>
    <!-------------------------------------------------- information table section start----------------------------------------------------->
    <!-- header -->
    <div class="row pl-4 py-3">
        <h4>Additional Information</h4>
    </div>
    <!-- table data -->
    <div class="row">
        <div class="col-2">
            <p class="text-muted pl-2">Brand</p>
        </div>
        <div class="col-10">
            <p>lorem</p>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <p class="text-muted pl-2">Model</p>
        </div>
        <div class="col-10">
            <p>lorem</p>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <p class="text-muted pl-2">Lorem</p>
        </div>
        <div class="col-10">
            <p>lorem</p>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <p class="text-muted pl-2">Lorem</p>
        </div>
        <div class="col-10">
            <p>lorem</p>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <p class="text-muted pl-2">Lorem</p>
        </div>
        <div class="col-10">
            <p>lorem</p>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <p class="text-muted pl-2">Lorem</p>
        </div>
        <div class="col-10">
            <p>lorem</p>
        </div>
    </div>
    <!----------------------------------------------------- information table section end---------------------------------------------------->
    <hr>
    <!-------------------------------------------------------- review section start---------------------------------------------------------->
    <!-- header -->
    <div class="row pl-3 pt-3">
        <h4>Reviews (3)</h4>
    </div>
    <!-- Write Review button -->
    <div class="row pl-3 pt-2">
        <button type="button" class="btn btn-outline-secondary cards-menu-button-2">Write Review</button>
    </div>
    <!-- review carousel -->
    <div id="carouselExampleInterval" class="carousel slide pt-3" data-ride="carousel">
        <div class="carousel-inner">
            <!-- first slide -->
            <div class="carousel-item active" data-interval="10000">
                <div class="container">
                    <!-- review content -->
                    <div class="row align-items-center pt-2">
                        <!-- previous button -->
                        <div class="col-1 text-right">
                            <i class="fas fa-arrow-circle-left fa-2x text-success"></i>
                        </div>
                        <!-- personal image -->
                        <div class="col-2 d-flex justify-content-center">
                            <img class="review-image" src="{{asset('./img/useravatar.png')}}" alt="...">
                        </div>
                        <!-- personal review -->
                        <div class="col-8 justify-content-center">
                            <h5>Ahmed Elsheshtawi</h5>
                            <ul class="list-inline small">
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                            </ul>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Atque minus excepturi
                                obcaecati, architecto
                                nulla hic nostrum, veniam ratione quae eum similique voluptas fugiat neque
                                pariatur
                                iusto. Ullam
                                deserunt doloremque repellendus.</p>
                            <p class="small">12 jun 2020</p>
                        </div>
                        <!-- next button -->
                        <div class="col-1 text-left">
                            <i class="fas fa-arrow-circle-right fa-2x text-success"></i>
                        </div>
                    </div>
                </div>
            </div>
            <!-- second slide -->
            <div class="carousel-item" data-interval="2000">
                <div class="container">
                    <!-- review content -->
                    <div class="row align-items-center pt-2">
                        <!-- previous button -->
                        <div class="col-1 text-right">
                            <i class="fas fa-arrow-circle-left fa-2x text-success"></i>
                        </div>
                        <!-- personal image -->
                        <div class="col-2 d-flex justify-content-center">
                            <img class="review-image" src="{{asset('./img/useravatar.png')}}" alt="...">
                        </div>
                        <!-- personal review -->
                        <div class="col-8 justify-content-center">
                            <h5>Ahmed Elsheshtawi</h5>
                            <ul class="list-inline small">
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                            </ul>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Atque minus excepturi
                                obcaecati, architecto
                                nulla hic nostrum, veniam ratione quae eum similique voluptas fugiat neque
                                pariatur
                                iusto. Ullam
                                deserunt doloremque repellendus.</p>
                            <p class="small">12 jun 2020</p>
                        </div>
                        <!-- next button -->
                        <div class="col-1 text-left">
                            <i class="fas fa-arrow-circle-right fa-2x text-success"></i>
                        </div>
                    </div>
                </div>
            </div>
            <!-- third slide -->
            <div class="carousel-item">
                <div class="container">
                    <!-- review content -->
                    <div class="row align-items-center pt-2">
                        <!-- previous button -->
                        <div class="col-1 text-right">
                            <i class="fas fa-arrow-circle-left fa-2x text-success"></i>
                        </div>
                        <!-- personal image -->
                        <div class="col-2 d-flex justify-content-center">
                            <img class="review-image" src="{{asset('./img/useravatar.png')}}" alt="...">
                        </div>
                        <!-- personal review -->
                        <div class="col-8 justify-content-center">
                            <h5>Ahmed Elsheshtawi</h5>
                            <ul class="list-inline small">
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                                <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                            </ul>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Atque minus excepturi
                                obcaecati, architecto
                                nulla hic nostrum, veniam ratione quae eum similique voluptas fugiat neque
                                pariatur
                                iusto. Ullam
                                deserunt doloremque repellendus.</p>
                            <p class="small">12 jun 2020</p>
                        </div>
                        <!-- next button -->
                        <div class="col-1 text-left">
                            <i class="fas fa-arrow-circle-right fa-2x text-success"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- previous button -->
        <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev"></a>
        <!-- next button -->
        <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next"></a>
    </div>
</div>
<!------------------------------------------------------------------- review section end --------------------------------------------------->
<!------------------------------------------------------------------- information div end -------------------------------------------------->

<!---------------------------------------------------------------- product section start ---------------------------------------------------->
<!-- recently viewed cards start -->
<div class="container-fluid">
    <div class="container pt-5">
        <!-- First Row [Prosucts]-->
        <h2 class="font-weight-bold pb-4">Recently Viewed</h2>
        <div class="row pb-5 mb-4">
            <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                <!-- Card-->
                <div class="card rounded shadow-sm border-0">
                    <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                            class="img-fluid d-block mx-auto mb-3">
                        <h5> <a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                        <h5 class="text-muted">350.00 EGP</h5>
                        <ul class="list-inline small">
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                <!-- Card-->
                <div class="card rounded shadow-sm border-0">
                    <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                            class="img-fluid d-block mx-auto mb-3">
                        <h5> <a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                        <h5 class="text-muted">350.00 EGP</h5>
                        <ul class="list-inline small">
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                <!-- Card-->
                <div class="card rounded shadow-sm border-0">
                    <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                            class="img-fluid d-block mx-auto mb-3">
                        <h5> <a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                        <h5 class="text-muted">350.00 EGP</h5>
                        <ul class="list-inline small">
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                <!-- Card-->
                <div class="card rounded shadow-sm border-0">
                    <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                            class="img-fluid d-block mx-auto mb-3">
                        <h5> <a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                        <h5 class="text-muted">350.00 EGP</h5>
                        <ul class="list-inline small">
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
            <button type="button" class="btn btn-outline-secondary btn-md btn-block cards-menu-button-2">
                <i class="fas fa-redo-alt mr-2"></i>Show more</button>
        </div>
    </div>
</div>
<!-- recently viewed cards end -->

<!-- related product cards start -->
<div class="container-fluid">
    <div class="container">
        <!-- First Row [Prosucts]-->
        <h2 class="font-weight-bold pb-4">Related Products</h2>
        <div class="row pb-5 mb-4">
            <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                <!-- Card-->
                <div class="card rounded shadow-sm border-0">
                    <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                            class="img-fluid d-block mx-auto mb-3">
                        <h5><a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                        <h5 class="text-muted">350.00 EGP</h5>
                        <ul class="list-inline small">
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                <!-- Card-->
                <div class="card rounded shadow-sm border-0">
                    <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                            class="img-fluid d-block mx-auto mb-3">
                        <h5><a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                        <h5 class="text-muted">350.00 EGP</h5>
                        <ul class="list-inline small">
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                <!-- Card-->
                <div class="card rounded shadow-sm border-0">
                    <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                            class="img-fluid d-block mx-auto mb-3">
                        <h5> <a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                        <h5 class="text-muted">350.00 EGP</h5>
                        <ul class="list-inline small">
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-3 col-sm-6 mb-4 mb-lg-0 pb-3">
                <!-- Card-->
                <div class="card rounded shadow-sm border-0">
                    <div class="card-body p-4"><img src="{{asset('./img/n-seires-500x500.jpg')}}" alt=""
                            class="img-fluid d-block mx-auto mb-3">
                        <h5> <a href="#" class="text-dark">CL 10000 delta UPS</a></h5>
                        <h5 class="text-muted">350.00 EGP</h5>
                        <ul class="list-inline small">
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="far fa-star text-success"></i></li>
                        </ul>
                    </div>
                </div>
            </div>
            <button type="button" class="btn btn-outline-secondary btn-md btn-block cards-menu-button-2">
                <i class="fas fa-redo-alt mr-2"></i>Show more</button>
        </div>
    </div>
</div>
<!-- related product cards end -->
<!--------------------------------------------------------------------- product section end ------------------------------------------------->
@endsection
