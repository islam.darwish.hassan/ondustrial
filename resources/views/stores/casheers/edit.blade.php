@extends('layouts.app')
@section('title', 'Edit ' . ucfirst(\Illuminate\Support\Str::singular($casheer->name)))


@section('back')
<a href="{{ route($name. '.index', $store->id) }}" class=" text-gold ">{{ ucfirst($name) }}</a> /
@endsection

@section('content')


<form method="post" action="{{ route($name. '.update', [$store->id,$casheer->id]) }}" enctype="multipart/form-data">
               @csrf

    @method('put')

    <div class="row">
        <div class="col form-group">
            <label class="form-label">Name</label>
            <input type="text" name="name" class="form-control" placeholder="Enter Name here.."
             value="{{ $casheer->name}}">
        </div>
        <div class="col-1 form-group ">
            <label class="form-label">Current </label>
        <img class=" pp-square " src="{{ $casheer->avatar }}" alt="avatar" >
        </div>
        <div class="col form-group">
            <label class="form-label">Avatar</label>
            <div class="custom-file">
                <label class="custom-file-label" for="validatedCustomFile">Replace avatar...</label>
                <input type="file" name="avatar" id="custom-file-input-image" class="custom-file-input">
            </div>
        </div>
        <div class="col form-group">
                <label class="form-label">Phone</label>
                <input type="tel" name="phone" class="form-control " placeholder="Phone" id="phone"
                    value="{{ $casheer->phone}}">
            </div>
    </div>
    </div>
    <div class="row">
        <div class="col form-group">
            <label class="form-label">Bio</label>
            <textarea type="text" row="5" name="bio" class="form-control" placeholder="Enter Bio here.."
                value="{{ $casheer->bio}}">{{ $casheer->bio}}</textarea>
        </div>
        <div class="col form-group">
            <label class="form-label">Address</label>
            <textarea type="text" row="5" name="address" class="form-control" placeholder="Enter Address here.."
                value="{{ $casheer->address}}">{{ $casheer->address}}</textarea>
        </div>

    </div>
    <br>
    <button type="submit" class="btn btn-block btn-success bg-brandgreen ">Update
        {{ ucfirst(\Illuminate\Support\Str::singular($name)) }}</button>

</form>
<script>
    $('.custom-file-input').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
</script>

@endsection
