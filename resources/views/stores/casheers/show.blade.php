@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col mx-2">
        <div class="row align-content-center">
            <img src="{{$casheer->avatar}}" class="pp-big-rounded  ">
            <div class="col">
            <b > {{$casheer->name}} </b>
            <div><b class="small ">{{$casheer->store->name}}</b></div>
            </div>
        </div>
    </div>
</div>
<div class="card    my-2 ">
    <div class="card-header  d-flex justify-content-between align-items-center">
    <b>Basic Info</b> <i class="fas fa-cog"></i>
    </div>
    <div class="card-body col">
        
        <div class="form-control my-2" readonly ><b>Name: {{$casheer->name}}</b></div>
            <div class="form-control my-2" readonly><b>Address: {{$casheer->address}}</b></div>
                <div class="form-control my-2" readonly><b>Phone: {{$casheer->phone}}</b></div>
                    <div><textarea  class="form-control my-2" readonly>{{$casheer->bio}}</textarea></div>

    </div>
</div>

@endsection