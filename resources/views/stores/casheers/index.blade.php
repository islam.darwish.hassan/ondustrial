@extends('layouts.resource.index')
@section('title', ucfirst($name))
@section('create-btn')
<a href="{{ route($name . '.create',$store->id) }}" class=" clr-blue p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index',$store->id) }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block bg-blue mb-2">Filter</button>
            </div>
            <a href="{{ route($name. '.index',$store->id) }}" class="small-header-bold"> X</a>
        </div>
    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-body')

@section('table-header')
            <th scope="col">Avatar</th>
            <th scope="col">@sortablelink('name','Name')</th>
            <th scope="col">Bio</th>
            <th scope="col">Phone</th>
            <th scope="col">Address </th>
            <th scope="col">@sortablelink('created_at','Created At')</th>
            <th  scope="col" style="width: 10%">Operations</th>

@endsection
@section('table-body')
            @foreach ($casheers as $casheer)
                <tr>
                    <td><img src="{{ $casheer->avatar}}" class="pp-square"  ></td>
                    <td>{{ $casheer->name }}</td>
                    <td>{{ $casheer->bio }}</td>
                    <td>{{ $casheer->phone }}</td>
                    <td>{{ $casheer->address }}</td>
                    <td>{{Carbon\Carbon::parse($casheer->created_at)->diffForHumans()}}
                    <td class="d-flex justify-content-center"><a
                        href="{{ route($name . '.show',[$store->id,$casheer->id]) }}" class="btn clr-black  "><i class="fas fa-eye"></i></a>
                       <a href="{{ route($name.'.edit',  [$store->id, $casheer->id]) }}"
                         class="btn clr-black "><i class="fas fa-edit"></i></a>
                           <form method="POST" action="{{ route($name.'.destroy',  [$store->id, $casheer->id]) }}">
                              @csrf
                               {{ method_field('DELETE') }}
                               <button type="submit"
                                onclick="return confirm('Are you sure you want to delete this casheer?')"
                                data-toggle="modal" data-target="#exampleModal"
                           class="btn  clr-blue  bg-transparent"><i class="fas fa-trash-alt "></i></button>
                           </form>
                       </td>
                    </tr>
            @endforeach
 @endsection
    {{ $data->appends(request()->query())->links() }}
@section('table-footer')
<p class="table-footer">Results :  {{$data->total()}}</p>

@endsection
@endsection
