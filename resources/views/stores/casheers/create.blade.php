@extends('layouts.app')
@section('title', 'Create ' . ucfirst(\Illuminate\Support\Str::singular($name)) )


@section('back')
<a href="{{ route($name. '.index', $store->id) }}" class=" text-gold ">{{ ucfirst($name) }}</a> /
@endsection

@section('content')


<form method="post" action="{{ route($name. '.store', $store->id) }}" enctype="multipart/form-data">

               @csrf

    <div class="row">
            <div class="col form-group">
                <label class="form-label">E-mail</label>
                <input type="email" name="email" class="form-control" placeholder="E-mail" value="{{ old('email') }}">
            </div>
            <div class=" col form-group">
                <label class="form-label">Password</label>
                <input type="password" name="password" class="form-control" placeholder="Password"
                    value="{{ old('password') }}" becrypt>
            </div>
    </div>
    <div class="row">
        <div class="col form-group">
            <label class="form-label">Name</label>
            <input type="text" name="name" class="form-control" placeholder="Enter Name here.."
             value="{{ old('name') }}">
        </div>
            <div class=" col form-group">
                <label class="form-label">Avatar</label>
                <div class="custom-file">
                    <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                    <input type="file" name="image" id="custom-file-input-image" class="custom-file-input">
                </div>
            </div>
            <div class="col form-group">
                <label class="form-label">Phone</label>
                <input type="tel" name="phone" class="form-control " placeholder="Phone" id="phone"
                    value="{{ old('phone') }}">
            </div>
    </div>
    </div>
    <div class="row">
        <div class="col form-group">
            <label class="form-label">Bio</label>
            <textarea type="text" row="5" name="bio" class="form-control" placeholder="Enter Bio here.."
                value="{{ old('bio')}}">{{ old('bio')}}</textarea>
        </div>
        <div class="col form-group">
            <label class="form-label">Address</label>
            <textarea type="text" row="5" name="address" class="form-control" placeholder="Enter Address here.."
                value="{{ old('address')}}">{{ old('address')}}</textarea>
        </div>

    </div>
    <br>
    <button type="submit" class="btn btn-block btn-success bg-brandgreen ">Create
        {{ ucfirst(\Illuminate\Support\Str::singular($name)) }}</button>

</form>
<script>
    $('.custom-file-input').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
</script>
<script>
    $('.custom-file-input').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
</script>

@endsection
