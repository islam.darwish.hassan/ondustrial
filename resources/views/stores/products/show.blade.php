@extends('layouts.app')
@section('title',ucfirst($product->name))


@section('content')

@if($product->status!=2 && $product->status!=1)
<div class="d-flex align-items-between ">
<a href="{{ route('products' . '.edit',[$store->id,$product->id]) }}" class=" clr-blue p-2 "><b>Edit Product </b><i class="fas fa-cog">
</i></a>
<a href="{{ route('specs' . '.index',[$store->id,$product->id]) }}" class=" clr-blue p-2 "><b>Manage Specs </b><i class="fas fa-cog">
</i></a>
@if(isset($product->offer))
<form method="POST" action="{{ route('offers.'.'destroy',[$store->id,$product->id,$product->offer->id]) }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
    <button type="submit"
    onclick="return confirm('Are you sure you want to remove the offer there is no back for this process ?')"
    data-toggle="modal" data-target="#Modal"
class="btn bg-red text-white  float-right  mt-2 mr-2 btn-sm">Remove Offer</button>
</form>

@else
<a href="{{ route('offers' . '.create',[$store->id,$product->id]) }}" class=" clr-blue p-2 "><b>Add Offer </b><i class="fas fa-cog">

@endif
</i></a>

@elseif($product->status==1)
<form method="POST" action="{{ route('products.'.'to_offline',[$store->id,$product->id]) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <button type="submit"
    onclick="return confirm('Are you sure you want to offline this product there is no back for this process ?')"
    data-toggle="modal" data-target="#Modal"
class="btn bg-grey text-black  float-right mb-2 mr-2 text-uppercase btn-sm">Make product offline</button>
</form>
@endif

</div>
<div class= "row shadow-sm mx-1 mb-3">
    <div class="col border-right">
        <img src={{$product->image}} class="pp-product " alt={{$store->name}}{{" - "}}{{$product->name}}>
        @if($product->status!=2 && $product->status!=1)
        @if($product->image)
        <div class="col-12 mx-auto">
          <div>
            <label class="btn btn-default clr-blue d-flex justify-content-end   align-items-center">
              <b class=pr-1>Edit Product Picture </b><i class=" fas fa-cog"></i>
              <form method="POST" action="{{ route('products.update_image', [$store->id,$product->id]) }}"  id="upload_image" enctype="multipart/form-data">
                           @csrf

                @method('PUT')
              <input type="file" class="hide" name="image" id="image">
            </form>

            </label>
          </div>

        </div>
        @endif
        @endif
        </div>
    <div class="col pt-5">
        <td>
            @if(isset($product->offer))
            <h3 class="mb-0">{{$product->price}} EGP</h3>
        Has offer of <b>{{$product->offer->offer}}% </b> to be <h3><b>{{$product->price- (($product->price*$product->offer->offer)/100)}} EGP </b></h3>
            @else
            <h3>{{$product->price}} EGP</h3>

            @endif
            @for($j = 0; $j < $store->rate; $j++)
                <span class="fa fa-star checked"></span>
            @endfor
            @for($j = 0; $j < (5-$store->rate); $j++)
                <span class="fa fa-star" style="color:darkcyan;"></span>
            @endfor
        </td>
        @if ($product->status == 1)
        <span class="badge badge-success text-white">Online</span>
        @elseif ($product->status == 2)
        <span class="badge badge-primary text-black">InReview</span>
        @elseif ($product->status == 3)
        <span class="badge badge-danger text-white">Rejected</span>
        @elseif ($product->status == 4)
        <span class="badge bg-grey  ">Offline</span>
        @elseif ($product->status == 5)
        <span class="badge bg-purple text-white">Draft</span>
        @else
        <span class="badge bg-grey text-black">Not determined</span>
        @endif
        <p>{!!$product->description !!}</p>
        <div class="row">
            <div class="col">
                <p class="text-bold"><u>Users Visits: </u></p>
                <p>{{$product->visited}} user views of this product  </p>
                <p class="text-bold"><u>Products Orders: </u></p>
                <p>{{$product->ordered}} items included in users orders </p>
            </div>
        <div class="col">
                <p class="text-bold"><u>In Stock: </u></p>
                <p>{{$product->stock}} items avaliable in stock </p>
                <p class="text-bold"><u>Reviews & Ratings: </u></p>
                <p>{{$product->reviews->count()}} reviews & users ratings </p>

    </div>
</div>
@if($product->specs->count()>0)
    <h4 class="pb-2">Specifications:</h4>
    <div class="row">
    @foreach($product->specs as $spec)
        <div class="col-3">
            <p class="text-bold"><u>{{$spec->title}} </u></p>
            <p>{{$spec->value}} </p>
        </div>
    @endforeach
    </div>
@endif

@if($product->status!=1&&$product->status!=2)
<form method="POST" action="{{ route('products.'.'to_review',[$store->id,$product->id]) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <button type="submit"
    onclick="return confirm('Are you sure you want to confirm this product there is no back for this process ?')"
    data-toggle="modal" data-target="#Modal"
class="btn bg-purple text-white  float-right mb-2 mr-2 text-uppercase">Submit for review</button>
</form>
@endif
@if($product->status==2)

@can('view-super',Auth::user())
<form method="POST" action="{{ route('products.'.'to_approve',[$store->id,$product->id]) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <button type="submit"
    onclick="return confirm('Are you sure you want to confirm this product there is no back for this process ?')"
    data-toggle="modal" data-target="#Modal"
class="btn bg-purple text-white  float-right mb-2 mr-2 text-uppercase">Approve</button>
</form>
<form method="POST" action="{{ route('products.'.'to_reject',[$store->id,$product->id]) }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <button type="submit"
    onclick="return confirm('Are you sure you want to confirm this product there is no back for this process ?')"
    data-toggle="modal" data-target="#Modal"
class="btn bg-red text-white  float-right mb-2 mr-2 text-uppercase">Reject</button>
</form>

<a  class="btn  disabled  btn-block text-uppercase">Waiting for Admin Approval</a>

@endcan
@endif
</div>


    </div>
</div>
<div class="row d-flex justify-content-center mb-3">
    @if($product->status!=2 && $product->status!=1)
    <div class="col-12 mx-auto  ">
      <div>
        <label class="btn btn-default clr-blue d-flex justify-content-end  align-items-center">
          <b class=pr-1>Add Product Picture </b><i class=" fas fa-cog"></i>
          <form method="POST" action="{{ route('products.add_image', [$store->id,$product->id]) }}"  id="add_image" enctype="multipart/form-data">
                       @csrf

            @method('POST')
          <input type="file" class="hide" name="added_image" id="added_image">
        </form>
        </label>
      </div>
    </div>
    @endif
    @foreach ($product_images as $item)
<div class="col-2">
<img src={{$item->image}} alt={{$item->image}} class="img-thumbnail" style="max-width:200px; height:200px; object-fit: cover;"/>
<form method="POST" action="{{ route('products.remove_image',[$store->id,$product->id,$item->id]) }}">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
    <button type="submit"
     onclick="return confirm('Are you sure you want to delete this product image?')"
     data-toggle="modal" data-target="#exampleModal"
class="btn  clr-blue  bg-transparent"><i class="fas fa-trash-alt "></i>
Delete Image
</button>
</div>
</form>
    @endforeach

</div>
<div class="row justify-content-center d-flex">
    @if($product->reviews->count()>0)
    <div class="col">
      <div class="card border-white">
        <div class="card-header bg-blue text-white"><b>Product Users Reviews</b>
        </div>
        <table class="table  table-bordered  table-hover table-sm text-center  table-center ">
          <thead class="thead-light  ">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Client Name</th>
              <th scope="col">Review </th>
              <th scope="col">Rate</th>
              <th scope="col">Created at </th>
              </tr>
          </thead>
          <tbody>
            @foreach($product->reviews as $review)
            <!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->


            <td>{{ $review->id }}</td>
            <td>{{ $review->client->name }} </td>
            <td>{{ $review->review }} </td>
            <td>{{ $review->rate }} </td>
            <td>{{Carbon\Carbon::parse($review->created_at)->diffForHumans()}}</td>

           </tr>
         @endforeach
          </tbody>
        </table>
      </div>
    <a href="{{route('reviews.index',$product->id)}}" class="clr-blue text-bold float-right mb-2" >See more -></a>
    </div>
    @endif

</div>
<script type="text/javascript">
    document.getElementById("upload_image").onchange = function() {
        // submitting the form
        document.getElementById("upload_image").submit();
    };
  </script>
<script type="text/javascript">
    document.getElementById("add_image").onchange = function() {
        // submitting the form
        document.getElementById("add_image").submit();
    };
  </script>

<script>
    $('.custom-file-input').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
</script>

@endsection
