@extends('layouts.resource.index')
@section('title', ucfirst($name))
@section('create-btn')
<a href="{{ route('products' . '.create',$store->id) }}" class=" clr-blue p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index',$store->id) }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'Search...']])
            </div>
            <div class="p-2 row flex-fill">
                <div class=" col form-group">
                    <select name="status" class="custom-select">
                        <option value="0"  @if(request()->query('status') == '0') selected @endif>Status...</option>
                        <option value="1"  @if(request()->query('status') == '1') selected @endif>Online</option>
                        <option value="2"  @if(request()->query('status') == '2') selected @endif>InReview</option>
                        <option value="3" @if(request()->query('status') == '3') selected @endif>Rejected</option>
                        <option value="4" @if(request()->query('status') == '4') selected @endif>Offline</option>
                        <option value="5" @if(request()->query('status') == '5') selected @endif>Draft</option>

                    </select>
                </div>
                <div class="col form-group ">
                <select class="custom-select" name="sub_category">
                    <option value="none" @if(request()->query('sub_category') == 'none') selected @endif>Sub Category...</option>
                    @foreach ($subCategories as $sub_category)
                    <option value={{$sub_category->id}}
                 @if(request()->query('sub_category') == $sub_category->id) selected @endif>{{$sub_category->name}} </option>
                    @endforeach
                </select>
                </div>

            </div>

            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block bg-blue mb-2">Filter</button>
            </div>
            <a href="{{ route($name. '.index',$store->id) }}" class="small-header-bold"> X</a>
        </div>
    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-body')

@section('table-header')

<th scope="col">@sortablelink('id', '#')</th>
<th scope="col">Image</th>
<th scope="col">@sortablelink('name','Product')</th>
<th scope="col">@sortablelink('sub_catogrey_id','SubCategory')</th>
<th scope="col">@sortablelink('price','Price')</th>
{{-- <th scope="col">Visits</th> --}}
<th scope="col">@sortablelink('ordered','Ordered')</th>
<th scope="col">@sortablelink('stock','In Stock')</th>
<th scope="col">@sortablelink('status','Status')</th>
<th scope="col">@sortablelink('rate','Rate')</th>
<th scope="col">@sortablelink('created_at','Created at') </th>
<th scope="col">Operations </th>
@endsection
@section('table-body')
@foreach($products as $product)
<!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->


<td>{{ $product->id }}</td>
<td><img src={{$product->image}} class="pp-square"></td>
<td>{{ Str::limit($product->name ,70) }} </td>
<td>{{ $product->sub_category->name }}  </td>
<td>{{ $product->price }} EGP </td>
{{-- <td>{{ $product->visited }} </td> --}}
<td>{{ $product->ordered }} </td>
<td>{{ $product->stock }} </td>

<td>
    @if ($product->status == 1)
    <span class="badge badge-success text-white">Online</span>
    @elseif ($product->status == 2)
    <span class="badge badge-primary text-black">InReview</span>
    @elseif ($product->status == 3)
    <span class="badge badge-danger text-white">Rejected</span>
    @elseif ($product->status == 4)
    <span class="badge bg-grey  ">Offline</span>
    @elseif ($product->status == 5)
    <span class="badge bg-purple text-white">Draft</span>
    @else
    <span class="badge bg-grey text-black">Not determined</span>
    @endif
</td>
<td>
    @for($j = 0; $j < $product->rate; $j++)
        <span class="fa fa-star checked"></span>
    @endfor
    @for($j = 0; $j < (5-$product->rate); $j++)
        <span class="fa fa-star" style="color:grey;"></span>
    @endfor
</td>
<td>{{Carbon\Carbon::parse($product->created_at)->diffForHumans()}}</td>

<!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->

<td class="d-flex justify-content-center"><a href="{{ route('products.show',[$store->id, $product->id]) }}"
    class="btn clr-black  "><i class="fas fa-eye"></i></a>
    @if($product->status!=2 && $product->status!=1)
   <a href="{{ route('products.edit', [$store->id, $product->id]) }}"
     class="btn clr-black "><i class="fas fa-edit"></i></a>
     @endif
       <form method="POST" action="{{ route('products.'.'destroy',[$store->id, $product->id]) }}">
           {{ csrf_field() }}
           {{ method_field('DELETE') }}
           <button type="submit"
            onclick="return confirm('Are you sure you want to delete this product?')"
            data-toggle="modal" data-target="#exampleModal"
       class="btn  clr-blue  bg-transparent"><i class="fas fa-trash-alt "></i></button>
       </form>
   </td>
</tr>
@endforeach
@endsection
    {{ $data->appends(request()->query())->links() }}
@section('table-footer')
<p class="table-footer">Results :  {{$data->total()}}</p>

@endsection
@endsection
