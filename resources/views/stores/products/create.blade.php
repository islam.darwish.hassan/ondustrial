@extends('layouts.app')
@section('title', 'Create New Product')



@section('content')


<form method="post" action="{{ route($name. '.store', $store->id) }}" enctype="multipart/form-data">

               @csrf

    <div class="row">
        <div class="col form-group">
            <label class="form-label">Product Name <b style="color:red"> * </b></label class="form-label">
            <input type="text" name="name" class="form-control" placeholder="Name"
                value="{{ old('name') }}">
        </div>
        <div class="col form-group">
            <label class="form-label">Image 1:1 *min 512 *max 2048<b style="color:red"> * </b></label class="form-label">
            <div class="custom-file">
                <label class="custom-file-label" for="validatedCustomFile">Choose file...</label class="form-label">
                <input type="file" name="image" id="custom-file-input-image" class="custom-file-input" >
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col form-group">
            <label class="form-label">Category <b style="color:red"> * </b></label class="form-label">
            <select name="category" class="cat custom-select" id="category" data-dependent="sub_category">
                <option>Choose...</option>
                @foreach ($categories as $category)
                <option value="{{ $category->id }}"
                    @if(request()->query('category') == $category->id ||old('category')== $category->id) selected @endif
                    >{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col form-group">
            <label class="form-label">Sub category <b style="color:red"> * </b></label class="form-label"><br>
            <select name="sub_category" class="sub custom-select" id="sub_category">
                <option>Choose...</option>
                @foreach ($subs as $sub)
                <option value="{{ $sub->id }}"
                    @if(request()->query('sub_category') == $sub->id ||old('sub_category')== $sub->id) selected @endif
                    >{{ $sub->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col form-group">
            <label class="form-label">In Stock <b style="color:red"> * </b></label class="form-label">
            <input type="number" name="in_stock" class="form-control" placeholder="Stock"
                value="{{ old('in_stock') }}">
        </div>
        <div class="col form-group">
            <label class="form-label">Handling days <b style="color:red"> * </b></label class="form-label">
            <input type="number" name="handling_days" class="form-control" placeholder="Handling in Days"
                value="{{ old('handling_days') }}">
        </div>

        <div class="col form-group">
            <label class="form-label">Price <b style="color:red"> * </b></label class="form-label">
            <input type="number" name="price" class="form-control" placeholder="Stock"
                value="{{ old('price') }}">
        </div>

    </div>
    <div class="row">
        <div class="col form-group">
            <label class="form-label">Description <b style="color:red"> * </b></label class="form-label">
            <textarea type="text" row="8" name="description" class="form-control" placeholder="Body"
             value="{{ old('description')}}">{{ old('description')}}</textarea>
        </div>
    </div>
    <div class="row">
        <div class="col form-group">
            <label class="form-label">Brand Name </label class="form-label">
            <input type="text" name="brand" class="form-control" placeholder="Name"
                value="{{ old('brand') }}">
        </div>
        <div class="col form-group">
            <label class="form-label">Modal Number </label class="form-label">
            <input type="text" name="modal_number" class="form-control" placeholder="Modal "
                value="{{ old('modal_number') }}">
        </div>
        <div class="col form-group">
            <label class="form-label">EAN-13 </label class="form-label">
            <input type="number" name="ean_13" class="form-control" placeholder="Number"
                value="{{ old('ean') }}">
        </div>
        <div class="col form-group">
            <label class="form-label">External Product ID </label class="form-label">
            <input type="number" name="external_product_id" class="form-control" placeholder="Number"
                value="{{ old('external_product_id') }}">
        </div>

    </div>
    <div class="row">
        <div class="col form-group">
            <label class="form-label">Package Thickness </label class="form-label">
            <input type="number" name="package_thickness" class="form-control" placeholder="Number"
                value="{{ old('package_thickness') }}">
        </div>

        <div class="col form-group">
            <label class="form-label">Package Height </label class="form-label">
            <input type="number" name="package_height" class="form-control" placeholder="In cm"
                value="{{ old('package_height') }}">
        </div>
        <div class="col form-group">
            <label class="form-label">Package Width </label class="form-label">
            <input type="number" name="package_width" class="form-control" placeholder="In cm"
                value="{{ old('package_width') }}">
        </div>
        <div class="col form-group">
            <label class="form-label">Package Weight </label class="form-label">
            <input type="number" name="package_weight" class="form-control" placeholder="In Grams"
                value="{{ old('package_weight') }}">
        </div>

    </div>
    <br>
    <button type="submit" class="btn btn-block btn-success bg-brandgreen ">Create
        {{ ucfirst(\Illuminate\Support\Str::singular($name)) }}</button>

</form>
<script>
    $('.custom-file-input').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
</script>
<script>
    $(document).ready(function () {
        //to get params from url
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        var type = $.urlParam('type');
        if (type) {
            console.log(type);
            $(".sel").val(type);
            $(".form" + type).show();
        } else {
            console.log('no type')
        }
    });

</script>
<script>
    $('.sub').prop('disabled', false);
    $('.cat').change(function () {
       // $('.sub').prop('disabled', false);
        if ($(this).val() != '') {
            var select = $(this).attr("id");
            var value = $(this).val();
            var dependent = $(this).data('dependent');
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('category.fetch') }}",
                method: "POST",
                data: {
                    select: select,
                    value: value,
                    dependent: dependent,
                    _token: _token
                },
                success: function (result) {
                    $('#' + dependent).html(result);
                }
            })
        }
    });

    $('#category').change(function () {
        $('#sub_category').val('');
    });
</script>

@endsection
