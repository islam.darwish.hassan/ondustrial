@extends('layouts.app')
@section('title', 'tip of ' .$tip->client->first_name." ".$tip->client->last_name)
@section('content')
<div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-2 ">
          <div class="card bg-blue mb-2  flex-fill h-100 ">
            <div class="card-body row text-center">
              <div class="col">
                <div class="text-uppercase text-white small">Caption</div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-10  align-self-stretch">
          <div class="card bg-white mb-2  h-100">
            <div class="card-body row text-center">
              <div class="">
                <b class="mx-2">
                  {{$tip->caption}}
                </b>
              </div>
            </div>
          </div>
        </div>
      </div>
  <div class=" d-flex justify-content-end pb-5">
    <div>
     <div class="badge"><b>{{$upvotes->count()}} Up Votes </b></div>
    <div class="badge"><b>{{$downvotes->count()}} Down Votes</b></div>
    </div>
  </div>
</div>
<!-- Start of parameters-->
<hr>
</div>
<div class="card-header bg-blue clr-white  d-flex justify-content-start align-items-center">
  <b>Ratings</b>
</div>
<div style="row px-5 ">
  <div class="mt-2 px-3">
    <b>This is User Rate For This Store</b>
  </div>
  <div class="col ">
    <div class="progress mt-2">
      <div class="progress-bar progress-bar-striped progress-bar-animated
          {{$tip->rate<=3 ? 'bg-blue' : null}}
          {{$tip->rate>=5 && $tip->rate<7 ? 'bg-yellow': null}}
          {{$tip->rate>=7 ?'bg-green': null}}" role="progressbar" aria-valuenow="75" aria-valuemin="0"
        aria-valuemax="100" style="width: {{$tip->rate/10 *100 .'%'}}">{{$tip->rate}}
      </div>
    </div>
  </div>
</div>
<hr>

  @endsection