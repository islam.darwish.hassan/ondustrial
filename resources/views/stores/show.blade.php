@extends('layouts.app')

@section('content')
<!--Info-->

  <div class="card mb-3  shadow-sm " style="margin-right: -1vh" >
  <div class="row no-gutters storeCardContainer">
    <div class="col-md-2 d-flex align-content-center justify-content-center border-right ">
      <img src={{$store->avatar}} class="pp-store " alt={{$store->name}}>
    </div>
    <div class="col-md-8 border-right ">
      <div class="card-body ">
        <h5 class="card-title mb-0 text-bold">{{$store->name}}</h5>

        {{-- <p class="card-text mb-0"><small class="text-muted bold">{{$store->sub_category->name}}</small></p> --}}
        <p class="card-text mb-0 text-muted">{{isset($store->bio)?$store->phone:"No available bio entered"}}</p>
      </div>
    </div>
    <div class="col-md-2 border-right">
      <div class="card-header py-0 d-flex justify-content-between pr-0 align-items-center">
        <b>Contact Info</b>
        <a href="{{ route('stores'.'.edit', $store->id) }}"  class="btn clr-blue  "><i class=" fas fa-cog"></i></a>
      </div>
      <div class="card-body">
        <p class="card-text mb-0"><small class="text-muted"><i class="fas fa-phone-square"></i> {{isset($store->phone)?$store->phone:"Not Available"}}</small></p>
      </div>
    </div>

  </div>
</div>

<!--end of info -->
{{-- <div class="row">
    @if($store->header_image)
    <div class="col-10 mx-auto">
      <img src="{{asset('files')."/stores/images/".$store->header_image }} " class="storecover mx-auto d-block " alt={{$store->name}} >
      <div>
        <label class="btn btn-default clr-blue d-flex justify-content-end   align-items-center">
          <b class=pr-1>Edit Cover Picture </b><i class=" fas fa-cog"></i>
          <form method="POST" action="{{ route($name. '.update_cover', $store->id) }}"  id="upload_cover" enctype="multipart/form-data">
                       @csrf


            @method('PUT')
          <input type="file" class="hide" name="headerImage" id="headerImage">
        </form>

        </label>
      </div>

    </div>
    @else

      <div>
      <label class="btn btn-default clr-blue d-flex justify-content-end   align-items-center">
        <b class=pr-1>Upload Cover Picture </b><i class=" fas fa-cog"></i>
        <form method="POST" action="{{ route($name. '.update_cover', $store->id) }}"  id="upload_cover" enctype="multipart/form-data">
                     @csrf


          @method('PUT')
        <input type="file" class="hide" name="headerImage" id="headerImage">
      </form>

      </label>
    </div>
    @endif
  </div>
</div> --}}
<!--Statistics-->
<div class="row">
  <div class="col">
    <div class="card bg-blue mb-2  flex-fill ">
      <div class="card-header  d-flex justify-content-center">
        <i class="fas fa-star-half-alt text-white my-3 display-4"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <div class="text-vlarge text-white">{{$store->rate}}</div>
          <div class="text-uppercase text-white small text-bold">Stars</div>
        </div>
      </div>
    </div>
  </div>
  <div class="col ">
    <div class="card bg-blue mb-2 flex-fill ">
      <div class="card-header  d-flex justify-content-center">
        <i class="fas fa-pencil-alt text-white my-3 display-4"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <div class="text-vlarge text-white">0</div>
          <div class="text-uppercase text-white small text-bold">Reviews</div>
        </div>
      </div>
    </div>
  </div>
  <div class="col" onclick="window.location='{{ route('stores' . '.show' ,$store->id) }}'">
    <div class="card bg-blue mb-2 flex-fill ">
      <div class="card-header  d-flex justify-content-center">
        <i class="fas fa-box-open text-white my-3 display-4"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <div class="text-vlarge text-white">{{$store->products->count()}}</div>
          <div class="text-uppercase text-white small text-bold">Products</div>
        </div>
      </div>
    </div>
  </div>
  <div class="col" onclick="window.location='{{ route('stores' . '.show' ,$store->id) }}'">
    <div class="card bg-blue mb-2 flex-fill ">
      <div class="card-header  d-flex justify-content-center">
        <i class="fas fa-chart-line text-white my-3 display-4"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <div class="text-vlarge text-white">0</div>
          <div class="text-uppercase text-white small text-bold">Sales</div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--end of Statistics-->
<!--extra Info-->
<div class="row justify-content-center d-flex">
    @if($products->count()>0)
    <div class="col">
      <div class="card border-white">
        <div class="card-header bg-blue text-white"><b>Products</b>
            <a href="{{ route('products' . '.create' ,[$store->id]) }}" class=" clr-blue p-2 "><i class="fas fa-plus"></i></a>
        </div>
        <table class="table  table-bordered  table-hover table-sm text-center  table-center ">
          <thead class="thead-light  ">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Image</th>
              <th scope="col">Product</th>
              <th scope="col">Price</th>
              <th scope="col">Visits</th>
              <th scope="col">Ordered</th>
              <th scope="col">In Stock</th>
              <th scope="col">Status</th>
              <th scope="col">Rate</th>
              <th scope="col">Created at </th>
              <th scope="col">Operations </th>
              </tr>
          </thead>
          <tbody>
            @foreach($products as $product)
            <!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->


            <td>{{ $product->id }}</td>
            <td><img src={{$product->image}} class="pp-square"></td>
            <td>{{ $product->name }} </td>
            <td>{{ $product->price }} EGP </td>
            <td>{{ $product->visited }} </td>
            <td>{{ $product->ordered }} </td>
            <td>{{ $product->stock }} </td>

            <td>
                @if ($product->status == 1)
                <span class="badge badge-success text-white">Online</span>
                @elseif ($product->status == 2)
                <span class="badge badge-primary text-black">InReview</span>
                @elseif ($product->status == 3)
                <span class="badge badge-danger text-white">Rejected</span>
                @elseif ($product->status == 4)
                <span class="badge bg-grey  ">Offline</span>
                @elseif ($product->status == 5)
                <span class="badge bg-purple text-white">Draft</span>
                @else
                <span class="badge bg-grey text-black">Not determined</span>
                @endif
            </td>
            <td>
                @for($j = 0; $j < $product->rate; $j++)
                    <span class="fa fa-star checked"></span>
                @endfor
                @for($j = 0; $j < (5-$product->rate); $j++)
                    <span class="fa fa-star" style="color:grey;"></span>
                @endfor
            </td>
                        <td>{{Carbon\Carbon::parse($product->created_at)->diffForHumans()}}</td>

            <!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->

            <td class="d-flex justify-content-center"><a href="{{ route('products.show',[$store->id, $product->id]) }}"
                class="btn clr-black  "><i class="fas fa-eye"></i></a>
                @if($product->status!=2 && $product->status!=1)
                <a href="{{ route('products.edit', [$store->id, $product->id]) }}"
                  class="btn clr-black "><i class="fas fa-edit"></i></a>
                  @endif
                  <form method="POST" action="{{ route('products.'.'destroy',[$store->id, $product->id]) }}">
                      @csrf
                       {{ method_field('DELETE') }}
                       <button type="submit"
                        onclick="return confirm('Are you sure you want to delete this product?')"
                        data-toggle="modal" data-target="#exampleModal"
                   class="btn  clr-blue  bg-transparent"><i class="fas fa-trash-alt "></i></button>
                   </form>
               </td>
           </tr>
         @endforeach
          </tbody>
        </table>
      </div>
    <a href="{{route('products.index',$store->id)}}" class="clr-blue text-bold float-right mb-2" >See more -></a>
    </div>
    @else
    <div class="card-header bg-blue text-white flex-fill mx-3 py-3 "><b>Add New Products</b>
        <a href="{{ route('products' . '.create' ,[$store->id]) }}" class=" clr-blue p-2 "><i class="fas fa-plus"></i></a>
    </div>

    @endif



</div>



</div>
<script type="text/javascript">
  document.getElementById("upload_cover").onchange = function() {
      // submitting the form
      document.getElementById("upload_cover").submit();
  };
</script>
<!-- end of extra Info-->
@endsection
