@extends('layouts.app')
@section('title', 'Cards')

@section('content')
@foreach ($cards as $card)
<div class="row">
  @if($card->type==1)
  <div class="col-2">
    <div class="card bg-green mb-2  flex-fill">
      <div class="card-header  d-flex justify-content-center">
        <i class="fas fa-puzzle-piece text-white my-3 display-4"></i>
      </div>
      <div class="card-body row text-center">
        <div class="col">
          <div class="text-uppercase text-white small">Recommendation</div>
        </div>
      </div>
    </div>
  </div>
  @elseif($card->type==2)
    <div class="col-2">
      <div class="card bg-red mb-2  flex-fill">
        <div class="card-header  d-flex justify-content-center">
          <i class="fas fa-exclamation-triangle text-white my-3 display-4"></i>
        </div>
        <div class="card-body row text-center">
          <div class="col">
            <div class="text-uppercase text-white small">Complain</div>
          </div>
        </div>
      </div>
    </div>
  @endif

    <div class="col">
        <div class="card bg-white mb-2  flex-fill">
          <div class="card-header  d-flex justify-content-start align-items-center">
            <img src="{{ $card->client->avatar }}"  class="pp-circle mr-2 ">
            <b> {{$card->client->first_name}} {{$card->client->last_name}}</b>
          </div>
          <div class="card-body row text-center">
            <div class="col-2 border-right">
              <div class="text-vlarge clr-blue">{{$card->satisfaction_rate}}</div>
              <div class="text-uppercase clr-blue small">Satisfaction</div>
            </div>
            <div class="col-8">
                <b>
                    @foreach ($card->messages as $message)
                    @if ($loop->first)
                      @if($message->type==1)
                      {{Str::limit($message->message,250)}}
                      @elseif($message->type==2)
                      <div class="row d-flex align-content-center">
                      <div class="col-1">
                        <img src={{asset('files/cards/images/')."/".$message->message}} class="pp-big">
                      </div>
                      <div class="col-10 align-self-center">
                        Photo has been sent
                      </div>
                    </div>
                      @endif
                    @endif
                    @endforeach
                    @if ($card->messages->count()<=0)
                    There is no messages
                    @endif
                  </b>
              </div>
              <div class="col-2 border-left">
                <div class="text-vlarge clr-blue"><a href="{{ route('cards'.'.show',[ $store->id,$card->id]) }}"
                    class=" clr-blue  ">  <i class="fas fa-comment-dots fa-2x "></i></a> </div>
                <div class="text-uppercase clr-blue small">Replay</div>
              </div>
              <div class="col-6">
                </div>
  
          </div>
        </div>
      </div>
  
</div>
@endforeach

<p class="table-footer">Results :  {{$cards->total()}}</p>
{{ $cards->appends(\Request::except('page'))->render() }}
@if($cards->total()<=0)
<div class="row justify-content-center">
    <h5 class="empty-table-text"> There are no results  😴  </h5>
</div>
@endif

@endsection