@extends('layouts.app')
@section('title', 'Card of ' .$card->client->first_name." ".$card->client->last_name)
@section('content')
@if($messages->count()>0)
<div class="card-header bg-blue clr-white  d-flex justify-content-between align-items-center">
  <b>Messages</b>
</div>
<div class=" mt-2  d-flex flex-fill justify-content-end small" style="margin-bottom:-10px">
  {{ $messages->appends(\Request::except('page'))->render() }}
</div>
@foreach ($messages as $message)
<div style="row mx-5  ">
  <div class="card    my-2 ">
      @if($message->user==2 )
      <div class="card-header  d-flex justify-content-between align-items-center">
      <b class="clr-blue">{{$card->client->first_name}} {{$card->client->last_name}}</b>
      <b class="small">{{Carbon\Carbon::parse($message->created_at)->diffForHumans()}}</b>
      @elseif($message->user==1)
      @if(isset($client))
      <div class="card-header  d-flex justify-content-between align-items-center">
      <b>{{$card->store->name}}</b>
      <b class="small">{{Carbon\Carbon::parse($message->created_at)->diffForHumans()}}</b>
      @else
      <div class="card-header   d-flex justify-content-between align-items-center">
      <b>You</b>
      <b class="small">{{Carbon\Carbon::parse($message->created_at)->diffForHumans()}}</b>

      @endif
      @endif
    </div>
    <div class="card-body row text-center px-5 justify-content-center">
        @if($message->type==1)
        <b>{{$message->message}}</b>
        @elseif($message->type==2)
        <img src={{asset('files/cards/images/')."/".$message->message}} class="pp-vbig  ">
        @endif
    </div>
  </div>
  @endforeach
</div>
<div class="table-footer ">
  <p class="table-footer ">Results : {{$messages->total()}}</p>
</div>
<div class="row mb-5">
</div>
@if($messages->total()<=0) <div class="row justify-content-center ">
  <h5 class="empty-table-text"> There are no results 😴 </h5>
@endif
@endif

<form method="POST" action="{{ route('messages'. '.store',[$store->id,$card->id]) }}">
             @csrf

  @method('POST')
  <div class="row p-3 ">
    <div class="row flex-fill p-1">
      <div class="col">
        <textarea type="text" row="5" name="message" class="form-control mb-2 " placeholder="Add message here ..."
          value=""></textarea>
        <button type="submit" class="btn1 btn float-right bg-blue col-2  ">Submit</button>
      </div>
    </div>
</form>


<div class="col-md-2">
  <div class="card bg-blue mb-2  ">
    <div class="card-header  d-flex justify-content-center">
      <i class="fas fa-seedling text-white my-3 fa-3x"></i>
    </div>
    <div class="card-body row text-center">
      <div class="col">
        <form method="POST" action="{{ route('messages'. '.store',[$store->id,$card->id]) }}">
                     @csrf

          @method('POST')
          <div>
            <input type="hidden" name="message" class="form-control  " value="Thanks for your Card ">
            <button class=" btn text-uppercase bg-blue clr-black small">Say Thanks !</button>
          </div>

        </form>
      </div>

    </div>

  </div>
  {{-- <form method="POST" action="{{ route('messages'. '.store',[$store->id,$card->id]) }}" enctype="multipart/form-data">
               @csrf

    @method('POST')
    <div class="col form-group">
        <div class="custom-file">
            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
            <input type="file" name="image" class="custom-file-input">
        </div>
    </div>
    </div>
  </form> --}}

</div>
</div>
@endsection
