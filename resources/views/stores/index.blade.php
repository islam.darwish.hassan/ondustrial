@extends('layouts.resource.index')
@section('title', ucfirst('Parteneres'))

@section('create-btn')
<a href="{{ route('users' . '.create' ,["type"=>3]) }}" class=" clr-blue p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <select class="form-control form-inline" name="status">
                    <option value="none" @if(request()->query('status') == 'none') selected @endif>Status</option>
                    <option value="1"  @if(request()->query('status') == '1') selected @endif>Verfied</option>
                    <option value="2"  @if(request()->query('status') == '2') selected @endif>Not Verfied</option>
                    <option value="3"  @if(request()->query('status') == '3') selected @endif>Suspended</option>

                </select>
            </div>
            <div class="p-2 flex-fill ">
                <select class="form-control form-inline" name="sub_category">
                    <option value="none" @if(request()->query('sub_category') == 'none') selected @endif>Available Category</option>
                    @foreach ($subCategories as $sub_category)
                    <option value={{$sub_category->id}}
                 @if(request()->query('sub_category') == $sub_category->id) selected @endif>{{$sub_category->name}} </option>
                    @endforeach
                </select>
            </div>

            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block bg-blue mb-2">Filter</button>
            </div>
            <a href="{{ route($name. '.index') }}" class="small-header-bold"> X</a>

        </div>

    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-header')


    <th scope="col" style="width: 10%" >@sortablelink('id', '#')</th>
    <th scope="col">Img</th>
    <th scope="col">@sortablelink('name', 'Partener Name')</th>
    <th scope="col">Phone</th>
    <th scope="col">@sortablelink('rate', 'Rating')</th>
    <th scope="col">@sortablelink('products_count', 'Products')</th>
    <th scope="col">@sortablelink('status', 'Status')</th>
    <th  scope="col" style="width: 10%">Operations</th>

@endsection



@section('table-body')

    @foreach ($stores as $store)
        <tr>
            <td>{{ $store->id }}</td>
             <td><img src="{{ $store->avatar }}" alt="avatar" class="pp-square"></td>
            <td>{{ $store->name }}</td>
            <td>{{ isset($store->phone)?$store->phone:"Not Available" }}</td>
            <td>
                @for($j = 0; $j < $store->rate; $j++)
                    <span class="fa fa-star checked"></span>
                @endfor
                @for($j = 0; $j < (5-$store->rate); $j++)
                    <span class="fa fa-star" style="color:darkcyan;"></span>
                @endfor
            </td>
            <td>{{ $store->products_count }}</td>

            <td>
                @if ($store->status == 1)
                <span class="badge badge-success text-white">Verified</span>
                @elseif ($store->status == 2)
                <span class="badge badge-warning text-black">Not Verified</span>
                @elseif ($store->status == 3)
                <span class="badge badge-danger text-white">Suspended</span>
                @elseif ($store->status == 4)
                <span class="badge badge-primary text-white">Accepted</span>
                @else
                <span class="badge bg-grey text-black">Not determined</span>
                @endif
            </td>
            <td class="d-flex justify-content-center">
                @if(isset($store->id))
                <a href="{{ route('stores'.'.show', $store->id) }}"
                class="btn clr-black  "><i class="fas fa-eye"></i></a>
               <a href="{{ route('stores'.'.edit', $store->id) }}"
                 class="btn clr-black "><i class="fas fa-edit"></i></a>
                   <form method="POST" action="{{ route('users'.'.destroy', $store->user_id) }}">
                      @csrf
                       {{ method_field('DELETE') }}
                       <button type="submit"
                        onclick="return confirm('Are you sure you want to delete this store?')"
                        data-toggle="modal" data-target="#exampleModal"
                   class="btn  clr-blue  bg-transparent"><i class="fas fa-trash-alt "></i></button>
                   </form>
                @endif
               </td>
           </tr>
    @endforeach
    @section('table-footer')
        <p class="table-footer">Results :  {{$data->total()}}</p>
    @endsection

@endsection
