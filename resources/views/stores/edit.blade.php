@extends('layouts.resource.edit')


@section('form')

<form method="POST" action="{{ route($name. '.update', $store->id) }}" enctype="multipart/form-data">

               @csrf


    @method('PUT')

    <div class="form2">
        <div class=" mb-3">
        <h3 class="section-header ">Additional information: </h3>
        <p class="section-subheader">
            This is required additional information for creating new
            <code>Partner</code>
        </p>
        </div>
        </div>
            <div class=" flex-fill ">
                <div class="row">
                    <div class="col form-group">
                        <label class="form-label">Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Name"
                            value={{isset($store) ? $store->name:old('name')}}>
                    </div>
                    <div class="col form-group">
                        <label class="form-label">Avatar</label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="validatedCustomFile">Replace avatar...</label>
                            <input type="file" name="avatar" id="custom-file-input-image" class="custom-file-input">
                        </div>
                    </div>
                    <div class="col-1 form-group ">
                        <label class="form-label">Current </label>
                    <img class=" pp-square " src="{{ $store->avatar }}" alt="avatar" >
                    </div>
                </div>

                <div class="row">
                    <div class=" col form-group">
                        <label class="form-label">BIO</label>
                        <textarea type="text" row="5" name="bio" class="form-control" placeholder="BIO"
                       value= {{isset($store) ? $store->bio:old('bio')}}>{{isset($store) ? $store->bio:old('bio')}}</textarea>
                    </div>
                </div>

        </div>
        <div class="row">
            @can('view-super',Auth::user())
            <div class="col form-group">
                <label class="form-label">Status</label>
                <select name="status" class="custom-select">
                    <option value="0" {{isset($store) ? $store->status == 'none' ? 'selected' :"":""}} >Choose...</option>
                    <option value="1" {{isset($store) ? $store->status == '1' ? 'selected' :"":""}}>Verified</option>
                    <option value="2" {{isset($store) ? $store->status == '2' ? 'selected' :"":""}}>Not verified</option>
                    <option value="3" {{isset($store) ? $store->status == '3' ? 'selected' :"":""}}>Suspended</option>

                </select>
            </div>
            @endcan

        </div>

        <fieldset class="gllpLatlonPicker d-flex ">
            <div class="row">
                <div class="col-6 form-group ">
                    <label class="form-label">Location </label>
                    <div class="gllpMap" style="width:100% width: 500px; height: 250px; ">Google Maps</div>
                </div>
                <br />
                <div class="col-6 form-group">
                    <div class="row">
                        <div class=" col-md-5 form-group ">
                            <label class="form-label">Latitude</label>
                            <input type="text" class="gllpLatitude  form-control  " name="lat"
                                value={{isset($store) ? $store->latitude :""}} />
                        </div>
                        <div class=" col-md-5  form-group">
                            <label class="form-label">Longitude</label>
                            <input type="text" class="gllpLongitude form-control  " name="long"
                                value={{isset($store) ? $store->longitude :""}} />
                        </div>
                        <div class=" col-md-2  form-group">
                            <label class="form-label">Zoom</label>
                            <input type="text" class="gllpZoom form-control" value="20" />
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col form-group">
                            <label class="form-label">Address :
                                @foreach($store->addresses as $address)
                                {{$address->address}}
                                @endforeach
                            </label>
                            {{-- <input name="address" type="text" class="gllpLocationName form-control flex-fill"
                            value="{{ Str::limit($store->address,300)}}" placeholder="{{ Str::limit($store->address,300)}}" /> --}}
                        </div>
                    </div>
                    <input type="button" class="gllpUpdateButton p-2  btn btn-block  bg-blue mb-2 " value="Update Map">

                </div>
            </div>
    </fieldset>
    <button type="submit" class="btn1 btn float-right bg-blue mb-5 ">Submit</button>
    </div>

</form>

<script type="text/javascript">
    $(document).ready(function(){
      $(".form_datetime").datetimepicker({
        pickDate: false,
        minuteStep: 15,
        pickerPosition: 'bottom-right',
        format: 'HH:ii:00',
        autoclose: true,
        showMeridian: true,
        startView: 1,
        maxView: 1,
      });
      });
    </script>
   <script>
    $('.custom-file-input').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
</script>

<script>
    $('.sub').prop('disabled', true);
    $('.cat').change(function () {
        $('.sub').prop('disabled', false);
        if ($(this).val() != '') {
            var select = $(this).attr("id");
            var value = $(this).val();
            var dependent = $(this).data('dependent');
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('category.fetch') }}",
                method: "POST",
                data: {
                    select: select,
                    value: value,
                    dependent: dependent,
                    _token: _token
                },
                success: function (result) {
                    $('#' + dependent).html(result);
                }
            })
        }
    });

    $('#category').change(function () {
        $('#sub_category').val('');
    });
</script>

@endsection
