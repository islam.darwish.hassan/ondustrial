<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Ondustrial - @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <!--Google-->
        {{-- <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script> --}}
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9lwRaQPyQ4ZDOYN7cfhsl8HaCvyjpaL8&callback=myMap"></script>
        <!-- CSS and JS for our code -->
        {{-- <link   href={{asset("css/jquery-gmaps-latlon-picker.css")}} rel="stylesheet"/> --}}
        <script src={{asset('js/jquery-gmaps-latlon-picker.js')}}></script>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontawesome/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom2.css') }}" rel="stylesheet">
	<link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/animate/animate2.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/animsition/css/animsition.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/select2/select2.min.css')}}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('vendor/daterangepicker/daterangepicker.css')}}">
	<!--===============================================================================================-->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/quill/2.0.0-dev.4/quill.snow.min.css" />


</head>


<body >
    <div id="app" >
        <main class="bg-white" >
            <div class="container-fluid  ">
                <div class="row justify-content-center   " >
                    @auth
                    <div class="col-md-2 side bg-grad"  >
                        <div class="" style="border:none">
                            <div class="row  justify-content-between  ">
                                <div class="form-inline  my-lg-0">

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                                                   @csrf

                                    <a  href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();"
                                        class="btn      "
                                        ><i class="fas fa-power-off text-white"></i></a>
                                        <a id="navbarDropdown" class="nav-link text-dark adminLogin"
                                        href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        <i class=" fas fa-ellipsis-v clr-blue"></i> </a>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            <b class="dropdown-item ">
                                                {{Auth::user()->email}}
                                            </b>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            </form>
                                        </div>
                                </div>
                            </div>
                            <div class="card-body ">
                                <ul class="list-unstyled " >
                                    <div class = "panel-group ">
                                    <div class = "panel panel-default " >
                                        <div class = "panel-heading ">
                                        </div>
                                        @can('view-super',Auth::user())
                                        <div class="pl-3">
                                            <li class="menu_item"><a href="{{ route('dashboard.index') }}" class="btn-md text-white d-flex justify-content-between  align-items-center">
                                                <span>Dashboard</span> <i class="fas fa-chart-line  text-white fa-sm"></i> </a></li>
                                            <li class="menu_item"><a href="{{ route('categories.index') }}" class="btn-md text-white d-flex justify-content-between  align-items-center">
                                                <span>Categories & Subs</span> <i class="fas fa-air-freshener  text-white fa-1x"></i> </a></li>

                                                <li class="menu_item"><a href="{{ route('users.index') }}" class="btn-md text-white d-flex justify-content-between  align-items-center">
                                                <span>All Users</span> <i class="fas fa-user-friends  text-white fa-sm"></i> </a></li>
                                            <li class="menu_item"><a href="{{ route('clients.index') }}" class="btn-md text-white d-flex justify-content-between  align-items-center">
                                                <span>Clients</span> <i class="fas fa-user-ninja  text-white fa-sm"></i> </a></li>
                                            <li class="menu_item"><a href="{{ route('stores.index') }}" class="btn-md text-white d-flex justify-content-between  align-items-center">
                                                <span>Parteners</span> <i class="fas fa-store  text-white fa-xs"></i> </a></li>
                                            <li class="menu_item"><a href="{{ route('all_products.index') }}" class="btn-md text-white d-flex justify-content-between  align-items-center">
                                                <span>Products</span> <i class="fas fa-box-open  text-white fa-xs"></i> </a></li>

                                            <li class="menu_item"><a href="{{ route('orders.index') }}" class="btn-md text-white d-flex justify-content-between  align-items-center">
                                                <span>Orders</span> <i class="fas fa-shopping-cart  text-white fa-xs"></i> </a></li>
                                            <li class="menu_item"><a href="{{ route('banners.index') }}" class="btn-md text-white d-flex justify-content-between  align-items-center">
                                                    <span>Banners</span> <i class="fas fa-ad  text-white fa-xs"></i> </a></li>
                                            <li class="menu_item"><a href="{{ route('cities.index') }}" class="btn-md text-white d-flex justify-content-between  align-items-center">
                                              <span>Cities</span> <i class="fas fa-ad  text-white fa-xs"></i> </a></li>

                                                 {{-- <li class="menu_item"><a href="{{ route('tags_presets.index') }}" class="btn-md text-white d-flex justify-content-between  align-items-center">
                                                    <span>Tags Presets</span> <i class="fas fa-star-half-alt  text-white fa-sm"></i> </a></li> --}}

                                            </div>
                                        @endcan
                                        @can('view-store',Auth::user())
                                        <div class="pl-3">
                                            <li class="menu_item "><a href="{{ route('stores.show' ,App\Store::where('user_id',Auth::id())->first()) }}" class="btn-md text-white d-flex justify-content-between  align-items-center">
                                                <span>Dashboard</span> <i class="fas fa-chart-line  text-white fa-sm"></i> </a></li>
                                            <li class="menu_item"><a href="{{ route('products.index' ,App\Store::where('user_id',Auth::id())->first())}}" class="btn-md  text-white d-flex justify-content-between  align-items-center">
                                                <span>Products</span> <i class="fas fa-box-open  text-white fa-1x"></i> </a></li>
                                            <li class="menu_item"><a href="{{ route('shipments.index' ,App\Store::where('user_id',Auth::id())->first())}}"  class="btn-md  text-white d-flex justify-content-between  align-items-center">
                                                <span>Shipments</span> <i class="fas fa-pencil-alt text-white  fa-sm"></i> </a></li>
                                        </div>

                                      @endcan
                                    </div>

                                    </div>

                                </ul>
                            </div>
                        </div>
                        <img class=" qym_town fixed-bottom  " src="{{ asset('images/footer.png')}}" class="img-fluid" alt="">
                    </div>
                    @endauth

                    @auth
                    <div class="col-md-10  " >
                    @endauth
                    @guest
                    <div class="col-md-12">
                    @endguest
                        @if (session('message'))
                            <div class="alert alert-success mt-2" role="alert">
                                <b>{{ session('message') }}</b>
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger mt-2" role="alert" dir="rtl">
                                <b>{{ session('error') }}</b>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger mt-2">
                                <p><b>Please fix these errors.</b></p>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card" style="border:none">
                            <div class="card-header bg-white  mt-1 d-flex align-items-center" style="margin-left:-0.5rem">
                                <span ><b style="font-size:20px;">@yield('back') @yield('title')</b></span>
                                @yield('create-btn')
                            </div>

                            <div class="bg-white" style="margin-top: 10px;">
                                @yield('content')
                            </div>
                        </div>

                    </div>

                </div>

            </div>
        </main>
        <div class='cookies d-flex justify-content-center'>
            @include('cookieConsent::index')
            </div>

    </div>
</body>
@yield('extra_scripts')

</html>
