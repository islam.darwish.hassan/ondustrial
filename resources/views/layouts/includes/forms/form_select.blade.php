<select class="form-control" name="{{ $field }}">
    <option value="none" @if(request()->query($field) == 'none') selected @endif> --- {{ studly_case($field) }} --- </option>
    @foreach ($options as $option_name => $option_value)
        <option value="{{ $option_value }}" @if(request()->query($field) == $option_value) selected @endif>{{ $option_name }}</option>        
    @endforeach
</select>
