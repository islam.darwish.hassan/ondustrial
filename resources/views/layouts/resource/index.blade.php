@extends('layouts.app')
@section('title', ucfirst($name))

@section('content')


    @yield('search-filter')



    <!-- Table START-->
    <table class="table  table-bordered  table-hover table-sm text-center  table-center px-3 border-white">
        <thead class="  ">
            <tr >
                @yield('table-header')
            </tr>
        </thead>
        <tbody>
                @yield('table-body')
        </tbody>
    </table>
    @yield('table-footer')
    {{ $data->appends(\Request::except('page'))->render() }}
    @if($data->total()<=0)
    <div class="row justify-content-center">
        <h5 class="empty-table-text"> There are no results  😴  </h5>
    </div>
    @endif

    <!-- Table END-->

@endsection

