<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="shortcut icon" href="favicon.ico" />
    <title>Ondustrial</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- MainStyle -->
    {{-- <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet"> --}}
  <!-- CustomStyle -->
  <link rel="stylesheet" href="{{asset('website/css/open-iconic-bootstrap.min.css')}}">
  <link rel="stylesheet"  href="{{asset('website/css/animate2.css')}}">

  <link rel="stylesheet"  href="{{asset('website/css/owl.carousel.min.css')}}">
  <link rel="stylesheet"  href="{{asset('website/css/owl.theme.default.min.css')}}">
  <link rel="stylesheet"  href="{{asset('website/css/magnific-popup.css')}}">


  <link rel="stylesheet"  href="{{asset('website/css/ionicons.min.css')}}">

  {{-- <link rel="stylesheet"  href="{{asset('website/css/bootstrap-datepicker.css')}}">
  <link rel="stylesheet"  href="{{asset('website/css/jquery.timepicker.css')}}"> --}}
  <link href="{{ asset('css/fontawesome/css/all.min.css') }}" rel="stylesheet">

  <link rel="stylesheet"  href="{{asset('website/css/flaticon.css')}}">
  <link rel="stylesheet"  href="{{asset('website/css/icomoon.css')}}">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet"  href="{{asset('website/css/style5.css')}}">
  <link href="{{ asset('css/fontawesome/css/all.min.css') }}" rel="stylesheet">

</head>

<body>
{{--     <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
 --}}
 {{-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
      </ul>
    </div>
  </nav> --}}

    <nav class="navbar navbar-expand-lg  navbar-light navbar-fixed-top bg-light shadow" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="{{route('index')}}" style="font-family:'brandfont_bold'">Ondustrial</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false"
          aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>


	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
                @guest
                <li class="nav-item active"><a href="{{route('web.login')}}" class="nav-link">Sign in</a></li>
              @endguest
              @auth

              <li class="nav-item dropdown ">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink_auth" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(Auth::user()->role==2)
                    {{App\Client::where('user_id',Auth::user()->id)->first()->name}}
                    @else
                    {{Auth::user()->email}}

                    @endif
                </a>
                <ul class="dropdown-menu "  aria-labelledby="navbarDropdownMenuLink_auth">
                    <li><a class="dropdown-item " href="{{route('web.myaddresses.index')}}" >  My Addresses</a><li>
                    <li><a class="dropdown-item " href="{{route('web.mywishlists')}}" >  My Wishlists</a><li>
                    <li>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                         {{ __('Logout') }}
                     </a>
                     <form id="logout-form" action="{{ route('web.logout') }}" method="POST" style="display: none;">
                        @csrf
                      </form>
                    <li>

                {{-- <b class="dropdown-item ">
                <a href="{{route('web.myorders.index')}}" > My Orders</a>
                </b>
                <b class="dropdown-item ">
                    <a href="{{route('web.mywishlists')}}" > Wishlists</a>
                 </b>

                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('web.logout') }}" method="POST" style="display: none;">
                               @csrf

                </form> --}}
                </ul>

            </li>


              @endauth

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Shop
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <li><a class="dropdown-item" href="{{route('products.web.index')}}">Shop All</a>
                  </li>
                  <div class="dropdown-divider"></div>

                  @foreach (App\Category::all() as $category)
                  <li class="dropdown-submenu">
                    <a class="dropdown-item dropdown-toggle" href="{{route('products.web.index',["category"=>$category->id])}}">{{$category->name}}</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{route('products.web.index',["category"=>$category->id])}}">Shop All</a>
                        </li>
                        <div class="dropdown-divider"></div>
                        @foreach($category->subcategories as $subcategory)
                      <li><a class="dropdown-item" href="{{route('products.web.index',["sub_category"=>$subcategory->id])}}">{{$subcategory->name   }}</a></li>
                      @endforeach
                    </ul>
                  </li>
                  @endforeach
                </ul>
              </li>

              <li class="nav-item"><a href="{{route('login')}}" class="nav-link">Partners</a></li>
        <li class="nav-item cta cta-colored"><a href="{{route('cart.index')}}" class="nav-link"><span class="icon-shopping_cart"></span>[{{Cart::getTotalQuantity()}}]</a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

<div class="">
    @yield('search_bar')
    @yield('content')
    @yield('footer')
</div>
<footer class="container-fluid " style="background-color:'#333; height:30vh">
</footer>
  <!-- loader -->
  {{-- <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="rgba(34,193,195,1)"/></svg></div> --}}


  <script src="{{asset('website/js/jquery.min.js')}}"></script>
  <script src="{{asset('website/js/jquery-migrate-3.0.1.min.js')}}"></script>
  <script src="{{asset('website/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('website/js/jquery.easing.1.3.js')}}"></script>
  <script src="{{asset('website/js/jquery.waypoints.min.js')}}"></script>
  <script src="{{asset('website/js/jquery.stellar.min.js')}}"></script>
  <script src="{{asset('website/js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('website/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('website/js/aos.js')}}"></script>
  <script src="{{asset('website/js/jquery.animateNumber.min.js')}}"></script>
  <script src="{{asset('website/js/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('website/js/scrollax.min.js')}}"></script>

  <script src="{{asset('website/js/main4.js')}}"></script>
  <script>
            $('.dropdown-submenu .show').removeClass('show');

        $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
        }
        var $subMenu = $(this).next('.dropdown-menu');
        $subMenu.toggleClass('show');


        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
            $('.dropdown-submenu .show').removeClass('show');
        });


        return false;
        });
</script>

  </body>



</html>
