@extends('layouts.app')
@section('title', 'Create ' . ucfirst(\Illuminate\Support\Str::singular($name)) . ' Info')

@section('back')
        <a href="{{ route($name . '.index', $category->id) }}" >{{ ucfirst($name." of " .$category->name) }}</a> /
@endsection

@section('content')

    <form class="p-2 flex-grow-1" method="post" action="{{ route($name. '.store', $category->id) }}" enctype="multipart/form-data">
                   @csrf

        <div class="d-flex bd-highlight">
            <div class="form1 p-2 flex-fill bd-highlight">
                <div class="row">
                    <div class="col form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Name" required
                            value="{{ old('name') }}">
                    </div>
                    <div class="col form-group">
                        <label>Image</label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                            <input type="file" name="image" class="custom-file-input">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button type="submit" class="btn1 btn btn-block btn-success bg-brandgreen ">Create new sub category</button>
    </form>

@endsection
