@extends('layouts.resource.edit')
@section('title', 'Show ' . ucfirst(\Illuminate\Support\Str::singular($name)))


@section('form')

<form class="form" action="{{ route($name . '.show', $user->id) }}" method="get">

               @csrf


    @method('put')

    <div class="row">

        <div class=" col form-group">
            <label>Email</label>
            <input disabled type="email" name="email-view" class="form-control" placeholder="Email Address"
                value="{{  $user->email }}">
        </div>
        <div class="col form-group">
            <label>Role</label>
             <select  disabled class="form-control m-input" name="role" id="role" >
                <option value="">Select role</option>
                <option value="1" {{  $user->role=="1" ? 'selected' : '' }}>Admin</option>
                <option value="2" {{  $user->role=="2" ? 'selected' : '' }}>Store</option>
                <option value="3" {{  $user->role=="3" ? 'selected' : '' }}>Client</option>
                <option value="4" {{  $user->role=="4" ? 'selected' : '' }}>Casheer</option>
            </select>
        </div>
    </div>

</form>


@endsection
