@extends('layouts.resource.create')
@section('title', 'Create New User')


@section('form')

<form class="p-2 " method="post" action="{{ route($name. '.store') }}" enctype="multipart/form-data">
               @csrf

    <div class="d-flex flex-fill">
            <div class="row ">
                <div class="col form-group">
                    <label class="form-label">Type Of User<b style="color:red"> * </b></label>
                    <select class="sel custom-select " name="role" id="role">
                        <option selected>-- Choose --</option>
                        <option value="1">Admin</option>
                        <option value="2" >Client</option>
                        <option value="3" >Partner</option>
                    </select>
                </div>

        </div>
    </div>
    <div class="row flex-fill">
        <div class="col form-group">
            <label class="form-label">E-mail <b style="color:red"> * </b></label>
            <input type="email" name="email" class="form-control" placeholder="E-mail" required
            value="{{request()->query('email')}}" >
        </div>
        <div class=" col form-group">
            <label class="form-label">Password <b style="color:red"> * </b></label>
            <input type="password" name="password" class="form-control" placeholder="Password" required
                value="{{request()->query('password')}}" becrypt>
        </div>

    </div>
    <div class="form3">
        <div class=" mb-3">
        <h3 class="section-header ">Additional information: </h3>
        <p class="section-subheader">
            This is required additional information for creating new user
            <code>Partner</code>
        </p>
        </div>
            <div class=" flex-fill ">
                <div class="row">
                    <div class="col form-group">
                        <label class="form-label">Store Name<b style="color:red"> * </b></label>
                        <input type="text" name="name_store" class="form-control" placeholder="Name"
                            value="{{ old('name_store') }}">
                    </div>
                    <div class="col form-group">
                        <label class="form-label">Avatar<b style="color:red"> * </b></label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                            <input type="file" name="avatar_store" id="custom-file-input-image" class="custom-file-input">
                        </div>
                    </div>
                    <div class="col form-group">
                        <label class="form-label">Status<b style="color:red"> * </b></label>
                        <select name="status_store" class="custom-select">
                            <option value="0"  @if(request()->query('status_store') == 'none'||old('status_store')=='none') selected @endif>Choose...</option>
                            <option value="1"  @if(request()->query('status_store') == '1'||old('status_store')=='1') selected @endif>Verified</option>
                            <option value="2"  @if(request()->query('status_store') == '2'||old('status_store')=='2') selected @endif>Not verified</option>
                            <option value="3"  @if(request()->query('status_store') == '3'||old('status_store')=='3') selected @endif>Suspended</option>
                        </select>
                    </div>
                    <div class="col form-group">
                        <label class="form-label">Phone</label>
                            <input type="tel" name="phone_store" class="form-control " placeholder="Phone" id="phone_store"
                            value="{{null !==(old('phone_store'))? old('phone_store'):'' }}" />
                            </select>
                    </div>


                </div>
                <div class="row">
                    <div class=" col form-group">
                        <label class="form-label">BIO</label>
                        <textarea type="text" row="5" name="bio_store" class="form-control" placeholder="BIO"
                        value="{{null !==(old('bio_store'))? old('bio_store'):'' }}"
                        >{{null !==(old('bio_store'))? old('bio_store'):'' }}</textarea>
                    </div>
                </div>
        </div>
        <fieldset class="gllpLatlonPicker d-flex ">
            <div class="row">
                <div class="col-6 form-group ">
                    <label class="form-label">Location<b style="color:red"> * </b> </label>
                    <div class="gllpMap" style="width:100% width: 500px; height: 250px; ">Google Maps</div>
                </div>
                <br />
                <div class="col-6 form-group">
                    <div class="row">
                        <div class=" col-md-5 form-group ">
                            <label class="form-label">Latitude<b style="color:red"> * </b></label>
                            <input type="text" class="gllpLatitude  form-control  " name="lat"
                                value="{{null !==(old('lat'))? old('lat'):30.1054859543 }}" />
                        </div>
                        <div class=" col-md-5  form-group">
                            <label class="form-label">Longitude<b style="color:red"> * </b></label>
                            <input type="text" class="gllpLongitude form-control  " name="long"
                            value="{{null !==(old('long'))? old('long'):31.37631986912 }}" />
                        </div>
                        <div class=" col-md-2  form-group">
                            <label class="form-label">Zoom</label>
                            <input type="text" class="gllpZoom form-control" value="20" />
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col form-group">
                            <label class="form-label">Address <b style="color:red"> * </b></label>
                            <input name="address_store" id="address_store" type="text" class="gllpLocationName form-control flex-fill"
                            value="{{null !==(old('address_store'))? old('address_store'):'23 El-Zohour, Sheraton Al Matar, El Nozha, Cairo Governorate, Egypt' }}" />
                        </div>
                    </div>
                    <input type="button" class="gllpUpdateButton p-2  btn btn-block  bg-blue mb-2 " value="Update Map">

                </div>
            </div>
    </div>
    </fieldset>
        <!-- BEGIN : Additional  Section for User -->
        <div >
            <div class="form2">
                <div class=" mb-3">
                    <h3 class="section-header ">Additional information: </h3>
                    <p class="section-subheader">
                        This is  additional information for creating new
                        <code>Client</code>
                    </p>
                    </div>

                            <div class="m-section__content">
                        <div class="row">
                            <div class="form-group col">
                                <label class="form-label">Client name<b style="color:red"> * </b> </label>
                                <input type="name" name="name" class="form-control m-input"
                                id="name" aria-describedby="nameHelp" placeholder="Enter user name" value="{{ old('name') }}">
                            </div>
                            <div class="col form-group">
                                <label class="form-label">Status</label>
                                <select name="status" class="custom-select">
                                    <option value="0">Choose...</option>
                                    <option value="1" @if(request()->query('status') == 'none'||old('status')=='none') selected @endif>Verified</option>
                                    <option value="2" @if(request()->query('status') == 'none'||old('status')=='none') selected @endif>Not verified</option>
                                    <option value="3" @if(request()->query('status') == 'none'||old('status')=='none') selected @endif >Suspended</option>
                                </select>
                            </div>
                            <div class="col form-group">
                                <label class="form-label">Nationality</label>
                                <select name="status" class="custom-select">
                                    <option value="0">Choose...</option>
                                    @foreach(App\Nation::all() as $nation)
                                    <option value={{{$nation->id}}} >{{$nation->en_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col ">
                                <label class="form-label">Gender  </label>
                                <div>
                                    <select class="form-control m-input " name="gender" ">
                                                            <option value="">Select a gender </option>
                                                            <option value="m" @if(request()->query('gender') == 'm') selected @endif>Male</option>
                                        <option value="f" @if(request()->query('gender') == 'f') selected @endif>Female</option>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group col">
                                <label class="form-label">Phone : </label>
                                <input type="phone" name="phone" class="form-control m-input"
                                 id="phone" aria-describedby="nameHelp" placeholder="Enter user phone" value="{{ old('phone') }}">
                            </div>
                        </div>
                        {{-- <div class="form-group col-md-3">
                            <label class="form-label">Facebook : </label>
                            <input type="text" name="fb_url" class="form-control m-input" id="fb_url"
                            aria-describedby="nameHelp" placeholder="Enter user facebook" value="{{ old('fb_url') }}">
                        </div> --}}

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- END : of Additional Section for User -->
    <button type="submit" class="btn1 btn float-right bg-blue mb-5 ">Submit</button>
    </div>

</form>

<script>
    $('#custom-file-input-image').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image").files[0].name;
        //replace the "Choose a file" label

        $('.custom-file-label').html(fileName);
        console.log(fileName)

    })
    $('#custom-file-input-image2').on('change', function () {
        //get the file name
        var fileName = document.getElementById("custom-file-input-image2").files[0].name;
        //replace the "Choose a file" label
        console.log(fileName)
        $('#custom-file-label2').html(fileName);

    })


</script>
<script>
    $(".form1").hide();
    $(".sel").on('change', function (e) {
        if ($(this).val() == "1") {
            $(".form1").show();
            window.location="create?type=1"
        } else {

            $(".form1").hide();
        }
    });
    // client form
    $(".form2").hide();
    $(".sel").on('change', function (e) {
        if ($(this).val() == "2") {
            $(".form2").show();
            window.location="create?type=2"
        } else {

            $(".form2").hide();
        }
    });
    $(".form3").hide();
    $(".sel").on('change', function (e) {
        if ($(this).val() == "3") {
            $(".form3").show();
            window.location="create?type=3"
        } else {
            $(".form3").hide();
        }
    });
    $(".form4").hide();
    $(".sel").on('change', function (e) {
        if ($(this).val() == "4") {
            $(".form4").show();
            window.location="create?type=4"
        } else {

            $(".form4").hide();
        }
    });
</script>
<script>
    $(document).ready(function () {
        //to get params from url
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        var type = $.urlParam('type');
        if (type) {
            console.log(type);
            $(".sel").val(type);
            $(".form" + type).show();
        } else {
            console.log('no type')
        }
    });

</script>
<script>
    $('.sub').prop('disabled', false);
    $('.cat').change(function () {
       // $('.sub').prop('disabled', false);
        if ($(this).val() != '') {
            var select = $(this).attr("id");
            var value = $(this).val();
            var dependent = $(this).data('dependent');
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('category.fetch') }}",
                method: "POST",
                data: {
                    select: select,
                    value: value,
                    dependent: dependent,
                    _token: _token
                },
                success: function (result) {
                    $('#' + dependent).html(result);
                }
            })
        }
    });

    $('#category').change(function () {
        $('#sub_category').val('');
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
      $(".form_datetime").datetimepicker({
        pickDate: false,
        minuteStep: 15,
        pickerPosition: 'bottom-right',
        autoclose: true,
        showMeridian: true,
        startView: 1,
        maxView: 1,
      });
      });
    </script>

@endsection
@section('extra-scripts')
@endsection
