@extends('layouts.resource.index')
@section('title', 'All Users')
@section('create-btn')
<a href="{{ route($name . '.create') }}" class=" clr-blue p-2 "><i class="fas fa-plus"></i></a>
@endsection

@section('search-filter')

    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="d-flex justify-content-between">
            <div class="p-2 flex-fill ">
                @include('layouts.includes.forms.form_text',['field' => ['name' => 'name', 'text' => 'User']])
            </div>
            <div class="p-2 flex-fill ">
                <select class="form-control form-inline" name="role">
                    <option value="none" @if(request()->query('role') == 'none') selected @endif>- Choose role -</option>
                    <option value="1" @if(request()->query('role') == '1') selected @endif>Admin</option>
                    <option value="2" @if(request()->query('role') == '2') selected @endif>Client</option>
                    <option value="3" @if(request()->query('role') == '3') selected @endif>Partner</option>
                </select>
            </div>
            <div class="p-2 flex-fill ">
                <button type="submit" class="btn btn-block bg-blue mb-2">Filter</button>
            </div>
            <a href="{{ route($name. '.index') }}" class="small-header-bold"> X</a>

        </div>
    </form>

@endsection

@section('table-header')

    <th scope="col" style="width: 10%" >@sortablelink('id', '#')</th>
    <th scope="col"  >@sortablelink('email')</th>
    <th scope="col" >Name</th>
    <th scope="col"  >@sortablelink('role','Role')</th>
    <th  scope="col" style="width: 10%">Operations</th>
@endsection



@section('table-body')

    @foreach ($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->email }}</td>
            <td>
                @if($user->role!= '3' )
                    @if(null !==(App\Client::where('user_id',$user->id)->pluck('name')->first()))
                    {{ App\Client::where('user_id',$user->id)->pluck('name')->first() }}
                    @else
                    Not Available
                    @endif
                @elseif($user->role == '3')
                    @if(null !==(App\Store::where('user_id',$user->id)->pluck('name')->first()))
                    {{ App\Store::where('user_id',$user->id)->pluck('name')->first() }}
                    @else
                    Not Available
                    @endif
                @endif

            </td>

            <td>
                @if($user->role == '1')
                    Admin
                @elseif($user->role == '2')
                    Client
                @elseif($user->role == '3')
                    Partner
                @elseif($user->role == '4')
                    Cashier
                @endif
            </td>

            <td class="d-flex justify-content-center">
                @if($user->role=='1')
                <a href="{{ route($name.'.show', $user->id) }}"
                    class="btn clr-black  "><i class="fas fa-eye"></i></a>
                @elseif($user->role == '2')
                <a href="{{ route('clients'.'.show', $user->client->id) }}"
                    class="btn clr-black  "><i class="fas fa-eye"></i></a>
                @elseif($user->role == '3' && isset( $user->store))
                <a href="{{ route('stores'.'.show', $user->store->id) }}"
                    class="btn clr-black  "><i class="fas fa-eye"></i></a>
                @elseif($user->role == '4')
                <a href="{{ route('casheers'.'.show' ,  [$user->casheer->store->id ,$user->casheer->id]) }}"
                        class="btn clr-black  "><i class="fas fa-eye"></i></a>
                @endif
                @if($user->role=='1')
                <a href="{{ route($name.'.edit', $user->id) }}"
                    class="btn clr-black "><i class="fas fa-edit"></i></a>
                    @elseif($user->role == '2')
                {{-- <a href="{{ route($name.'.edit', $user->client->id) }}"
                    class="btn clr-black "><i class="fas fa-edit"></i></a> --}}
                    @elseif($user->role == '3' && isset( $user->store))
                    <a href="{{ route('stores'.'.edit', $user->store->id) }}"
                    class="btn clr-black "><i class="fas fa-edit"></i></a>
                    @elseif($user->role == '4')
                <a href="{{ route('casheers'.'.edit' ,  [$user->casheer->store->id ,$user->casheer->id]) }}"
                    class="btn clr-black "><i class="fas fa-edit"></i></a>
                @endif
                <form method="POST" action="{{ route($name.'.destroy', $user->id) }}">
                   @csrf
                    {{ method_field('DELETE') }}
                    <button type="submit"
                     onclick="return confirm('Are you sure you want to delete this user?')"
                     data-toggle="modal" data-target="#exampleModal"
                     class="btn  clr-blue  bg-transparent"><i class="fas fa-trash-alt "></i></button>
                </form>
            </td>
        </tr>
    @endforeach
    @section('table-footer')
    <p class="table-footer">Results :  {{$data->total()}}</p>
@endsection

@endsection
