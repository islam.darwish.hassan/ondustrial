@extends('layouts.app')
@section('title', 'Create ' . ucfirst(\Illuminate\Support\Str::singular($name)) )


@section('content')


<form method="post" action="{{ route($name. '.store',$preset->id) }}" >

               @csrf

    <div class="row">
        <div class="col-2 form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control" placeholder="Name "
                value="{{ old('name') }}">
        </div>
        <div class="col form-group">
            <label>Description</label>
            <textarea type="text" row="5" name="desc" class="form-control" placeholder="Description "
             value="{{ old('desc')}}">{{ old('desc')}}</textarea>
        </div>

    </div>
    <br>
    <button type="submit" class="btn btn-block btn-success bg-brandgreen ">Create
        {{ ucfirst(\Illuminate\Support\Str::singular($name)) }}</button>

</form>

@endsection
