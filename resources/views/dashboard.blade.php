@extends('layouts.app')
@section('title', ucfirst($name))

@section('content')
<!--Statistics-->
<div class="row">
    <div class="col"  onclick="window.location='{{ route('orders' . '.index' ) }}'">
        <div class="card bg-blue mb-2  flex-fill">
          <div class="card-header  d-flex justify-content-center">
            <i class="fas fa-box-open text-white my-3 display-4"></i>
          </div>
          <div class="card-body  text-center text-bold ">
              <div class="d-flex align-items-center justify-content-between" >
                  <div class="text-uppercase text-white "> Total Products: </div>
                  <div class="text-vlarge text-white ">{{$products_count}}</div>
              </div>
              <div class="d-flex align-items-center justify-content-between" >
                  <div class="text-uppercase text-white ">Online:</div>
                  <div class="text-vlarge text-white">{{$product_online_count}}</div>
              </div>
              <div class="d-flex align-items-center justify-content-between" >
                <div class="text-uppercase text-white ">Offline:</div>
                <div class="text-vlarge text-white">{{$product_offline_count}}</div>
            </div>
            <div class="d-flex align-items-center justify-content-between" >
                <div class="text-uppercase text-white ">Rejected:</div>
                <div class="text-vlarge text-white">{{$product_rejected_count}}</div>
            </div>
            <div class="d-flex align-items-center justify-content-between" >
                <div class="text-uppercase text-white ">InReview:</div>
                <div class="text-vlarge text-white">{{$product_inreview_count}}</div>
            </div>

          </div>
        </div>
        </div>
        <div class="col"  onclick="window.location='{{ route('users' . '.index' ) }}'">
            <div class="card bg-blue mb-2  flex-fill">
              <div class="card-header  d-flex justify-content-center">
                <i class="fas fa-shopping-cart text-white my-3 display-4"></i>
              </div>
              <div class="card-body  text-center text-bold ">
                  <div class="d-flex align-items-center justify-content-between" >
                      <div class="text-uppercase text-white "> Total Orders: </div>
                      <div class="text-vlarge text-white ">{{$orders_count}}</div>
                  </div>
                  <div class="d-flex align-items-center justify-content-between" >
                      <div class="text-uppercase text-white ">Requests:</div>
                      <div class="text-vlarge text-white">{{$orders_requests_count}}</div>
                  </div>
                  <div class="d-flex align-items-center justify-content-between" >
                    <div class="text-uppercase text-white ">In Process:</div>
                    <div class="text-vlarge text-white">{{$orders_inprocess_count}}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between" >
                    <div class="text-uppercase text-white ">In Shipping:</div>
                    <div class="text-vlarge text-white">{{$orders_inshipping_count}}</div>
                </div>
                <div class="d-flex align-items-center justify-content-between" >
                    <div class="text-uppercase text-white ">Shipped:</div>
                    <div class="text-vlarge text-white">{{$orders_shipped_count}}</div>
                </div>

              </div>
            </div>
            </div>

    <div class="col"  onclick="window.location='{{ route('stores' . '.index' ) }}'">
        <div class="card bg-blue mb-2  flex-fill">
          <div class="card-header  d-flex justify-content-center">
            <i class="fas fa-store text-white my-3 display-4"></i>
          </div>
          <div class="card-body  text-center  text-bold">
              <div class="d-flex align-items-center justify-content-between" >
                  <div class="text-uppercase text-white "> Total Partners: </div>
                  <div class="text-vlarge text-white ">{{$stores_count}}</div>
              </div>
              <div class="d-flex align-items-center justify-content-between" >
                  <div class="text-uppercase text-white ">Verified:</div>
                  <div class="text-vlarge text-white">{{$stores_ver_count}}</div>
              </div>
              <div class="d-flex align-items-center justify-content-between" >
                  <div class="text-uppercase text-white ">Not Verified:</div>
                  <div class="text-vlarge text-white">{{$stores_not_ver_count}}</div>
              </div>
              <div class="d-flex align-items-center justify-content-between" >
                <div class="text-uppercase text-white ">Suspended:</div>
                <div class="text-vlarge text-white">{{$stores_sus_count}}</div>
            </div>

          </div>
        </div>
        </div>
        <div class="col"  onclick="window.location='{{ route('users' . '.index' ) }}'">
            <div class="card bg-blue mb-2  flex-fill">
              <div class="card-header  d-flex justify-content-center">
                <i class="fas fa-user-ninja text-white my-3 display-4"></i>
              </div>
              <div class="card-body  text-center text-bold ">
                  <div class="d-flex align-items-center justify-content-between" >
                      <div class="text-uppercase text-white text-bold "> Total Users: </div>
                      <div class="text-vlarge text-white ">{{$clients_count +$admins_count}}</div>
                  </div>
                  <div class="d-flex align-items-center justify-content-between" >
                      <div class="text-uppercase text-white ">Clients:</div>
                      <div class="text-vlarge text-white">{{$clients_count}}</div>
                  </div>
                  <div class="d-flex align-items-center justify-content-between" >
                    <div class="text-uppercase text-white ">Admins:</div>
                    <div class="text-vlarge text-white">{{$admins_count}}</div>
                </div>

              </div>
            </div>
            </div>

                    <div class="col"  onclick="window.location='{{ route('users' . '.index' ) }}'">
                        <div class="card bg-blue mb-2  flex-fill">
                          <div class="card-header  d-flex justify-content-center">
                            <i class="fas fa-air-freshener text-white my-3 display-4"></i>
                          </div>
                          <div class="card-body  text-center text-bold ">
                              <div class="d-flex align-items-center justify-content-between" >
                                  <div class="text-uppercase text-white "> Total Categories : </div>
                                  <div class="text-vlarge text-white ">{{$categories_count}}</div>
                              </div>
                              <div class="d-flex align-items-center justify-content-between" >
                                  <div class="text-uppercase text-white ">Total SubCategories:</div>
                                  <div class="text-vlarge text-white">{{$sub_categories_count}}</div>
                              </div>

                          </div>
                        </div>
                        </div>
  </div>
  <!--end of Statistics-->
    <div class="row">
        <div class="col">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div id="pop_div"></div>
                            {!! \Lava::render('AreaChart', 'Population', 'pop_div') !!}
                        </div>
                        <div class="col-lg-6">
                            <div id="pop_div2"></div>
                            {!! \Lava::render('AreaChart', 'Stores', 'pop_div2') !!}
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-lg-6">
                            <div id="pop_div3"></div>
                            {!! \Lava::render('AreaChart', 'Clients', 'pop_div3') !!}
                        </div>
                        <div class="col-lg-6">
                            <div id="pop_div4"></div>
                            {!! \Lava::render('AreaChart', 'Orders', 'pop_div4') !!}
                        </div>

                    </div>

                    </div>


        </div>
    </div>
@endsection
