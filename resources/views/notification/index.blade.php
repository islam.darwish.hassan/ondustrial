@extends('layouts.app')
@section('title', ucfirst($name))

@section('create-btn')
<a href="{{ route($name . '.create') }}" class="btn btn-success float-right">Send {{ ucfirst(\Illuminate\Support\Str::singular($name)) }}</a>
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header"><b>Statstics</b></div>
                <div class="card-body">
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Notification</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{-- <tr>
                                <td><b><a href="{{ route('admin') }}">Admins Count</a></b></td>
                                <td>{{ $admins_count }}</td>
                            </tr>
                            <tr>
                                <td><b><a href="{{ route('clients.index') }}">Clients Count</a></b></td>
                                <td>{{ $clients_count }}</td>
                            </tr>
                            <tr>
                                <td><b><a href="{{ route('stores.index') }}">Stores Count</a></b></td>
                                <td>{{ $stores_count }}</td>
                            </tr>
                            <tr>
                                <td><b><a href="">Reviews Count</a></b></td>
                                <td>{{ $reviews_count }}</td>
                            </tr>
                            <tr>
                                <td><b><a href="{{ route('categories.index') }}">Categories Count</a></b></td>
                                <td>{{ $categories_count }}</td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
