@extends('layouts.resource.create')


@section('form')

<div class="row">

    <div class="col-md-6">
        <p><b>Send By Country</b></p>
        <form method="post" action="{{ route($name.'.store') }}">
                       @csrf

            <input type="hidden" name="type" value="by_country">
            <div class="form-group">
                <label>Notification Body</label>
                <textarea class="form-control" name="notification" rows="6"
                    placeholder="Enter Notification body.">{{ old('notification') }}</textarea>
            </div>
            <div class="form-group">
                <label>Choose Specific country</label>
                <select class="form-control m-input dynamic" name="country_id">
                    <option selected value="">Select Country</option>
                    @foreach($countries as $country)
                    <option value="{{ $country->id }}" {{ (old('country_id') == $country->id) ? 'selected' : '' }}>
                        {{ $country->ar_name }}</option>
                    @endforeach
                </select>
            </div>
            <br>
                <button type="button" data-toggle="modal" data-target="#sendcountrymodal"
                class="btn btn-block btn-success bg-brandgreen ">Send
                {{ ucfirst(\Illuminate\Support\Str::singular($name)) }} to country</button>
            <!-- Modal -->
            <div class="modal fade" id="sendcountrymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Notification</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Send {{ ucfirst(\Illuminate\Support\Str::singular($name)) }} to this country
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                           @csrf
                            {{ method_field('POST') }}
                            <button type="submit" class="btn btn-success bg-brandgreen">Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="col-md-6">
        <p><b>Send By User</b></p>
        <form method="post" action="{{ route($name.'.store') }}">
                       @csrf

            <input type="hidden" name="type" value="by_user">
            <div class="form-group">
                <label>Notification Body</label>
                <textarea class="form-control" name="notification" rows="6"
                    placeholder="Enter Notification body.">{{ old('notification') }}</textarea>
            </div>
            <div class="form-group">
                <label>Choose Specific user</label>
                <select class="form-control m-input dynamic" name="user_id">
                    <option selected value="">Select User</option>
                    @foreach($users as $user)
                    <option value="{{ $user->id }}" {{ (old('user_id') == $user->id) ? 'selected' : '' }}>
                        {{ $user->id }}- {{ $user->email }}</option>
                    @endforeach
                </select>
            </div>
            <br>

            <button type="button" data-toggle="modal" data-target="#sendusermodal"
                class="btn btn-block btn-success bg-brandgreen ">Send
                {{ ucfirst(\Illuminate\Support\Str::singular($name)) }} for user</button>
            <!-- Modal -->
            <div class="modal fade" id="sendusermodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Notification</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Send {{ ucfirst(\Illuminate\Support\Str::singular($name)) }} to this user
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                           @csrf
                            {{ method_field('POST') }}
                            <button type="submit" class="btn btn-success bg-brandgreen">Send</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>

</div>

@endsection
