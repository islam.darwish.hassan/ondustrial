@extends('layouts.resource.index')
@section('title', "Shipment Managment")

@section('search-filter')
    <!-- Search and Filters START-->
    <form class="form " action="{{ route($name . '.index') }}" method="get">
        <div class="row d-flex justify-content-between">
            <div class="p-2 col-lg-6 col-md-12 ">
             @include('layouts.includes.forms.form_text',['field' => ['name' => 'code', 'placeholder' => 'Search By Code..']])
            </div>
            <div class="p-2 col-lg-2 col-md-4">
                <div class="col form-group">
                    <select name="status" class="custom-select">
                        <option value="0"  @if(request()->query('status') == '0') selected @endif>Status...</option>
                        <option value="1"  @if(request()->query('status') == '1') selected @endif>Request</option>
                        <option value="2"  @if(request()->query('status') == '2') selected @endif>Processing</option>
                        <option value="3" @if(request()->query('status') == '3') selected @endif>Shipping</option>
                        <option value="3" @if(request()->query('status') == '4') selected @endif>Shipped</option>

                    </select>
                </div>

             </div>
             <div class="p-2 col-lg-2 col-md-4">
             <div class="col form-group">
                <select name="delivey_city" class="custom-select">
                    <option value="0"  @if(request()->query('delivey_city') == '0') selected @endif>Delivery City...</option>
                    @foreach(App\City::all() as $city)
                   <option value="{{$city->id}}"  @if(request()->query('delivey_city') ==$city->id) selected @endif>{{$city->en_name}}</option>
                    @endforeach
                </select>
            </div>
             </div>
            <div class="p-2 col-lg-2 col-md-2  ">
                <button type="submit" class="btn btn-block bg-blue mb-2 ">Filter</button>
            <div class="float-right  ">
            <a href="{{ route($name. '.index') }}" class="small-header-bold "><b class="text-muted small"> Clear Filters  X</b></a>
            </div>
           </div>

        </div>
    </form>
    <!-- Search and Filters END-->
@endsection

@section('table-body')

@section('table-header')

        <th scope="col">@sortablelink('id', '#')</th>
        <th scope="col">@sortablelink('hash_code', 'Code')</th>
        <th scope="col">@sortablelink('ordered_products_count', 'Products')</th>
        <th scope="col">Delivery City</th>
        <th scope="col">@sortablelink('status', 'Status')</th>
        <th scope="col">@sortablelink('created_at', 'Created At') </th>
        <th scope="col">Operations </th>
@endsection
@section('table-body')
@foreach($shipments as $shipment)
<!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->


<td>{{ $shipment->id }}</td>
<td>{{ $shipment->hash_code }}</td>
<td>{{ $shipment->ordered_products->count() }}</td>
<td>{{ $shipment->order->address->city->en_name }}</td>
<td>
    @if ($shipment->status == 1)
    <span class="badge bg-purple text-white">Request</span>
    @elseif ($shipment->status == 2)
    <span class="badge badge-primary text-white">Processing</span>
    @elseif ($shipment->status == 3)
    <span class="badge badge-warning text-black">In Shipping</span>
    @elseif ($shipment->status == 4)
    <span class="badge badge-success text-white">Shipped</span>
    @elseif ($shipment->status == 5)
    <span class="badge bg-grey text-black">Draft</span>
    @else    <span class="badge bg-grey text-black">Not determined</span>
    @endif
</td>
<td>{{Carbon\Carbon::parse($shipment->created_at)->diffForHumans()}}</td>

<!------------------------------------------------------------- Rows START---------------------------------------------------------------------------->

<td class="d-flex justify-content-center"><a href="{{ route('orders.show',[$shipment->order->id]) }}"
    class="btn clr-black  "><i class="fas fa-eye"></i></a>
</tr>
@endforeach
@endsection
    {{ $data->appends(request()->query())->links() }}
@section('table-footer')
<p class="table-footer">Results :  {{$data->total()}}</p>

@endsection
@endsection
